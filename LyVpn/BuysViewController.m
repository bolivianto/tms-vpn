//
//  BuysViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "BuysViewController.h"
#import "BuysTableViewCell.h"
#import <StoreKit/StoreKit.h>
#import "LSProgressHUD.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "NewWork.h"
#import "BuyRecordViewController.h"
#import "JInPayController.h"
#import "CLAlertView.h"
#import "BindingViewController.h"
#import "VPN-Swift.h"

@interface BuysViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary* dic;
    UIImageView *topImage;
}
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSMutableArray * arrayData;
@property (nonatomic,strong) NSMutableDictionary * ADdict;
@property (nonatomic,strong) NSString * username;

//商品图片
@property (weak, nonatomic)  UIImageView *foodImageView;
//商品名
@property (weak, nonatomic)  UILabel *name;
//颜色值
@property (weak, nonatomic)  UILabel *month_saled;
//减
@property (weak, nonatomic)  UIButton *minus;
//加
@property (weak, nonatomic)  UIButton *plus;
//数量
@property (weak, nonatomic)  UILabel *orderCountTF;

@property (nonatomic,strong) NSArray * productsArray;
@end

@implementation BuysViewController
{
    NSInteger indexNum;
}
- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound) {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}

- (NSString*)currentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Load Layout
    [self createTableView];
    [self creatNavi];
    
    self.ADdict = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshdata) name:@"refreshpaydata" object:nil];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case NotReachable:
                [Global printLogWithObject:AFStringFromNetworkReachabilityStatus(status) function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                [self loadOfflinePayments];
                break;
            default:
                if([JInPayController sharedInstanceMethod].isActivate) {
                    self.arrayData = [JInPayController sharedInstanceMethod].arrayData;
                    [self.tableView reloadData];
                } else {
                    [self loadOfflinePayments];
                }
                break;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (void)loadOfflinePayments {
    self.arrayData = [[NSUserDefaults standardUserDefaults] objectForKey:ListProductIdentifier];
    [self.tableView reloadData];
}

- (void)creatNavi {
    UIButton *backButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [backButton setImage:[UIImage imageNamed:@"返回1"] forState:UIControlStateNormal];
    //    if (IS_IPHONEX) {
    //        [adBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    //    }
    [backButton addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * iteml4=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = iteml4;
    
    if ([[self panduan] isEqualToString:@"chinese"]){
        self.navigationItem.title = @"购买套餐";
        
    } else {
        self.navigationItem.title = @"Purchase";
    }
    
    
    /*
     *透明效果
     */
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    
}
- (void)dismissViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)createTableView {
    topImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH - 100 * KWidth_Scale)];
    topImage.image = [UIImage imageNamed:@"background"];
    [self.view addSubview:topImage];
    
    
    ///无广告
    UIImageView *advertImage = [[UIImageView alloc]initWithFrame:CGRectMake(20*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 30*KWidth_Scale, 25 * KWidth_Scale)];
    advertImage.image = [UIImage imageNamed:@"AD"];
    [topImage addSubview:advertImage];
    
    UILabel *advertLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(advertImage.frame) + 5*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 65*KWidth_Scale, 25*KWidth_Scale)];
    if ([[self panduan]isEqualToString:@"chinese"]) {
        advertLabel.text = @"无广告";
    }else{
        advertLabel.text = @"No ad";
    }
    advertLabel.textColor = [UIColor whiteColor];
    advertLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    [topImage addSubview:advertLabel];
    
    
    
    ///多线路
    UIImageView *MultilineImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - 50*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 35*KWidth_Scale, 23*KWidth_Scale)];
    MultilineImage.image = [UIImage imageNamed:@"线路"];
    [topImage addSubview:MultilineImage];
    
    UILabel *multilineLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(MultilineImage.frame) + 5*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 65*KWidth_Scale, 23*KWidth_Scale)];
    if ([[self panduan]isEqualToString:@"chinese"]) {
        multilineLabel.text = @"多线路";
    }else{
        multilineLabel.text = @"Multiple";
    }
    multilineLabel.textColor = [UIColor whiteColor];
    multilineLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    [topImage addSubview:multilineLabel];
    
    
    
    
    ///更稳定
    UIImageView *stableImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 120*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 30*KWidth_Scale, 25*KWidth_Scale)];
    stableImage.image = [UIImage imageNamed:@"稳定"];
    [topImage addSubview:stableImage];
    
    UILabel *stableLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(stableImage.frame) + 5*KWidth_Scale, CGRectGetMaxY(topImage.frame) - 40*KWidth_Scale, 65*KWidth_Scale, 25*KWidth_Scale)];
    if ([[self panduan]isEqualToString:@"chinese"]) {
        stableLabel.text = @"更稳定";
    }else{
        stableLabel.text = @"Stable";
    }
    stableLabel.textColor = [UIColor whiteColor];
    stableLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    [topImage addSubview:stableLabel];
    
    
    if (IS_IPAD) {
        multilineLabel.font = [UIFont systemFontOfSize:11*KWidth_Scale];
        stableLabel.font = [UIFont systemFontOfSize:11*KWidth_Scale];
        advertLabel.font = [UIFont systemFontOfSize:11*KWidth_Scale];
    }
    
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH / 4, SCREEN_WIDTH / 4)];
    
    logoImageView.image = [UIImage imageNamed:@"topLogo"];
    
    logoImageView.center = CGPointMake(topImage.center.x, topImage.center.y);
    
    [topImage addSubview:logoImageView];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(topImage.frame)+10*KWidth_Scale, SCREEN_WIDTH, SCREEN_HEIGHT ) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UITableView alloc]init];
    [self.tableView setSeparatorColor: [UIColor clearColor]];
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"BuysTableViewCell" bundle:nil] forCellReuseIdentifier:@"BuysTableViewCell"];
    
}
- (NSMutableArray *)arrayData
{
    if (_arrayData == nil) {
        
        _arrayData = [NSMutableArray arrayWithCapacity:0];
    }
    return _arrayData;
}
- (NSMutableDictionary *)ADdict
{
    if (_ADdict == nil) {
        _ADdict = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return _ADdict;
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;{
    
    // the user clicked OK
    
    //只有一个选项，哈哈哈
    BindingViewController *logVC = [[BindingViewController alloc]init];
    logVC.userName = dic[@"UserName"];
    self.navigationController.tabBarController.tabBar.hidden = YES;
    [self.navigationController pushViewController:logVC animated:YES];
    
    
}

#pragma mark  ----------------  UITableViewDelegate  &  UITableViewDataSource  -------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayData.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        return 50*KWidth_Scale;
    }
    return 75*KWidth_Scale;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView * view = [UIView new];
//    view.backgroundColor = [UIColor clearColor];
//    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
//    label.text = NSLocalizedString(@"buy1", nil);
//    label.textColor = [UIColor darkGrayColor];
//    label.font = [UIFont systemFontOfSize:18];
//    [view addSubview:label];
//    return view;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 50;
//}
//
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView * view = [UIView new];
//    view.backgroundColor = [UIColor clearColor];
//    return view;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuysTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"BuysTableViewCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    if (indexPath.row == 0) {
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell GetCellDataMessage:self.arrayData index:indexPath.row];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *ProductDict;
    if ([JInPayController sharedInstanceMethod].isActivate) {
        ProductDict = [JInPayController sharedInstanceMethod].arrayData[indexPath.row];
    }else{
        [SVProgressHUD showErrorWithStatus:@"请稍后再试"];
        return;
    }
    NSString *payStr = self.ADdict[@"PaymentLink"];
    if ([self.ADdict[@"PaymentSwitch"] isEqualToString:@"1"] && payStr.length > 1)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ppppp"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:payStr]];
        
    } else {
        [[JInPayController sharedInstanceMethod] buy:(long)indexPath.row];
        //                weakself(self);
        [JInPayController sharedInstanceMethod].bingdingBlock = ^{
            [self refreshdata];
        };
    }
}
-(void)refreshdata
{
    NSString *string = [NSString stringWithFormat:@"%@",dic[@"Email"]];
    if (string) {
        if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"]){
            
            //加开关，控制提示
            if ([self.ADdict[@"buttonSwitch"] isEqualToString:@"1"])
            {
                //如果没有邮箱，提示绑定
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"reglog6", nil) message:NSLocalizedString(@"other9", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
                [alertView show];
            }
            //刷新数据
            NSDictionary* dic2 = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
            //    NSString *username = dic[@"UserName"];
            NSString *name = dic2[@"UserName"];
            
            NSString *string = [NSString stringWithFormat:@"%@/lygamesService.asmx/FeevisitorLoginReg?apikey=lygames_0953&uuid=%@&name=%@&source=%@",GONGYOUURL,[UUID getUUID],name,TYPESTRING];
            
            NSURL *url = [NSURL URLWithString:string];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
            
            NSURLResponse *response = nil;
            NSError *error = nil;
            
            NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (userdata)
            {
                NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
                
                if (array)
                {
                    [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                    
                }
                
            }
        } else {
            //刷新数据
            NSDictionary* dic1 = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
            NSString *passWord = dic1[@"Password"];
            
            NSString *email = dic1[@"Email"];
            
            
            NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserLogin?apikey=lygames_0953&uuid=%@&name=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],email,passWord,TYPESTRING];
            
            NSURL *url = [NSURL URLWithString:userString];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
            
            NSURLResponse *response = nil;
            NSError *error = nil;
            
            
            NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (userdata)
            {
                NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
                
                if (array)
                {
                    [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                }
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
