//
//  UserDetailViewController.m
//   VPN
//
//  Created by cacac on 2017/2/13.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "UserDetailViewController.h"
#import "LogViewController.h"
#import "ModPassViewController.h"
static NSString * cellidenty = @"cell";
@interface UserDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView * tableView;

@end

@implementation UserDetailViewController


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];

    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"key2", nil);
    
    [self createNavi];
    [self createTableView];
    self.view.backgroundColor = [UIColor whiteColor];
  
}


- (void) createTableView
{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NaviHeight)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame) + 5*KWidth_Scale, SCREEN_WIDTH, SCREEN_HEIGHT-NaviHeight) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableView.scrollEnabled = NO;
    self.tableView.tableFooterView = [[UITableView alloc]init];
    [self.view addSubview:self.tableView];
}

#pragma mark  -----------------UITableViewDelegate  &  UITableViewDataSource-----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD)
    {
        return 80;
    }else
    {
        return 50;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenty];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"user6", nil);
    }else if (indexPath.row == 1)
    {
        cell.textLabel.text = self.UUID;
        cell.textLabel.font = [UIFont systemFontOfSize:13];
    }else if (indexPath.row == 2)
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.text = NSLocalizedString(@"userMess9", nil);
        cell.textLabel.font = [UIFont systemFontOfSize:13];
    }
    
    cell.textLabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.textLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    return cell;
}
#warning .....
/*
 -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {NSUInteger count = 0;
 if ([tableView isEqual:self.ShopCartView.OrderList.tableView])
 {
 
 return [self.ordersArray count];
 
 }
 if ([tableView isEqual:self.tableView]) {
 count = [_dataArray count];
 
 }
 return count;
 }
 
 -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 MZWColorListCell *cell = nil;
 if ([tableView isEqual:self.tableView]) {
 static NSString *cellID = @"MZWColorListCell";
 
 cell = (MZWColorListCell *) [tableView dequeueReusableCellWithIdentifier:cellID];
 //        _cell = (MZWColorListCell*) [tableView cellForRowAtIndexPath:indexPath];
 if (!cell) {
 cell = [[[NSBundle mainBundle] loadNibNamed:cellID owner:nil options:nil] lastObject];
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
 float price = [_dataArray[indexPath.section][@"min_price"] floatValue];
 //            NSInteger nSaledNum =[_dataArray[indexPath.section][@"month_saled"] integerValue];
 //            NSInteger nPraiseNum =[_dataArray[indexPath.section][@"praise_num"] integerValue];
 
 cell.name.text = _dataArray[indexPath.section][@"name"];
 cell.month_saled.text = [NSString stringWithFormat:@"¥%.2f/Kg",price];
 cell.foodImageView.image = [UIImage imageNamed:_dataArray[indexPath.section][@"picture"]];
 
 
 cell.foodId = [_dataArray[indexPath.section][@"id"] integerValue];
 cell.amount = _dataArray[indexPath.section][@"orderCountTF"]?[_dataArray[indexPath.section][@"orderCountTF"] integerValue]:0;
 
 // 1. 创建一个点击事件，点击时触发labelClick方法
 UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelClick:)];
 // 2. 将点击事件添加到label上
 [cell.orderCountTF addGestureRecognizer:labelTapGestureRecognizer];
 cell.orderCountTF.userInteractionEnabled = YES; // 可以理解为设置label可被点击
 cell.orderCountTF.tag=indexPath.section;
 
 cell.plus.tag =indexPath.section;
 cell.minus.tag =indexPath.section;
 
 __weak __typeof(&*cell)weakCell =cell;
 cell.plusBlock = ^(NSInteger nCount,NSInteger rowcount,BOOL animated,BOOL countall)
 {
 NSMutableDictionary * dic = _dataArray[rowcount];//从数组中取出那一行
 [dic setObject:[NSNumber numberWithInteger:nCount] forKey:@"orderCountTF"];//把指定那一行数量变更
 CGRect parentRect = [weakCell convertRect:weakCell.plus.frame toView:self.view];
 
 
 NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:dic];
 
 [self storeOrders:dict isAdded:animated withSectionIndex:0 withRowIndex:0];
 if (countall) {
 if (animated) {
 [self JoinCartAnimationWithRect:parentRect];
 _totalOrders ++;
 }
 else
 {
 _totalOrders --;
 }
 }else{
 if (animated) {
 [self JoinCartAnimationWithRect:parentRect];
 _totalOrders +=nCount;
 }
 else
 {
 _totalOrders -=nCount;
 }
 }
 _ShopCartView.badge.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[self.ordersArray[0] count]];//显示购物车有多少订单
 
 [self setCartImage];
 [self setTotalMoney];
 NSIndexSet *indexSetA = [[NSIndexSet alloc]initWithIndex:rowcount];    //刷新第3段
 
 [tableView reloadSections:indexSetA withRowAnimation:UITableViewRowAnimationAutomatic];
 
 };
 
 
 }
 
 return cell;
 }
 else if ([tableView isEqual:_ShopCartView.OrderList.tableView])
 {
 static NSString *cellID = @"ColorShoppingCatrCell";
 
 ColorShoppingCatrCell *cell = (ColorShoppingCatrCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
 
 if (!cell) {
 cell = [[[NSBundle mainBundle] loadNibNamed:cellID owner:nil options:nil] lastObject];
 }
 NSMutableArray *sectionArray =[NSMutableArray array];
 sectionArray = [self.ordersArray objectAtIndex:indexPath.section];
 
 //
 cell.id = [sectionArray[indexPath.row][@"id"] integerValue];
 cell.nameLabel.text = sectionArray[indexPath.row][@"name"];
 
 float price = [sectionArray[indexPath.row][@"min_price"] floatValue];
 cell.priceLabel.text = [NSString stringWithFormat:@"￥%.0f",price] ;
 
 NSInteger count = [sectionArray[indexPath.row][@"orderCountTF"] integerValue];
 
 // 1. 创建一个点击事件，点击时触发labelClick方法
 UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShoppingCatrcount:)];
 // 2. 将点击事件添加到label上
 [cell.numberLabel addGestureRecognizer:labelTapGestureRecognizer];
 cell.numberLabel.userInteractionEnabled = YES; // 可以理解为设置label可被点击
 cell.numberLabel.tag=[sectionArray[indexPath.row][@"id"] integerValue];
 cell.minus.tag=indexPath.row;
 cell.plus.tag=indexPath.row;
 cell.number = count;
 cell.numberLabel.text = [NSString stringWithFormat:@"%ld",count];
 
 cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 if (indexPath.section % 3 == 0) {
 cell.dotLabel.textColor = [UIColor greenColor];
 }
 else if (indexPath.section % 3 == 1)
 {
 cell.dotLabel.textColor = [UIColor yellowColor];
 }
 else if (indexPath.section % 3 == 2)
 {
 cell.dotLabel.textColor = [UIColor redColor];
 }
 
 cell.operationBlock = ^(NSUInteger nCount,NSUInteger rowcount,BOOL plus)
 {
 NSMutableDictionary * dic = sectionArray[rowcount];
 
 //更新订单列表中的数量
 [dic setObject:[NSNumber numberWithInteger:nCount] forKey:@"orderCountTF"];
 NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:dic];
 
 //获取当前id的所有数量
 NSInteger nTotal = [self CountAllSections:[NSString stringWithFormat:@"%ld",[dic[@"id"] integerValue]]];
 [dict setObject:[NSNumber numberWithInteger:nTotal] forKey:@"orderCountTF"];
 
 [self storeOrders:dict isAdded:plus withSectionIndex:indexPath.section withRowIndex:indexPath.row];
 
 //            if (plus) {
 //
 //                _totalOrders ++;
 //            }
 //            else
 //            {
 //                _totalOrders --;
 //            }
 _totalOrders = plus ? ++_totalOrders : --_totalOrders;
 
 _ShopCartView.badge.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)_totalOrders];
 //刷新tableView
 [self.tableView reloadData];
 [self setCartImage];
 [self setTotalMoney];
 if (_totalOrders <=0) {
 [_ShopCartView dismissAnimated:YES];
 }
 
 };
 
 return cell;
 }
 return cell;
 
 }
*/
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        UIPasteboard *pab = [UIPasteboard generalPasteboard];
        NSString *string = self.UUID;
        [pab setString:string];
        if (pab == nil) {
            //        NSLog(@"复制失败");
            [self showMessage:NSLocalizedString(@"custom9", nil) duration:2];
        }else
        {
            //        NSLog(@"复制成功");
            [self showMessage:NSLocalizedString(@"custom8", nil) duration:2];
        }
    }else if (indexPath.row == 2)
    {
        ModPassViewController *modPassVC = [[ModPassViewController alloc]init];
        
        [self.navigationController pushViewController:modPassVC animated:YES];
        
    }
}
-(void)showMessage:(NSString *)message duration:(NSTimeInterval)time
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [UIColor grayColor];
    showview.frame = CGRectMake(1, 1, 1, 1);
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    
    UILabel *label = [[UILabel alloc]init];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.f],
                                 NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [message boundingRectWithSize:CGSizeMake(207, 999)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes context:nil].size;
    
    label.frame = CGRectMake(10, 5, labelSize.width +20, labelSize.height);
    label.text = message;
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = 1;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:15];
    [showview addSubview:label];
    
    showview.frame = CGRectMake((screenSize.width - labelSize.width - 20)/2,
                                screenSize.height - 300,
                                labelSize.width+40,
                                labelSize.height+10);
    [UIView animateWithDuration:time animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}

- (void) createNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
    
}

- (void) backMainVC:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
