//
//  GoodEvalutionTableViewCell.h
//  极光VPN
//
//  Created by cacac on 2017/4/21.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodEvalutionTableViewCell : UITableViewCell

- (void) setEveryName:(NSArray *)array index:(NSInteger)index;

@end
