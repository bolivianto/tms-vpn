//
//  RoadViewController.h
//   VPN
//
//  Created by cacac on 2017/2/10.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^CountryNameBlock)(NSString *,NSArray *);

@interface RoadViewController : UIViewController

@property (nonatomic,copy) CountryNameBlock ChooseCountryNameBlock;

@property (nonatomic,assign) NSInteger ChannelState;
@end
