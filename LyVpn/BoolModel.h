//
//  BoolModel.h
//   VPN
//
//  Created by cacac on 16/10/21.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BoolModel : NSObject


/*!
 *  @brief YES：选中 NO：没选中
 */
@property (nonatomic,assign) BOOL selectStatus;

@end
