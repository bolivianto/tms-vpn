//
//  ValidateUserViewController.m
//  -VPN
//
//  Created by a on 2017/8/2.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ValidateUserViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "ZiDongConfragitionViewController.h"
#import "UsersViewController.h"
#import "BuysViewController.h"
#import "OKViewController.h"
#import "ACFloatingTextField.h"
#import "VPN-Swift.h"
#import "AFNetworking.h"
#import "KeyChainStore.h"

#undef  WXDispatchQueueRelease
#undef  WXDispatchQueueSetterSementics
#define WXDispatchQueueRelease(q)
#define WXDispatchQueueSetterSementics strong
#undef  WXDispatchQueueRelease
#undef  WXDispatchQueueSetterSementics
#define WXDispatchQueueRelease(q) (dispatch_release(q))
#define WXDispatchQueueSetterSementics assign


@interface ValidateUserViewController ()<UITextFieldDelegate> {
    
    BOOL isShowPassword;
    
    UIButton *bindBtn;
    //绑定失败提示
    UIAlertView *alert;
    UIAlertView *kongAlert;
    UIAlertView *oldAlert;
}

@property IBOutlet UIButton *btnShowPassword;
@property IBOutlet ACFloatingTextField *txtEmail;
@property IBOutlet ACFloatingTextField *txtPassword;

@property (nonatomic, readwrite, strong) NSMutableDictionary* pluginsDict;
@property (nonatomic, readwrite, strong) NSMutableDictionary* settings;
@property (nonatomic, readwrite, strong) NSMutableArray* pluginNames;

@end

@implementation ValidateUserViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Init Show Password
    isShowPassword = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTitle];
}

// Hide/Show password
- (IBAction)showPassword {
    if (isShowPassword) {
        [self.btnShowPassword setImage:[UIImage imageNamed:@"ic_hide_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = YES;
        isShowPassword = NO;
    } else {
        [self.btnShowPassword setImage:[UIImage imageNamed:@"ic_show_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = NO;
        isShowPassword = YES;
    }
}

- (void)initTitle {
    self.navigationItem.title = NSLocalizedString(@"oldUser3", nil);
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)creatMain {
    
    ///logo
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH / 7 , 80*KWidth_Scale, SCREEN_WIDTH / 3.5, SCREEN_WIDTH / 3.5)];
    
    logoImageView.image = [UIImage imageNamed:@"account"];
    
    
    [self.view addSubview:logoImageView];
    
    
    UILabel *remindLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(logoImageView.frame) + 20*KWidth_Scale, SCREEN_WIDTH, 40)];
    
    remindLabel.backgroundColor = [UIColor clearColor];
    
    remindLabel.textColor = bluecOLOR;
    
    remindLabel.textAlignment = 1;
    
    remindLabel.text = NSLocalizedString(@"oldUser7", nil);
    
    remindLabel.font = [UIFont boldSystemFontOfSize:23.0*KWidth_Scale];
    
    remindLabel.numberOfLines = 0;
    
    [self.view addSubview:remindLabel];
    
    UILabel *bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(remindLabel.frame), SCREEN_WIDTH, 40)];
    
    bottomLabel.backgroundColor = [UIColor clearColor];
    
    bottomLabel.textColor = bluecOLOR;
    
    bottomLabel.textAlignment = 1;
    
    bottomLabel.text = NSLocalizedString(@"oldUser16", nil);
    
    bottomLabel.font = [UIFont systemFontOfSize:20.0*KWidth_Scale];
    
    bottomLabel.numberOfLines = 0;
    
    [self.view addSubview:bottomLabel];
    
    
    UILabel *emailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(bottomLabel.frame) + 30*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    emailTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    emailTextLabel.text = NSLocalizedString(@"reglog34", nil);
    emailTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:emailTextLabel];
    
    
    UIImageView *emailTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    emailTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:emailTextImage];
    
    
    //初始化textfield并设置位置及大小
    self.txtEmail = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    //设置边框样式，只有设置了才会显示边框样式
    self.txtEmail.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    self.txtEmail.backgroundColor = [UIColor clearColor];
    
    //文字居中
    self.txtEmail.textAlignment = 1;
    //当输入框没有内容时，水印提示 提示内容为邮箱
    self.txtEmail.placeholder = NSLocalizedString(@"reglog34", nil);
    [self.txtEmail setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    self.txtEmail.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    
    //设置输入框内容的字体样式和大小
    self.txtEmail.font = [UIFont fontWithName:@"Arial" size:16.0f*KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    self.txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    self.txtEmail.clearsOnBeginEditing = NO;
    //设置键盘的样式
    self.txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    self.txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    self.txtEmail.delegate = self;
    //return键变成什么键
    self.txtEmail.returnKeyType = UIReturnKeyDone;
    
    [self.view addSubview:self.txtEmail];
    
    
    
    
    UILabel *passTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(self.txtEmail.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    passTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    passTextLabel.text = NSLocalizedString(@"reglog35", nil);
    passTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:passTextLabel];
    
    
    UIImageView *passTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:passTextImage];
    
    
    //初始化textfield并设置位置及大小
    self.txtPassword = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    //设置边框样式，只有设置了才会显示边框样式
    self.txtPassword.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    self.txtPassword.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    self.txtPassword.placeholder = NSLocalizedString(@"reglog35", nil);
    [self.txtPassword setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    self.txtPassword.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    
    //设置输入框内容的字体样式和大小
    self.txtPassword.font = [UIFont fontWithName:@"Arial" size:16.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    //    passWordText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    self.txtPassword.clearsOnBeginEditing = NO;
    //设置键盘的样式
    self.txtPassword.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    self.txtPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    self.txtPassword.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    self.txtPassword.secureTextEntry = YES;
    //    最右侧加图片是以下代码   左侧类似
    //      文字从中间向两边扩展
    self.txtPassword.textAlignment = 1;
    
    self.txtPassword.returnKeyType = UIReturnKeyNext;
    
    [self.view addSubview:self.txtPassword];
    
    //显示密码
    self.btnShowPassword = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    self.btnShowPassword.center = CGPointMake(CGRectGetMaxX(self.txtPassword.frame) + 15*KWidth_Scale, self.txtPassword.center.y);
    [self.btnShowPassword setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [self.btnShowPassword addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.btnShowPassword];
    
    
    //绑定按钮
    bindBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/6,  CGRectGetMaxY(self.txtPassword.frame) + 50*KWidth_Scale, SCREEN_WIDTH/3*2, 40*KWidth_Scale)];
    
    bindBtn.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    
    [bindBtn setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    
    [bindBtn setTitle:NSLocalizedString(@"oldUser15", nil) forState:UIControlStateNormal];
    
    [bindBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [bindBtn addTarget:self action:@selector(activate) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:bindBtn];
    
    //绑定失败提示
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                       message:NSLocalizedString(@"oldUser9", nil)
                                      delegate:self
                             cancelButtonTitle:nil
                             otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //为空
    kongAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"oldUser11", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //为空
    oldAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                          message:NSLocalizedString(@"oldUser18", nil)
                                         delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    
}

- (IBAction)activate {
    if ([self checkValidation]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
        NSString *md5Pass = [Global getMD5StringWithString:_txtPassword.text];
        NSDictionary *parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": _txtEmail.text, @"mail":self.txtEmail.text, @"pass": md5Pass, @"source": TYPESTRING};
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/RecoveryMember",GONGYOUURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"2"] || [status isEqualToString:@"-1"] || [status isEqualToString:@"0"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"oldUser18", nil)];
                } else {
                    // Successful, store local, enter the main interface
                    // Save To Keychain and UserDefault
                    [NSKeyedArchiver archiveRootObject:objectArray[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:objectArray[0]];
                    // Show OK View Controller
                    [self performSegueWithIdentifier:@"showOkSegue" sender:self];
                    [SVProgressHUD dismiss];
                }
            }
        }];
        [dataTask resume];
        /*
        
        NSURL *url = [NSURL URLWithString:userString];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        
        NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (userdata)
        {
            NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
            
            if (array)
            {
                if ([array[0][@"state"] isEqualToString:@"0"])
                {
                    [oldAlert show];
                }else
                {
                    //绑定成功，信息本地存储，跳转首页
                    [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                    
                    OKViewController *okVC = [[OKViewController alloc]init];
                    
                    okVC.isZidong = _isZidong;
                    
                    [self.navigationController pushViewController:okVC animated:YES];
                }
                
            }else
            {
                [alert show];
            }
        }else
        {
            [alert show];
        }
        */
    }
}

- (BOOL)checkValidation { // Check Validation For Reset Password
    if ([self.txtEmail.text isEqualToString:@""]) { // Empty Email
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if (![Global isValidEmailWithTestStr:self.txtEmail.text]) { // Wrong Email Format
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog20", nil)];
        return NO;
    }
    if ([self.txtPassword.text isEqualToString:@""]) { // Empty Password
        [self.txtPassword showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    return YES;
}


#pragma mark 判断邮箱手机号合法性
- (BOOL)checkPhone:(NSString *)phoneNumber{
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:phoneNumber];
    if (!isMatch)
    {
        return NO;
    }
    return YES;
}

- (BOOL)checkEmail:(NSString *)email{
    NSString *regex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [emailTest evaluateWithObject:email];
}

- (NSString *)MD5:(NSString *)mdStr {
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.txtEmail resignFirstResponder];
    [self.txtPassword resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
