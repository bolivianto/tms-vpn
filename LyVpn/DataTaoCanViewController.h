//
//  DataTaoCanViewController.h
//   VPN
//
//  Created by cacac on 2017/2/13.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Dezignables/Dezignables-Swift.h>

@interface DataTaoCanViewController : UIViewController
    @property (weak, nonatomic) IBOutlet DezignableView *tableViewContainer;
    @property (nonatomic,strong) NSString * TaoCanname;
    @property (nonatomic,strong) NSString * startTime;
    @property (nonatomic,strong) NSString * endTime;
    @property (nonatomic,strong) NSString * restTime;
    @end
