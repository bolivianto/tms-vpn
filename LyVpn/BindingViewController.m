//
//  BindingViewController.m
//   VPN
//
//  Created by a on 2017/8/3.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "BindingViewController.h"
#import <CommonCrypto/CommonDigest.h>
@interface BindingViewController ()<UITextFieldDelegate>
{
    UITextField *emailText;
    UITextField *passText;
    UITextField *conpassText;
    BOOL isLook;
    UIButton *bindingButton;
    UIButton *lookPassBtn;

    //各种弹窗提示
    UIAlertView *alert;
    UIAlertView *kongalert;
    UIAlertView *formatalert;
    UIAlertView *erroralert;
    UIAlertView *againalert;
    UIButton *qukLoginBtn;
    UIButton *oldUserBtn;
//    NSString *userName;
    NSString *passWord;
    NSString *usermail;
    NSString *userStatus;
    UIView *backView1;
    BOOL isLog;
}
@property (nonatomic,retain) UIImageView * backGroupImageView;
@property (nonatomic,strong) NSMutableDictionary * ADdict;
@property (nonatomic,strong) UIWindow * window;


@end

@implementation BindingViewController
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [emailText resignFirstResponder];
    [passText resignFirstResponder];
    return YES;
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [emailText resignFirstResponder];
    [passText resignFirstResponder];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self creatNavi];
    
    isLog = 1;
    //界面背景
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT + 100)];
    view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:view];
    
    
    //绑定
    [self logView];
}

-(void)creatNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;

    
    if ([[self panduan] isEqualToString:@"chinese"])
    {
        self.navigationItem.title = @"绑定邮箱";
        
    }else
    {
        self.navigationItem.title = @"Binding Email";
    }
     [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
    
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
//显示密码
-(void)lookPass
{
    if (isLook)
    {
        [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        passText.secureTextEntry = YES;
        isLook = 0;
    }else
    {
        [lookPassBtn setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        passText.secureTextEntry = NO;
        isLook = 1;
    }
}

-(void)logView
{
    
    UILabel *emailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,NaviHeight + 50*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    emailTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    emailTextLabel.text = NSLocalizedString(@"reglog34", nil);
    emailTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:emailTextLabel];
    
    
    
    UIImageView *emailTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    emailTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:emailTextImage];

    
    emailText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];

    //设置边框样式，只有设置了才会显示边框样式
    emailText.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    emailText.backgroundColor = [UIColor clearColor];
       emailText.textAlignment = 1;
    //当输入框没有内容时，水印提示 提示内容为邮箱
    emailText.placeholder = NSLocalizedString(@"reglog34", nil);
    [emailText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    emailText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];

    //设置输入框内容的字体样式和大小
    emailText.font = [UIFont fontWithName:@"Arial" size:16.0f*KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    emailText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    emailText.clearsOnBeginEditing = NO; 
    //设置键盘的样式
    emailText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    emailText.delegate = self;
      emailText.returnKeyType = UIReturnKeyDone;
    
    [self.view addSubview:emailText];

    
    
    
    UILabel *passTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailText.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    passTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    passTextLabel.text = NSLocalizedString(@"userMess10", nil);
    passTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:passTextLabel];
    
    
    UIImageView *passTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:passTextImage];

    //密码
    passText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];

    //设置边框样式，只有设置了才会显示边框样式
    passText.borderStyle = UITextBorderStyleNone;
    //      文字从中间向两边扩展
    passText.textAlignment = 1;
      //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    passText.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    passText.placeholder = NSLocalizedString(@"userMess10", nil);
    [passText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    passText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    
    //设置输入框内容的字体样式和大小
    passText.font = [UIFont fontWithName:@"Arial" size:16.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    passText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    passText.clearsOnBeginEditing = NO; 
    //设置键盘的样式
    passText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    passText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    passText.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    passText.secureTextEntry = YES;
    //最右侧加图片是以下代码   左侧类似
       //return键变成什么键
    passText.returnKeyType = UIReturnKeyDone;
    
    [self.view addSubview:passText];


    //显示密码
    lookPassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn.center = CGPointMake(CGRectGetMaxX(passText.frame) + 15*KWidth_Scale, passText.center.y);
    [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:lookPassBtn];    
    isLook = 0;
    
    //绑定按钮
    bindingButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/6, CGRectGetMaxY(passText.frame) + 20*KWidth_Scale, SCREEN_WIDTH/3*2, 40*KWidth_Scale)];

    bindingButton.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    
    [bindingButton setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    
    [bindingButton setTitle:NSLocalizedString(@"other10", nil) forState:UIControlStateNormal];
    
    [bindingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [bindingButton addTarget:self action:@selector(resetPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:bindingButton];
    
       
    //网络异常提示
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                       message:NSLocalizedString(@"reglog18", nil)
                                      delegate:self
                             cancelButtonTitle:nil
                             otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //格式不正确提示
    kongalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"reglog7", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //账号或密码不正确
    erroralert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"reglog19", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //邮箱格式不正确
    formatalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                             message:NSLocalizedString(@"reglog20", nil)
                                            delegate:self
                                   cancelButtonTitle:nil
                                   otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //不能重复注册
    againalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"reglog21", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    
    
}



//判断语言
- (NSString*)getPreferredLanguage
{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
    return preferredLang;
    
}
- (NSMutableDictionary *)ADdict
{
    if (_ADdict == nil) {
        _ADdict = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return _ADdict;
}
//注册登录切换
-(void)regButtonAC
{
    if (![emailText.text isEqualToString:@""] && ![passText.text isEqualToString:@""])
    {
        //判断是否为邮箱
        if ([self checkEmail:emailText.text])
        {
            NSString *md5Pass = [self MD5:passText.text];
            NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserReg?apikey=lygames_0953&uuid=%@&name=%@&mail=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],_userName,emailText.text,md5Pass,TYPESTRING];
            NSURL *url = [NSURL URLWithString:userString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
            NSURLResponse *response = nil;
            NSError *error = nil;
            NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (userdata)
            {
                NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil]; 
                if (array.count > 0)
                {
                    userStatus = array[0][@"state"];
                    if ([userStatus isEqualToString:@"0"])
                    {
                        //注册失败，提醒用户手机或邮箱不能重复注册
                        [againalert show];
                        
                    }else if ([userStatus isEqualToString:@"-1"])
                    {
                        //apikey输入错误
                        [alert show];
                    }else
                    {
                        
                        //绑定成功，存储本地，进入主界面
                        [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                        self.tabBarController.tabBar.hidden = NO;
                        //pop到指定界面
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                }else
                {
                    //解析失败，请求重新解析
                    [alert show];                    
                }
            }else
            {
                //请求失败，网络加载失败，重新加载
                [alert show];
            }
        }else
        {
            //格式不正确,提示用户
            [formatalert show];
        }
    }else
    {
        //输入为空，提示用户
        [kongalert show];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark 判断邮箱手机号合法性
- (BOOL)checkPhone:(NSString *)phoneNumber
{
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:phoneNumber];
    if (!isMatch)
    {
        return NO;
    }
    return YES;
}

- (BOOL)checkEmail:(NSString *)email{
    
    NSString *regex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [emailTest evaluateWithObject:email];
}

- (NSString *)MD5:(NSString *)mdStr
{
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}
- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        return @"chinese";
    }else{
        lang = @"en";
        return @"English";
    }
}
-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}


@end
