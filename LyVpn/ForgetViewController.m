//
//  ForgetViewController.m
//   VPN
//
//  Created by a on 2017/7/25.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "ForgetViewController.h"
#import "CLAlertView.h"
#import "ACFloatingTextField.h"
#import "VPN-Swift.h"
#import "AFNetworking.h"
#import "KeyChainStore.h"

@interface ForgetViewController ()<UITextFieldDelegate> {
    BOOL isShowPassword;
}

@property IBOutlet UIButton *showPasswordButton;
@property IBOutlet UIButton *verifyCodeButton;
@property IBOutlet ACFloatingTextField *emailText;
@property IBOutlet ACFloatingTextField *passwordText;
@property IBOutlet ACFloatingTextField *verifyCodeText;

@property (nonatomic,strong) NSMutableDictionary * ADdict;
@end

@implementation ForgetViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.email) {
        self.emailText.text = self.email;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Show Hide
    isShowPassword = NO;
    [self initTitle];
}

- (void)initTitle {
    self.navigationItem.title = NSLocalizedString(@"btnResetPasswordKey", nil);
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

// Hide/Show password
- (IBAction)showPassword {
    if (isShowPassword) {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_hide_password"] forState:UIControlStateNormal];
        self.passwordText.secureTextEntry = YES;
        isShowPassword = NO;
    } else {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_show_password"] forState:UIControlStateNormal];
        self.passwordText.secureTextEntry = NO;
        isShowPassword = YES;
    }
}

- (NSMutableDictionary *)ADdict {
    if (_ADdict == nil) {
        _ADdict = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return _ADdict;
}

//获取验证码
- (IBAction)verifyCodeAction {
    if ([self checkValidationVerifyCode]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
        NSDictionary *parameters = @{@"apikey": @"lygames_0953", @"mail": self.emailText.text};
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/VerifyMailbox",GONGYOUURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"0"] || [status isEqualToString:@"-1"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"reglog26", nil)];
                } else {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"reglog27", nil)];
                }
            }
        }];
        [dataTask resume];
    }
}

- (BOOL)checkValidation { // Check Validation For Reset Password
    if ([self.emailText.text isEqualToString:@""]) { // Empty Email
        [self.emailText showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if (![Global isValidEmailWithTestStr:self.emailText.text]) { // Wrong Email Format
        [self.emailText showErrorWithText:NSLocalizedString(@"reglog20", nil)];
        return NO;
    }
    if ([self.passwordText.text isEqualToString:@""]) { // Empty Password
        [self.passwordText showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if ([self.verifyCodeText.text isEqualToString:@""]) { // Empty Code Verification
        [self.verifyCodeText showErrorWithText:NSLocalizedString(@"reglog38", nil)];
        return NO;
    }
    return YES;
}

- (BOOL)checkValidationVerifyCode { // Check Validation For Verify
    if ([self.emailText.text isEqualToString:@""]) { // Empty Email
        [self.emailText showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if (![Global isValidEmailWithTestStr:self.emailText.text]) { // Wrong Email Format
        [self.emailText showErrorWithText:NSLocalizedString(@"reglog20", nil)];
        return NO;
    }
    return YES;
}

// 重置密码
- (IBAction)resetPassword { // Do Reset Password
    if ([self checkValidation]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
        NSString *md5Pass = [Global getMD5StringWithString:self.passwordText.text];
        NSDictionary *parameters = @{@"apikey": @"lygames_0953", @"mail": self.emailText.text, @"pass": md5Pass, @"vc": self.verifyCodeText.text};
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/ModifyPassword",GONGYOUURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"0"] || [status isEqualToString:@"-1"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"reglog29", nil)];
                } else {
                    // Successful, store local, enter the main interface
                    // Save To Keychain and UserDefault
                    [NSKeyedArchiver archiveRootObject:objectArray[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:objectArray[0]];
                    // Show Selection Alert
                    CLAlertView *alertView = [[CLAlertView alloc] initWithAlertViewWithTitle:NSLocalizedString(@"reglog6", nil) text:NSLocalizedString(@"reglog30", nil) DefauleBtn:NSLocalizedString(@"reglog31", nil) cancelBtn:NSLocalizedString(@"reglog32", nil) defaultBtnBlock:^(UIButton *defaultBtn) {
                        // Go To Auto Login
                        // Show Main Storyboard
                        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UINavigationController *mainNav = [sb instantiateInitialViewController];
                        if (self.navigationController.isBeingPresented) {
                            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                [SVProgressHUD dismiss];
                            }];
                        } else if (self.navigationController.presentedViewController == nil) {
                            [self.navigationController presentViewController:mainNav animated:YES completion:^{
                                [SVProgressHUD dismiss];
                            }];
                        }
                    } cancelBtnBlock:nil];
                    [alertView show];
                }
            }
        }];
        [dataTask resume];
    }
}

#pragma mark --判断手机号合法性
- (BOOL)checkPhone:(NSString *)phoneNumber{
    
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    BOOL isMatch = [pred evaluateWithObject:phoneNumber];
    
    if (!isMatch){
        return NO;
        
    }
    
    return YES;
}
#pragma mark 判断邮箱

- (NSString *)panduan {
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound) {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}

- (NSString*)currentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

@end
