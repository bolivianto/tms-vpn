//
//  BaseViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "BaseViewController.h"
#import "HelpViewController.h"
#import "EnglishViewController.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self CreateHelpController];
    
}

- (void) CreateHelpController
{
//    UIButton * buttn = [UIButton buttonWithType:UIButtonTypeCustom];
//    buttn.frame = CGRectMake(0, 0, 40, 40);
//    [buttn setImage:[UIImage imageNamed:@"yellow_question"] forState:UIControlStateNormal];
//    [buttn addTarget:self action:@selector(JoinInHelpController) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithCustomView:buttn];
//    self.navigationItem.rightBarButtonItem = search;
}


- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
//        NSLog(@"current Language == Chinese");
        return @"chinese";
    }
    else{
        lang = @"en";
//        NSLog(@"current Language == English");
        return @"English";
    }
}

-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

- (void) JoinInHelpController
{
    if ([[self panduan] isEqualToString:@"chinese"]) {
        HelpViewController * helpVC = [[HelpViewController alloc] init];
        helpVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:helpVC animated:YES];
    }
    else
    {
        EnglishViewController * engVC = [[EnglishViewController alloc] init];
        engVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:engVC animated:YES];
    }
    
    
//    if (IS_IPHONE) {
//        NSArray *languages = [NSLocale preferredLanguages];
//        NSString *currentLanguage = [languages objectAtIndex:0];
////        NSLog(@">>>>>%@",currentLanguage);
//        if ([currentLanguage isEqualToString:@"zh-Hans-CN"]) {
//            HelpViewController * helpVC = [[HelpViewController alloc] init];
//            helpVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:helpVC animated:YES];
//        }
//        else
//        {
//            EnglishViewController * engVC = [[EnglishViewController alloc] init];
//            engVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:engVC animated:YES];
//        }
//    }
//    else{
//        NSArray * languages = [NSLocale preferredLanguages];
//        NSString *currentLanguage = [languages objectAtIndex:0];
////        NSLog(@">>>>>%@",currentLanguage);
//        if ([currentLanguage isEqualToString:@"zh-Hans"]) {
//            HelpViewController * helpVC = [[HelpViewController alloc] init];
//            helpVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:helpVC animated:YES];
//        }
//        else
//        {
//            EnglishViewController * engVC = [[EnglishViewController alloc] init];
//            engVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:engVC animated:YES];
//        }
//    }
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}
@end
