//
//  DueTimeTableViewCell.h
//  VPN
//
//  Created by 王树超 on 17/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DueTimeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblKey;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@end
