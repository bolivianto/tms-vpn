//
//  ChangeTimeString.h
//   VPN
//
//  Created by cacac on 2017/2/16.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangeTimeString : NSObject

//获取精确时间
+  (NSString *) DataChangeTimeString:(NSString *)time;

//获取年月日即可
+ (NSString *) GetYearMonthDayString:(NSString *)time;

+ (NSString *)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime;

//得到当前时间
+ (NSString *)getCurrentTime;

+ (long)getZiFuChuan:(NSString*)time;
+ (long)getZiFuChuanFromDate:(NSDate*)time;

@end
