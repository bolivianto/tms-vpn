//
//  JiLuTableViewCell.h
//   VPN
//
//  Created by cacac on 2017/3/10.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JiLuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bianhaolabel;
@property (weak, nonatomic) IBOutlet UILabel *pricelabel;
@property (weak, nonatomic) IBOutlet UILabel *timelabel;
@property (weak, nonatomic) IBOutlet UILabel *datelabel;

@end
