//
//  ConnectVPNManager.h
//   VPN
//
//  Created by 王树超 on 2018/3/7.
//  Copyright © 2018年 cacac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    VPN_IKEv2,
    VPN_IPSEC,
} VPNTYPE;

@class routeModel;
@interface ConnectVPNManager : NSObject
@property(nonatomic,strong)routeModel* currentRouteModel;//当前线路
@property(nonatomic,assign)VPNTYPE currentVPN;//当前VPN类型
@property(nonatomic,strong)NSMutableArray *routeArrary;//备用线路数组；
@property(nonatomic,assign)int linkTime;//连接次数（点击链接按钮以后的）
+(instancetype)sharedManager;
//通过本地的国家名称获取线路和密码
-(void)GetVPNDataWithAreaName;
//不让连接
-(void)Repeat;
//设置当前线路
-(void)setCurrentRouteWith:(NSArray *)array;

//处理链接成功的情况
-(void)dealConnectedState;
//处理未链接的状态
-(BOOL)dealDisConnectedState;

//加载备用线路
-(void)loadSpareRoute;



@end
