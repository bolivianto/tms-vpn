//
//  UUID.h
//  DouBan
//
//  Created by 乐游先锋 on 16/5/24.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UUID : NSObject

+ (NSString *)getUUID;

@end
