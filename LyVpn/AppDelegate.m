//
//  AppDelegate.m
//   VPN
//
//  Created by cacac on 2017/2/8.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "AppDelegate.h"
#import "VPN-Swift.h"
#import "AFNetworking.h"
#import "JInPayController.h"
#import "KeyChainStore.h"
#import "ChangeTimeString.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <UIKit/UIKit.h>
#import <DPLocalization/DPLocalization.h>

@import TrueTime;

@interface AppDelegate() {
    NSMutableArray *domainNameList;
    int ipNum;
    int three;
    TrueTimeClient *ttClient;
}

@property (nonatomic,strong) NSMutableDictionary * ADdata;
@property (nonatomic,strong) NSArray * domainnameArray;
@property (nonatomic,strong) NSMutableArray * ipArray;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Set Language
    dp_set_current_language(@"id");
    // Init TrueTime
    ttClient = [TrueTimeClient sharedInstance];
    [ttClient startWithHostURLs:@[[NSURL URLWithString:@"time.apple.com"]]];
    
    // Init Keyboard Avoid
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    self.window = [[UIApplication sharedApplication] delegate].window;
    _ADdata = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    
    // Init UMSocial Data
    [UMSocialData setAppKey:@"507fcab25270157b37000010"];
    [UMSocialWechatHandler setWXAppId:@"wx913ab754af0a1c7a" appSecret:@"ea9cde8d70a7200c70ea190dfb183ebf" url:nil];
    UMConfigInstance.appKey = @"58ac032cb27b0a5518000636";
    UMConfigInstance.channelId = @"穿梭 VPN";
    
    // Call this method to initialize the SDK after configuring the above parameters!
    [MobClick startWithConfigure:UMConfigInstance];
    
    // Getting List Domain Name, Locally
    domainNameList = [NSMutableArray arrayWithCapacity:0];
    NSArray *iparray = [NSKeyedUnarchiver unarchiveObjectWithFile:ALLDOMAINDATA];
    if (!iparray || iparray.count == 0) {
        // If not, take it from the directory and take it all encrypted
        NSString *path = [[NSBundle mainBundle] pathForResource:@"DomainName.plist" ofType:nil];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        domainNameList = [NSMutableArray arrayWithArray:array];
        [NSKeyedArchiver archiveRootObject:domainNameList toFile:ALLDOMAINDATA];
    } else {
        domainNameList = [NSMutableArray arrayWithArray:iparray];
        [Global printLogWithObject:domainNameList function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    }
    
    // Navbar Appearance
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"toumingtu"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage imageNamed:@"toumingtu"]];
    [[UINavigationBar appearance] setTranslucent:YES];
    
    // Init VungleSDK
    NSString* appID = @"58dc743bcf842f7148001760";
    VungleSDK* sdk = [VungleSDK sharedSDK];
    [sdk startWithAppId:appID];
    
    // Check App Version
    [self getAppVersion];
    
    return YES;
}

- (void)getAppVersion {
    NSString * pathUrl = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetVersionInfo?apikey=lygames_0953",[[NSString alloc]initWithData:[[NSData alloc]initWithBase64EncodedString:[desFile decryptWithText:domainNameList[ipNum]] options:0] encoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:pathUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (data) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if (array) {
            [Global printLogWithObject:array function:[NSString stringWithFormat:@"%s", __FUNCTION__]];
            if (array[0][@"LinkVersion"]) {
                [[NSUserDefaults standardUserDefaults] setObject:domainNameList[ipNum] forKey:@"domain"];
                NSArray *iparray = [NSKeyedUnarchiver unarchiveObjectWithFile:LinkVersion];
                if (!iparray || iparray.count == 0) {
                    //如果没有，从目录中拿，拿的全是加密的
                    // If not, take it from the directory and take it all encrypted
                    NSString *path = [[NSBundle mainBundle] pathForResource:@"LinkVersion.plist" ofType:nil];
                    NSArray *array = [NSArray arrayWithContentsOfFile:path];
                    iparray = [NSMutableArray arrayWithArray:array];
                    [NSKeyedArchiver archiveRootObject:iparray toFile:LinkVersion];
                }
                
                NSString *local = iparray[0];
                NSString *service = array[0][@"LinkVersion"];
                if (![local isEqualToString:service]) {
                    [NSKeyedArchiver archiveRootObject:@[service] toFile:LinkVersion];
                    //子线程更新域名池
                    // Child thread updates the domain name pool
                    NSThread *myDomainThread=[[NSThread alloc]initWithTarget:self selector:@selector(getDomain) object:nil];
                    myDomainThread.name=[NSString stringWithFormat:@"myDomain"];//设置线程名称
                    [myDomainThread start];
                }
                // 用本地域名池请求
                // Use a local domain pool request
                [self goToMainPage];
            } else {
                [self loopRequest];
            }
        } else {
            [self loopRequest];
        }
    } else {
        [self loopRequest];
    }
}

// Loop Request
-(void)loopRequest {
    if (ipNum < domainNameList.count - 1) {
        ipNum++;
        [self getAppVersion];
    } else {
        if (three < 2) {
            NSArray *link3 = [NSKeyedUnarchiver unarchiveObjectWithFile:LINK3];
            ipNum = 0;
            three++;
            if (link3[three - 1] && ![link3[three - 1] isKindOfClass:[NSNull class]]) {
                [self ThirdPartyPlatform:[[NSString alloc]initWithData:[[NSData alloc] initWithBase64EncodedString:[desFile decryptWithText:link3[three - 1]] options:0] encoding:NSUTF8StringEncoding]];
            } else {
                //link3[0]
                if (three - 1 == 0) {
                    [self ThirdPartyPlatform:url_Blog];
                } else {
                    [self ThirdPartyPlatform:url_txt];
                }
            }
        } else {
            [self goToMainPage];
        }
    }
}

- (void)goToMainPage {
    // 第几个域名，需要记录，后续的请求都用这个域名----用的时候注意取出来加密
    // [[NSUserDefaults standardUserDefaults] setObject:damainArray[ipNum] forKey:@"domain"];
    // 广告开关获取
    [self getAdMessageData];
    
    // Init In Pay
    [[JInPayController sharedInstanceMethod] initInPay];
    // GetIP (child thread)
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status != AFNetworkReachabilityStatusNotReachable) {
            // Start Thread Get IP
            [Global printLogWithObject:@"Start Thread Get User IP" function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(getIP) object:nil];
            thread.name = [NSString stringWithFormat:@"myThread"]; // 设置线程名称
            [thread start];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 加载主界面
    NSDictionary *userData = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    NSDictionary *keychainUserData = [KeyChainStore load:KEY_USER_LOGINDATA];
    if (userData || keychainUserData) {
        // Load Main UI
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.window.rootViewController = [sb instantiateInitialViewController];
        [self.window makeKeyAndVisible];
    } else {
        // Load Main UI
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        self.window.rootViewController = [sb instantiateInitialViewController];
        [self.window makeKeyAndVisible];
    }
}

//更新域名池
// Update the domain name pool
- (void)getDomain {
    //先解密，再拼接
    // Decrypt first, then stitch
    NSString * str1 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetLinkvp?apikey=lygames_0953",[[NSString alloc]
                                                                                                          initWithData:[[NSData alloc]
                                                                                                                        initWithBase64EncodedString:[desFile decryptWithText:domainNameList[ipNum]] options:0] encoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:str1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (data) {
        //请求的数据再解密
        // Requested data decryption
        NSString *str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSData *nsdataFromBase64String = [[NSData alloc]
                                          initWithBase64EncodedString:[desFile decryptWithText:str] options:0];
        NSArray * array = [NSJSONSerialization JSONObjectWithData:nsdataFromBase64String options:NSJSONReadingMutableContainers error:nil];
        NSMutableArray *getIp = [NSMutableArray arrayWithCapacity:0];
        // 然后按照格式加密存到本地
        // Then encrypt it according to the format and store it locally
        for (int i = 0; i < array.count; i++) {
            [getIp addObject:[desFile encryptWithText:[[array[i][@"LinkUrl"]
                                                        dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0]]];
        }
        if (getIp.count > 0) {
            [NSKeyedArchiver archiveRootObject:getIp toFile:ALLDOMAINDATA];
        } else {
            // 如果请求的域名池为空，用原来的域名池
            // If the requested domain name pool is empty, use the original domain name pool.
            [NSKeyedArchiver archiveRootObject:domainNameList toFile:ALLDOMAINDATA];
        }
    } else {
        // 如果解析失败，用原来的域名池
        // If the parsing fails, use the original domain name pool
        [NSKeyedArchiver archiveRootObject:domainNameList toFile:ALLDOMAINDATA];
    }
}

- (void)ThirdPartyPlatform:(NSString *)link {
    if (three == 1)
    {
        // 第一种方式
        // First Way
        NSString *String0 = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]] encoding:NSUTF8StringEncoding error:nil];
        
        if (String0 && [String0 rangeOfString:@"········#######"].location != NSNotFound) {
            NSRange startRange = [String0 rangeOfString:@"········#######"];
            NSRange endRange = [String0 rangeOfString:@"#######......."];
            NSRange range = NSMakeRange(startRange.location + startRange.length, endRange.location - startRange.location - startRange.length);
            NSString *result = [String0 substringWithRange:range];
            NSString *strUrl = [result stringByReplacingOccurrencesOfString:@"<wbr>" withString:@""];
            NSData *nsdataFromBase64String12111 = [[NSData alloc]
                                                   initWithBase64EncodedString:[desFile decryptWithText:strUrl] options:0];
            
            if (nsdataFromBase64String12111) {
                NSArray * array0 = [NSJSONSerialization JSONObjectWithData:nsdataFromBase64String12111 options:NSJSONReadingMutableContainers error:nil];
                if (array0.count > 0) {
                    [domainNameList removeAllObjects];
                    for (int i = 0; i < array0.count; i++)
                    {
                        [domainNameList addObject:[desFile encryptWithText:[[array0[i][@"LinkUrl"]
                                                                             dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0]]];
                    }
                    [NSKeyedArchiver archiveRootObject:domainNameList toFile:ALLDOMAINDATA];
                    [self getAppVersion];
                } else {
                    [self getAppVersion];
                }
            } else {
                [self getAppVersion];
            }
        } else {
            [self getAppVersion];
        }
    } else {
        //第二中方式
        // Second Way
        NSString *String = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]] encoding:NSUTF8StringEncoding error:nil];
        
        if (String && [String rangeOfString:@"........#######"].location != NSNotFound) {
            NSRange startRange = [String rangeOfString:@"........#######"];
            NSRange endRange = [String rangeOfString:@"#######......."];
            NSRange range = NSMakeRange(startRange.location + startRange.length, endRange.location - startRange.location - startRange.length);
            NSString *result = [String substringWithRange:range];
            NSData *nsdataFromBase64String12 = [[NSData alloc]
                                                initWithBase64EncodedString:[desFile decryptWithText:result] options:0];
            if (nsdataFromBase64String12)
            {
                NSArray * array0 = [NSJSONSerialization JSONObjectWithData:nsdataFromBase64String12 options:NSJSONReadingMutableContainers error:nil];
                if (array0.count > 0)
                {
                    [domainNameList removeAllObjects];
                    for (int i = 0; i < array0.count; i++)
                    {
                        [domainNameList addObject:[desFile encryptWithText:[[array0[i][@"LinkUrl"]
                                                                             dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0]]];
                    }
                    [NSKeyedArchiver archiveRootObject:domainNameList toFile:ALLDOMAINDATA];
                    [self getAppVersion];
                }else
                {
                    [self getAppVersion];
                }
            } else {
                [self getAppVersion];
            }
        } else {
            [self getAppVersion];
        }
    }
}

- (void)getIP {
    NSString * str123 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetuserIP?apikey=lygames_0953&source=%@", GONGYOUURL, TYPESTRING];
    NSURL *url123 = [NSURL URLWithString:str123];
    NSURLRequest *request = [NSURLRequest requestWithURL:url123 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *ADdata11 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    [Global printLogWithObject:[NSJSONSerialization JSONObjectWithData:ADdata11 options:NSJSONReadingMutableContainers error:nil] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
}

- (void)getAdMessageData {
    NSString * str1 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetVpnjsonBYType?apikey=lygames_0953&type=%@",GONGYOUURL,TYPESTRING];
    NSURL *url = [NSURL URLWithString:str1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (userdata) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        if (array) {
            [Global printLogWithObject:array function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            _ADdata = array[0];
            [NSKeyedArchiver archiveRootObject:array[0] toFile:AllADData];
            // 将第三方链接存到本地
            // Simpan tautan pihak ketiga ke lokal Anda
            NSArray *link3 = @[_ADdata[@"UpdateAdd"],_ADdata[@"UpdateAdd1"]];
            [NSKeyedArchiver archiveRootObject:link3 toFile:LINK3];
            [self performSelectorInBackground:@selector(downloadImage:) withObject:_ADdata];
        }
    }
}

- (void)downloadImage:(NSDictionary *)ADdata {
    NSString *image1Str = ADdata[@"xwyap"];
    NSString *image2Str = ADdata[@"layap"];
    NSString *urlStr = ADdata[@"yapurl"];
    NSString *urlStr2 = ADdata[@"yapurlxw"];
    [[NSUserDefaults standardUserDefaults] setObject:urlStr forKey:@"yapurl"];
    [[NSUserDefaults standardUserDefaults] setObject:urlStr2 forKey:@"yapurlxw"];
    
    if (image1Str && image1Str.length > 1) {
        NSString *enCod = [image1Str stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLHostAllowedCharacterSet];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:enCod]];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"image1"];
    }
    
    if (image1Str && image2Str.length > 1) {
        NSString *enCod = [image2Str stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLHostAllowedCharacterSet];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:enCod]];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"image2"];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"ppppp"] isEqualToString:@"1"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshpaydata" object:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ppppp"];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

@end

