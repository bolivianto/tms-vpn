//
//  ConnectPopView.m
//  极光VPN
//
//  Created by cacac on 2017/4/20.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "ConnectPopView.h"

@interface ConnectPopView()<GADNativeExpressAdViewDelegate, GADVideoControllerDelegate>
{
    UILabel * PopLabel;
    UIImageView * PopimageView;
    UIButton * YesButton;
    UIButton * CancelButton;
}
@property (nonatomic,retain) GADNativeExpressAdView *nativeExpressAdView;
@end

@implementation ConnectPopView

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    return self;
}


- (void) setup:(UIViewController *)controller
{
    
    self.backgroundColor = [UIColor whiteColor];
        
    
    //原生广告
    self.nativeExpressAdView = [[GADNativeExpressAdView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(PopLabel.frame), self.frame.size.width, self.frame.size.height)];
    self.nativeExpressAdView.adUnitID = @"ChooseRoadKey";
    self.nativeExpressAdView.rootViewController = controller;
    self.nativeExpressAdView.delegate = self;
    
    // The video options object can be used to control the initial mute state of video assets.
    // By default, they start muted.
    GADVideoOptions *videoOptions = [[GADVideoOptions alloc] init];
    videoOptions.startMuted = true;
    [self.nativeExpressAdView setAdOptions:@[ videoOptions ]];
    
    // Set this UIViewController as the video controller delegate, so it will be notified of events
    // in the video lifecycle.
    self.nativeExpressAdView.videoController.delegate = self;
    GADRequest *request = [GADRequest request];
    [self.nativeExpressAdView loadRequest:request];
    [self addSubview:self.nativeExpressAdView];
    
    
      
}
#pragma mark - GADNativeExpressAdViewDelegate
- (void)nativeExpressAdViewDidReceiveAd:(GADNativeExpressAdView *)nativeExpressAdView {
    if (nativeExpressAdView.videoController.hasVideoContent) {
        //        NSLog(@"Received ad an with a video asset.");
    } else {
        //        NSLog(@"Received ad an without a video asset.");
    }
}

#pragma mark - GADVideoControllerDelegate
- (void)videoControllerDidEndVideoPlayback:(GADVideoController *)videoController {
    //    NSLog(@"Playback has ended for this ad's video asset.");
}

- (void) releaseNativeView
{
    self.nativeExpressAdView = nil;
    self.nativeExpressAdView.videoController.delegate = nil;
}
@end
