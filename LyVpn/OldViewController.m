//
//  OldViewController.m
//  -VPN
//
//  Created by a on 2017/8/1.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "OldViewController.h"
#import "ValidateUserViewController.h"
#import "ACFloatingTextField.h"
#import "AFNetworking.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"

@interface OldViewController ()<UITextFieldDelegate> {
    UIAlertView *againAlert;
    UIAlertView *errorAlert;
    UIAlertView *kongAlert;
}

@property IBOutlet ACFloatingTextField *txtUsername;
@property (nonatomic, readwrite, copy) NSString* configFile;
@property (nonatomic, readwrite, strong) NSArray *pluginNames;
@property (nonatomic, readwrite, strong) NSDictionary* settings;
@property (nonatomic,strong) NSMutableDictionary * ADdata;

@end

@implementation OldViewController
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (NSString *) panduan {
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        return @"chinese";
    }else{
        lang = @"en";
        return @"English";
    }
}

- (NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    _ADdata = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    [self initTitle];
}


-(void)creatMain {
    //    一段文字描述
    UILabel *statementLabel = [[UILabel alloc]initWithFrame:CGRectMake(50*KWidth_Scale, CGRectGetMaxY(self.txtUsername.frame) + 40*KWidth_Scale, SCREEN_WIDTH - 100*KWidth_Scale, 100)];
    
    statementLabel.backgroundColor = [UIColor clearColor];
    statementLabel.font = [UIFont systemFontOfSize:16*KWidth_Scale];
    statementLabel.text = @"作为本款VPN产品的开发者，我们非常真挚的感谢每一位用户的支持与厚爱，对于广告的投放，程序猿童鞋深感歉意，但是实在没有办法，开发费用，运营费用，以及每月循环投入的服务器费用，真的让我们在永久免费的道路上举步维艰，但是请放心，我们一定会一直坚持下去的！感谢理解！";
    statementLabel.text = _ADdata[@"ConcernMessage"];
    statementLabel.textAlignment = 1;
    statementLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    statementLabel.numberOfLines = 0;
    [statementLabel sizeToFit];
    [self.view addSubview:statementLabel];
    
    //下一步按钮
    UIButton *nextBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/6, CGRectGetMaxY(statementLabel.frame) + 50*KWidth_Scale, SCREEN_WIDTH / 3*2, 40*KWidth_Scale)];
    
    [nextBtn setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    [nextBtn setTitle:NSLocalizedString(@"oldUser4", nil) forState:UIControlStateNormal];
    
    [nextBtn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextBtn];
    
    //请求失败
    againAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"oldUser5", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //不是老用户
    errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"oldUser6", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //空
    kongAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"oldUser10", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
}

- (NSURL *)jsBundldddeURL {
    if (!self.settings) {
        return nil;
    }
    NSURL *jsBundleUrl = nil;
    if (self.settings[@"launch_locally"] && [self.settings[@"launch_locally"] boolValue]) {
        NSString *jsFile = self.settings[@"local_url"];
        if (jsFile && ![jsFile isEqualToString:@""]) {
            jsBundleUrl = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@/bundlejs/%@",[NSBundle mainBundle].bundlePath,jsFile]];
        }else {
            jsBundleUrl = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@/bundlejs/index.js",[NSBundle mainBundle].bundlePath]];
        }
    } else {
        NSString *hostAddress = self.settings[@"launch_url"];
        if (hostAddress && ![hostAddress isEqualToString:@""]) {
            jsBundleUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8080/dist/index.js",hostAddress]];
        }else {
            jsBundleUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8080/dist/index.js", [self getPackageHost]]];
        }
    }
    return jsBundleUrl;
}

- (NSString *)getPackageHost {
    static NSString *ipGuess;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *ipPath = [[NSBundle mainBundle] pathForResource:@"ip" ofType:@"txt"];
        ipGuess = [[NSString stringWithContentsOfFile:ipPath encoding:NSUTF8StringEncoding error:nil]
                   stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    });
    
    NSString *host = ipGuess ?: @"localhost";
    return host;
}

- (void)initTitle {
    /*
     //创建导航栏左按钮   用于编辑
     UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     leftBtn.frame = CGRectMake(0, 0,30, 30);
     [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
     if (IS_IPHONEX) {
     [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
     }
     [leftBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
     self.navigationItem.leftBarButtonItem = leftBarButtonItem;
     
     if ([[self panduan] isEqualToString:@"chinese"])
     {
     self.navigationItem.title = @"老用户";
     
     }else
     {
     self.navigationItem.title = @"old user";
     }*/
    
    [self.navigationItem setTitle:NSLocalizedString(@"oldUser3", nil)];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

-(void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)next {
    if ([self.txtUsername.text isEqualToString:@""]) {
        [self.txtUsername showErrorWithText:NSLocalizedString(@"oldUser10", nil)];
    } else {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
        NSDictionary *parameters = @{@"apikey": @"lygames_0953", @"name": self.txtUsername.text};
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/ValidateUser",GONGYOUURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"2"] || [status isEqualToString:@"-1"] || [status isEqualToString:@"0"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"oldUser6", nil)];
                } else {
                    [self performSegueWithIdentifier:@"showValidateUserSegue" sender:self];
                    [SVProgressHUD dismiss];
                }
            }
        }];
        [dataTask resume];
        /*
         NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/ValidateUser?apikey=lygames_0953&name=%@", GONGYOUURL, self.txtUsername.text];
         NSURL *url = [NSURL URLWithString:userString];
         NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
         NSURLResponse *response = nil;
         NSError *error = nil;
         NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
         if (userdata) {
         NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
         NSLog(@"%@", array[0]);
         if (array) {
         if ([array[0][@"state"] isEqualToString:@"1"])
         {
         //是老用户,下一步
         ValidateUserViewController *valiVC = [[ValidateUserViewController alloc]init];
         valiVC.userName = self.txtUsername.text;
         valiVC.isZidong = _isZidong;
         
         [self.navigationController pushViewController:valiVC animated:YES];
         } else {
         //用户名不存在，提醒
         [errorAlert show];
         }
         } else {
         //解析失败，提醒重新加载
         [againAlert show];
         }
         } else {
         //请求失败，提醒重新加载
         [againAlert show];
         }
         */
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.txtUsername resignFirstResponder];    //主要是[receiver resignFirstResponder]在哪调用就能把receiver对应的键盘往下收
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
