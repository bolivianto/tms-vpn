//
//  routeModel.h
//  Potatso
//
//  Created by Tommy on 2017/12/18.
//  Copyright © 2017年 TouchingApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface routeModel : NSObject

//国家地区
@property(nonatomic,copy)NSString *Area;

//服务器IP
@property(nonatomic,copy)NSString *IP;
//名称
@property(nonatomic,copy)NSString *name;
//密码
@property(nonatomic,copy)NSString *pass;

@property(nonatomic,copy)NSString *remoteId;
@property(nonatomic,copy)NSString *currentTime;

@end
