//
//  NewWork.h
//  AiBook
//
//  Created by 乐游先锋 on 16/5/24.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewWork : NSObject

/// 正确返回数据
typedef void(^HttpSuccess) (id json);
/// 错误返回数据
typedef void(^HttpFailure) (NSError *error);

+ (instancetype)shareNetWork;


+ (void)postWithApi:(NSString *)api parameters:(NSDictionary *)parameters success:(HttpSuccess)success failure:(HttpFailure)failure;


+ (void)getWithApi:(NSString *)api parameters:(NSDictionary *)parameters success:(HttpSuccess)success failure:(HttpFailure)failure;


@end
