//
//  NewWork.m
//  AiBook
//
//  Created by 乐游先锋 on 16/5/24.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import "NewWork.h"
#import "AFNetworking.h"
@implementation NewWork

+ (instancetype)shareNetWork {
    static NewWork *shareNetWork = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        shareNetWork = [[self alloc] init];
    });
    return shareNetWork;
}

+ (void)postWithApi:(NSString *)api parameters:(NSDictionary *)parameters success:(HttpSuccess)success failure:(HttpFailure)failure {
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer]; ///设置返回格式
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/html", nil];
    NSString *url = [NSString stringWithFormat:@"%@", api];
    [manger POST:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}


+ (void)getWithApi:(NSString *)api parameters:(NSDictionary *)parameters success:(HttpSuccess)success failure:(HttpFailure)failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    /// 设置返回格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/html", nil];
    
    NSString *url = [NSString stringWithFormat:@"%@", api];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}



@end
