//
//  main.m
//   VPN
//
//  Created by cacac on 2017/2/8.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
