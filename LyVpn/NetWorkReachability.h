//
//  NetWorkReachability.h
//  Potatso
//
//  Created by apple on 23/9/17.
//  Copyright © 2017年 TouchingApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@interface NetWorkReachability : NSObject
+(BOOL)internetStatus;
@end
