//
//  ZiDongConfragitionViewController.h
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZiDongConfragitionViewController : UIViewController
@property (nonatomic,strong) NSString *isZidong;
@property (nonatomic,strong) NSString *isYouke;
@property (nonatomic,strong) NSString *email;
@property (strong, nonatomic) UIWindow *window;
@end
