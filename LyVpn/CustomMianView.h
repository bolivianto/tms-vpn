//
//  CustomMianView.h
//  极光VPN
//
//  Created by cacac on 2017/4/24.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ConnectDeleAdBlock)();

@interface CustomMianView : UIView

@property (nonatomic,copy) ConnectDeleAdBlock ConnectBlock;

@end
