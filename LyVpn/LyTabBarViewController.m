//
//  LyTabBarViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "LyTabBarViewController.h"
#import "LyNavitionController.h"
#import "ZiDongConfragitionViewController.h"
#import "UsersViewController.h"
#import "BuysViewController.h"
#import "ForeiingViewController.h"
#import "HelpViewController.h"
#import "EnglishViewController.h"
@interface LyTabBarViewController ()

@end

@implementation LyTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = DR_COLOR_COMMON_BG;
    
    self.tabBar.tintColor = bluecOLOR;
    
    self.tabBar.translucent = NO;
    
    ZiDongConfragitionViewController * zidongVC = [[ZiDongConfragitionViewController alloc] init];
    zidongVC.title = NSLocalizedString(@"key4", nil);
//    LyNavitionController * zidongNavi = [[LyNavitionController alloc] initWithRootViewController:zidongVC];
    UINavigationController *zidongNavi = [[UINavigationController alloc] initWithRootViewController:zidongVC];
    zidongNavi.tabBarItem.image = [UIImage imageNamed:@"toolbar_autoconfigure"];
    zidongNavi.tabBarItem.selectedImage = [UIImage imageNamed:@"toolbar_autoconfigure_click"];
//    [zidongNavi.navigationBar setBackgroundImage:[UIImage imageNamed:@"toumingtu"] forBarMetrics:UIBarMetricsDefault];
//    zidongNavi.navigationBar.barStyle = UIBarStyleBlackTranslucent;
//    [[UINavigationBar appearance] setTranslucent:YES];
    
    UsersViewController * UserVC = [[UsersViewController alloc] init];
    UserVC.title = NSLocalizedString(@"key2", nil);
//    LyNavitionController * UserNavi = [[LyNavitionController alloc] initWithRootViewController:UserVC];
    UINavigationController *UserNavi = [[UINavigationController alloc] initWithRootViewController:UserVC];
    UserNavi.tabBarItem.image = [UIImage imageNamed:@"toolbar_account"];
    UserNavi.tabBarItem.selectedImage = [UIImage imageNamed:@"toolbar_account_click"];
    
    BuysViewController * BuyVC = [[BuysViewController alloc] init];
    BuyVC.title = NSLocalizedString(@"key3", nil);
//    LyNavitionController * BuyNavi = [[LyNavitionController alloc] initWithRootViewController:BuyVC];
    UINavigationController *BuyNavi = [[UINavigationController alloc] initWithRootViewController:BuyVC];
    BuyNavi.tabBarItem.image = [UIImage imageNamed:@"toolbar_reward"];
    BuyNavi.tabBarItem.selectedImage = [UIImage imageNamed:@"toolbar_reward_click"];
    HelpViewController * helpVC = [[HelpViewController alloc] init];
    LyNavitionController * helpNavi = [[LyNavitionController alloc] initWithRootViewController:helpVC];
    helpVC.title = NSLocalizedString(@"key6", nil);
    helpNavi.tabBarItem.image = [UIImage imageNamed:@"toolbar_question"];
    helpNavi.tabBarItem.selectedImage = [UIImage imageNamed:@"toolbar_question_click"];
    
    EnglishViewController * enhelpVC = [[EnglishViewController alloc] init];
    LyNavitionController * enhelpNavi = [[LyNavitionController alloc] initWithRootViewController:enhelpVC];
    enhelpVC.title = NSLocalizedString(@"key6", nil);
    enhelpNavi.tabBarItem.image = [UIImage imageNamed:@"toolbar_question"];
    enhelpNavi.tabBarItem.selectedImage = [UIImage imageNamed:@"toolbar_question_click"];
    
    if ([[self panduan] isEqualToString:@"chinese"]) 
    {
        self.viewControllers = @[zidongNavi,BuyNavi,helpNavi,UserNavi];
    }else
    {
        self.viewControllers = @[zidongNavi,BuyNavi,enhelpVC,UserNavi];
    }
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:80.0/255.0 green:165.0/255.0 blue:94.0/255.0 alpha:1]];
//    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1]];
//    self.viewControllers = @[zidongNavi,UserNavi,BuyNavi];
}
- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        //        NSLog(@"current Language == Chinese");
        return @"chinese";
    }
    else{
        lang = @"en";
        //        NSLog(@"current Language == English");
        return @"English";
    }
}

-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
@end
