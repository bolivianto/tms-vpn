//
//  ModPassViewController.m
//   VPN
//
//  Created by a on 2017/8/3.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ModPassViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "CLAlertView.h"

@interface ModPassViewController ()<UITextFieldDelegate>
{
    UITextField *emailText;
    UITextField *passWordText;
    UITextField *newPassWordText;
    BOOL isLook0;
    BOOL isLook;
    BOOL isLook1;
    UIButton *lookPassBtn0;

    UIButton *lookPassBtn;
    UIButton *lookPassBtn1;

    UIButton *modBtn;
    //绑定失败提示
    UIAlertView *alert;
    UIAlertView *kongAlert;
    UIAlertView *bindAlert;
    UIAlertView *passAlert;
}
@property (nonatomic,strong) NSMutableDictionary * Userdict;

@end

@implementation ModPassViewController

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *view12 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT + 49)];
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];

    view12.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:view12]; 
        [self creatNavi];
    
    [self creatMain];
}
#pragma mark 判断邮箱手机号合法性
- (BOOL)checkPhone:(NSString *)phoneNumber
{
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:phoneNumber];
    if (!isMatch)
    {
        return NO;
    }
    return YES;
}

- (BOOL)checkEmail:(NSString *)email{
    
    NSString *regex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [emailTest evaluateWithObject:email];
}

-(void)creatNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationItem.title = NSLocalizedString(@"userMess9", nil);
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];

}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)creatMain
{

    UILabel *emailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,NaviHeight + 50*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    emailTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    emailTextLabel.text = NSLocalizedString(@"reglog36", nil);
    emailTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:emailTextLabel];
    
    
    
    UIImageView *emailTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    emailTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:emailTextImage];

    
    
    //邮箱
    //初始化textfield并设置位置及大小
    emailText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    //设置边框样式，只有设置了才会显示边框样式
    emailText.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    emailText.returnKeyType = UIReturnKeyDone;
    
    //文字居中
    emailText.textAlignment = 1;

    emailText.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    emailText.placeholder = NSLocalizedString(@"reglog36", nil);
    [emailText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    emailText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];

    //设置输入框内容的字体样式和大小
    emailText.font = [UIFont fontWithName:@"Arial" size:16.0f*KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    emailText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    emailText.clearsOnBeginEditing = NO; 
    //设置键盘的样式
    emailText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    emailText.delegate = self;
       //return键变成什么键
    emailText.returnKeyType = UIReturnKeyDone;
    
    [self.view addSubview:emailText];
    
    
    
    UILabel *passTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailText.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    passTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    passTextLabel.text = NSLocalizedString(@"reglog23", nil);
    passTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:passTextLabel];
    
    
    UIImageView *passTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passTextImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:passTextImage];
    
    //初始化textfield并设置位置及大小
    passWordText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];    //设置边框样式，只有设置了才会显示边框样式
    passWordText.borderStyle = UITextBorderStyleNone;
        passWordText.returnKeyType = UIReturnKeyDone;
    
    //文字居中
    passWordText.textAlignment = 1;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    passWordText.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    passWordText.placeholder = NSLocalizedString(@"reglog23", nil);
    [passWordText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    passWordText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];

    //设置输入框内容的字体样式和大小
    passWordText.font = [UIFont fontWithName:@"Arial" size:16.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    //    passWordText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    passWordText.clearsOnBeginEditing = NO; 
    //设置键盘的样式
    passWordText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    passWordText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    passWordText.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    passWordText.secureTextEntry = YES;
        //return键变成什么键
    passWordText.returnKeyType = UIReturnKeyNext;
    
    [self.view addSubview:passWordText];
    
    
    
    UILabel *newPassWordLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passWordText.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    newPassWordLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    newPassWordLabel.text = NSLocalizedString(@"reglog37", nil);
    newPassWordLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [self.view addSubview:newPassWordLabel];
    
    
    UIImageView *newPassWordImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(newPassWordLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    newPassWordImage.image = [UIImage imageNamed:@"矩形4"];
    [self.view addSubview:newPassWordImage];

    
    
    //初始化textfield并设置位置及大小
    newPassWordText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(newPassWordLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    //设置边框样式，只有设置了才会显示边框样式
    newPassWordText.borderStyle = UITextBorderStyleNone;
    
        //文字居中
    newPassWordText.textAlignment = 1;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    newPassWordText.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    newPassWordText.placeholder = NSLocalizedString(@"reglog37", nil);
    [newPassWordText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    newPassWordText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];

    //设置输入框内容的字体样式和大小
    newPassWordText.font = [UIFont fontWithName:@"Arial" size:16.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    //    passWordText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    newPassWordText.clearsOnBeginEditing = NO; 
    //设置键盘的样式
    newPassWordText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    newPassWordText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    newPassWordText.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    newPassWordText.secureTextEntry = YES;
       newPassWordText.returnKeyType = UIReturnKeyDone;
    
    [self.view addSubview:newPassWordText];

    
    //显示密码
    lookPassBtn0 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn0.center = CGPointMake(CGRectGetMaxX(emailText.frame) + 15*KWidth_Scale, emailText.center.y);
    [lookPassBtn0 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn0 addTarget:self action:@selector(lookPass0) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:lookPassBtn0];
    
    //显示密码
    lookPassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn.center = CGPointMake(CGRectGetMaxX(passWordText.frame) + 15*KWidth_Scale, passWordText.center.y);
    [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:lookPassBtn];
    
    //显示密码
    lookPassBtn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn1.center = CGPointMake(CGRectGetMaxX(newPassWordText.frame) + 15*KWidth_Scale, newPassWordText.center.y);
    
    [lookPassBtn1 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn1 addTarget:self action:@selector(lookPass1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:lookPassBtn1];

    
    
    //绑定按钮
    modBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/6, CGRectGetMaxY(newPassWordText.frame) + 50*KWidth_Scale, SCREEN_WIDTH/3*2, 40*KWidth_Scale)];

    modBtn.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    [modBtn setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    
    [modBtn setTitle:NSLocalizedString(@"userMess9", nil) forState:UIControlStateNormal];
    
    [modBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [modBtn addTarget:self action:@selector(activate) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:modBtn];
    
    [self ceareAlertViewUI];
    
}

#warning .......

// # Test #
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:1 animated:YES];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:3 animated:NO];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:4 animated:YES];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(12 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:0 animated:YES];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:1 animated:YES];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(18 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:4 animated:NO];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(21 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:1 animated:NO];
//    });
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(24 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [pageView jumpImmediatelyToIndex:4 animated:YES];
//    });

#warning .....
- (void)ceareAlertViewUI{
    //绑定失败提示
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                       message:NSLocalizedString(@"other11", nil)
                                      delegate:self
                             cancelButtonTitle:nil
                             otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //为空
    kongAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"oldUser12", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //为空
    bindAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"oldUser13", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //为空
    passAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"oldUser14", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
}
-(void)bindBtnAction
{

    NSString *string = self.Userdict[@"Email"];
        if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"])
        {
            [bindAlert show];
        }else
        {
            if ([emailText.text isEqualToString:@""] || [passWordText.text isEqualToString:@""] || [newPassWordText.text isEqualToString:@""])
            {
                [kongAlert show];
            }else{
                
                NSString *md5Pass = [self MD5:emailText.text];
                NSString *newmd5Pass1 = [self MD5:passWordText.text];
                NSString *newmd5Pass2 = [self MD5:newPassWordText.text];
                
                if ([newmd5Pass1 isEqualToString:newmd5Pass2])
                {
                    //完成绑定操作
                    NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/Updateps?apikey=lygames_0953&mail=%@&pass=%@&newpass=%@",GONGYOUURL,self.Userdict[@"Email"],md5Pass,newmd5Pass1];
                    
                    NSURL *url = [NSURL URLWithString:userString];
                    
                    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
                    
                    NSURLResponse *response = nil;
                    NSError *error = nil;
                    
                    
                    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                    
                    if (userdata)
                    {
                        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil]; 
                        
                        if (array)
                        {
                            
                            if ([array[0][@"state"] isEqualToString:@"1"])
                            {
                                CLAlertView *alertView = [[CLAlertView alloc] initWithAlertViewWithTitle:NSLocalizedString(@"reglog6", nil) text:NSLocalizedString(@"reglog33", nil) DefauleBtn:NSLocalizedString(@"reglog31", nil) cancelBtn:NSLocalizedString(@"reglog32", nil) defaultBtnBlock:^(UIButton *defaultBtn) {
                          
                                    self.tabBarController.tabBar.hidden = NO;
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                    
                                } cancelBtnBlock:^(UIButton *cancelBtn) {  
                                }];
                                [alertView show];
                            }else if ([array[0][@"state"] isEqualToString:@"0"])
                            {
                                [alert show];
                            }
                        }else
                        {
                            [alert show];
                        }
                    }else
                    {
                        [alert show];
                    }
                }else
                {
                    [passAlert show];
                }
            }
        }

//    }
    
}
//显示密码
-(void)lookPass0
{
    if (isLook0)
    {
        [lookPassBtn0 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        emailText.secureTextEntry = YES;
        isLook0 = 0;
    }else
    {
        [lookPassBtn0 setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        emailText.secureTextEntry = NO;
        isLook0 = 1;
    }
}

//显示密码
-(void)lookPass
{
    if (isLook)
    {
        [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        passWordText.secureTextEntry = YES;
        isLook = 0;
    }else
    {
        [lookPassBtn setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        passWordText.secureTextEntry = NO;
        isLook = 1;
    }
}
//显示密码
-(void)lookPass1
{
    if (isLook1)
    {
        [lookPassBtn1 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        newPassWordText.secureTextEntry = YES;
        isLook1 = 0;
    }else
    {
        [lookPassBtn1 setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        newPassWordText.secureTextEntry = NO;
        isLook1 = 1;
    }
}
- (NSString *)MD5:(NSString *)mdStr
{
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [emailText resignFirstResponder];
    [passWordText resignFirstResponder];
    [newPassWordText resignFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
