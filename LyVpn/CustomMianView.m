//
//  CustomMianView.m
//  极光VPN
//
//  Created by cacac on 2017/4/24.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "CustomMianView.h"
#import "UIImageView+WebCache.h"
@interface CustomMianView()

@property (nonatomic,strong) UIImageView * imageView;
@property (nonatomic,strong) NSString * customPadImage;
@property (nonatomic,strong) NSString * customSImage;
@property (nonatomic,strong) NSString * customOtherImage;
@property (nonatomic,strong) NSString * customLink;
@property (nonatomic,strong) UIButton * endButton;
@property (nonatomic,strong) NSDictionary * dict;

@end
@implementation CustomMianView

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    [self setup];
    return self;
}

- (void) setup
{
    self.dict = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    _customLink = self.dict[@"Middle_Link"];
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    _imageView.center = CGPointMake(self.center.x, self.center.y);
    _imageView.userInteractionEnabled = YES;
    if (IS_IPAD) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:self.dict[@"Middle_ImgIpad"]] placeholderImage:[UIImage imageNamed:@""]];
        
    }else
    {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:self.dict[@"Middle_Img"]] placeholderImage:[UIImage imageNamed:@""]];
    }
    [self addSubview:_imageView];
    
    UITapGestureRecognizer * tiaozhuanTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToZheNewWebs)];
    [_imageView addGestureRecognizer:tiaozhuanTap];
    
}

//点击广告进入该链接的appstroe
- (void)jumpToZheNewWebs
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_customLink]];
}


@end
