//
//  HomeViewController.h
//  VPN
//
//  Created by 王树超 on 18/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (strong, nonatomic) UIWindow *window;
@property BOOL isShowQuickLoginButton;
@end
