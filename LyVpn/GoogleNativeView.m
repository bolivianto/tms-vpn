//
//  GoogleNativeView.m
//   VPN
//
//  Created by cacac on 2017/3/28.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "GoogleNativeView.h"

@interface GoogleNativeView()<GADNativeExpressAdViewDelegate, GADVideoControllerDelegate>
@property (nonatomic,retain) GADNativeExpressAdView *nativeExpressAdView;
@end
@implementation GoogleNativeView

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    return self;
}
- (void) setup:(UIViewController *)controller key:(NSString *)strkey
{
    self.nativeExpressAdView = [[GADNativeExpressAdView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.nativeExpressAdView.adUnitID = strkey;
    self.nativeExpressAdView.rootViewController = controller;
    self.nativeExpressAdView.delegate = self;
    // The video options object can be used to control the initial mute state of video assets.
    // By default, they start muted.
    GADVideoOptions *videoOptions = [[GADVideoOptions alloc] init];
    videoOptions.startMuted = true;
    [self.nativeExpressAdView setAdOptions:@[ videoOptions ]];
    // Set this UIViewController as the video controller delegate, so it will be notified of events
    // in the video lifecycle.
    self.nativeExpressAdView.videoController.delegate = self;
    GADRequest *request = [GADRequest request];
    [self.nativeExpressAdView loadRequest:request];
    [self addSubview:self.nativeExpressAdView];
}
#pragma mark - GADNativeExpressAdViewDelegate
- (void)nativeExpressAdViewDidReceiveAd:(GADNativeExpressAdView *)nativeExpressAdView {
    if (nativeExpressAdView.videoController.hasVideoContent) {
        //        NSLog(@"Received ad an with a video asset.");
    } else {
        //        NSLog(@"Received ad an without a video asset.");
    }
}

#pragma mark - GADVideoControllerDelegate
- (void)videoControllerDidEndVideoPlayback:(GADVideoController *)videoController {
    //    NSLog(@"Playback has ended for this ad's video asset.");
}
@end
