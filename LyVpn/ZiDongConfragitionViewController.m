//
//  ZiDongConfragitionViewController.m
//  VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ZiDongConfragitionViewController.h"
#import "AFNetworking.h"
#import <NetworkExtension/NetworkExtension.h>
#import "CLAlertView.h"
#import "LSProgressHUD.h"
#import "RoadViewController.h"
#import "LemonKit.h"
#import "ChangeTimeString.h"
#import "GoogleNativeView.h"
#import "CustomMianView.h"
#import "MianMiddleNativeView.h"
#import "LogViewController.h"
#import "CKAlertViewController.h"
#import "UIImageView+WebCache.h"
#import "DisconnectGoodEvaPopView.h"
#import "routeModel.h"
#import "DataTaoCanViewController.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"
@import TrueTime;

static NSString * cellidenty = @"cell";
NSInteger buttonClick = 0;

#define pi 3.14159265359
#define   DEGREES_TO_RADIANS(degrees)  ((pi * degrees)/ 180)

@interface ZiDongConfragitionViewController ()<CAAnimationDelegate,VungleSDKDelegate,UIAlertViewDelegate>
{
    UIImageView *logoImageView;
    NSString * PanDuanEva;
    NSString *aPath3;
    UIImageView * rightBtn;
    UIImageView *goodEvaImageView;
    UIButton *getOutBtn;
    NSString *success;
    UIImageView *topImage;
    UIButton *lineButton;
    UILabel *CountryLineName;
    UIImageView *CountryImage;
    BOOL isGet;
}
#pragma mark------------------  1  -----------------
//主界面上的ui
@property (nonatomic,strong) UIButton * MainBtn;
@property (nonatomic,strong) UILabel * statusLabel;
@property (nonatomic,strong) NSString * statusText;
@property (nonatomic,strong) NSMutableDictionary * ADdata;
@property (nonatomic,strong) NSMutableDictionary * userData;
@property (nonatomic,strong) MianMiddleNativeView * middleNativeView;
@property (nonatomic,strong) CustomMianView * CusMianView;
@property (nonatomic,strong) CKAlertViewController *alertVC;
@property (nonatomic,strong) DisconnectGoodEvaPopView * DisEvaPopView;
@property (nonatomic,strong) NSArray * titleName;
@property (nonatomic,strong) NSArray * imageName;
@property (nonatomic,strong) NSArray * URLArray;

//首页广告
@property (nonatomic,strong) UIView * mainAdView;
@property (strong, nonatomic) NEVPNManager * vpnManager;

//当前的线路国家名称
@property (nonatomic,strong) NSString * countryname;
//有效期至
@property (nonatomic,strong) NSString * timeYouXiao;
//当前时间，从后台获取
@property (nonatomic,strong) NSString * currentTime;

//判断是否是会员
@property (nonatomic,assign) NSString * ChannelState;
@property (nonatomic,strong) GoogleNativeView * NativeView;
@property (nonatomic,strong) CABasicAnimation * animation;
@property (nonatomic,strong) UIImageView * xuanzhuanImageView;
@property (nonatomic,strong) NSMutableArray * adArray;

//强卡视图
@property (nonatomic,strong) UIView * strongView;
@property (nonatomic,strong) UIView * BackView;
@property (nonatomic,strong) UIImageView * BackImageView;
@property (nonatomic,strong) UIImageView * customImageView;

// 起始日期  结束日期
@property (nonatomic,strong) NSString * startTime;
@property (nonatomic,strong) NSString * endTime;
@property (nonatomic,strong) NSString * restTime;
@property (nonatomic,strong) NSString * TaoCanName;
@property (nonatomic,strong) UIImageView *ConnectImageView;

@end

@implementation ZiDongConfragitionViewController
{
    OSStatus statuss;
    BOOL isChoose;
    BOOL isStrong;
    UIView * allview;
    UIView * twoview;
    BOOL isXieYi;
    NSString * nativeKey;
    TrueTimeClient *ttClient;
    NSDate *currentTrueTimeDate;
}

- (NSMutableArray *)adArray {
    if (_adArray == nil) {
        _adArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _adArray;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Init Window
    self.window = [[[UIApplication sharedApplication] delegate] window];
    
    // Hide Navigation Controller
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    /*
     *透明效果
     Transparent effect
     */
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    
    NSString * roadstr = [[NSUserDefaults standardUserDefaults] objectForKey:@"countryImage"];
    NSString * CountryName = [[NSUserDefaults standardUserDefaults] objectForKey:@"CountryName"];
    
    if (roadstr) {
        [CountryImage sd_setImageWithURL:[NSURL URLWithString:[roadstr stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLHostAllowedCharacterSet]]];
        CountryLineName.text = CountryName;
    } else {
        CountryImage.image = [UIImage imageNamed:@"美国"];
        CountryLineName.text = NSLocalizedString(@"tablename1", nil);
    }
    
    _ChannelState = @"";
    
    if (isGet) {
        [self GetLogintime];
    }
    [self GetLogintime1];
    [self vpnStatusDidChanged:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Init TrueTime
    ttClient = [TrueTimeClient sharedInstance];
    //[ttClient startWithHostURLs:@[[NSURL URLWithString:@"time.apple.com"]]];
    
    // To block waiting for fetch, use the following:
    [ttClient fetchIfNeededWithSuccess:^(NTPReferenceTime *referenceTime) {
        [Global printLogWithObject:[NSString stringWithFormat:@"Actual Time : %@", [referenceTime now]] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
        self->currentTrueTimeDate = [referenceTime now];
    } failure:^(NSError *error) {
        [Global printLogWithObject:[NSString stringWithFormat:@"Error : %@", error.localizedDescription] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    }];
    
    isGet = 1;
    
    if ([[self panduan] isEqualToString:@"chinese"]) {
        self.navigationItem.title = @"欢迎使用VPN";
    } else{
        self.navigationItem.title = @"Welcome to use VPN";
    }
    
    _userData = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    
    if (_userData == nil) {
        _userData = [KeyChainStore load:KEY_USER_LOGINDATA];
        NSLog(@"user data error");
    }
    
    _BackImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH - 30, 300*KHeight_Scale + 10)];
    _BackImageView.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    _BackImageView.image = [UIImage imageNamed:@"evaback"];
    
    // 判断是否显示注册页
    // Determine whether to display the registration page
    if (_userData == nil) { // User is not logged in
        [self getRegLogMessage];
    }
    
    // 原生广告开关获取
    // Native advertising switch acquisition
    //    [self GetAdMessageData];
    
    // Load Ad Data
    _ADdata = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    [Global printLogWithObject:_ADdata function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    
    isChoose = NO;
    // 创建VPN连接
    self.vpnManager = [NEVPNManager sharedManager];
    
    // 获取有效期至
    // Get valid until
    [self GetLogintime];
    [self GetLogintime1];
    
    // 界面1
    // Interface 1
    [self createallview];
    
    // 获取vpn连接的信息
    // Get information about the vpn connection
    [[ConnectVPNManager sharedManager] GetVPNDataWithAreaName];
    
    // 设置通知
    // Set notification
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(vpnStatusDidChanged:)
               name:NEVPNStatusDidChangeNotification
             object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTime:) name:@"refresh1" object:nil];
#pragma mark -- 刷新
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTaoCan:) name:@"refresh2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Repeat) name:@"Repeat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Repeat) name:@"Repeat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChangeLine) name:@"changeLine" object:nil];
    
    // 判断当前是否处于连接状态
    // Determine if it is currently connected
    [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        NEVPNStatus mystatus = self.vpnManager.connection.status;
        if (mystatus == NEVPNStatusConnected) {
        }
    }];
    // 强卡界面，需要判断后台是否开启
    // Strong card interface, you need to judge whether the background is open
    [self createStrongView];
}

- (void)ChangeLine {
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"changeLine"];
    self.currentTime = [ChangeTimeString DataChangeTimeString:dic[@"currentTime"]];
    isGet = 0;
}

- (void)Repeat {
    // 不让他连接
    [[ConnectVPNManager sharedManager] Repeat];
}

#pragma Mark -- Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"showLoginSegue"]) {
        LogViewController *vc = segue.destinationViewController;
        vc.email = self.email;
        vc.isYouke = self.isYouke;
        vc.isZidong = self.isZidong;
    }
    if ([segue.identifier isEqual: @"showDueTimeSegue"]) {
        DataTaoCanViewController * vc = segue.destinationViewController;
        vc.startTime = self.startTime;
        vc.endTime = self.endTime;
        vc.restTime = self.restTime;
        vc.TaoCanname = self.TaoCanName;
    }
    if ([segue.identifier isEqualToString:@"showServerSegue"]) {
        RoadViewController * vc = segue.destinationViewController;
        vc.ChannelState = [self.ChannelState integerValue];
        vc.ChooseCountryNameBlock = ^(NSString * countryName,NSArray * serverarrray)
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StatisticsConnect"];
            [self disconnect];
            self->isChoose = YES;
            self.countryname = countryName;
            [[ConnectVPNManager sharedManager] setCurrentRouteWith:serverarrray];
            //加载备用线路
            [[ConnectVPNManager sharedManager] loadSpareRoute];
        };
    }
}

//#pragma mark -- 帮主页面
//- (void)leftAction
//{
//
//    if ([[CommUtls  panduan] isEqualToString:@"chinese"]) {
//        HelpViewController * helpVC = [[HelpViewController alloc] init];
//        [self.navigationController pushViewController:helpVC animated:NO];
//    }
//    else
//    {
//        EnglishViewController * engVC = [[EnglishViewController alloc] init];
//        [self.navigationController pushViewController:engVC animated:NO];
//    }
//}
//获取用户是否注册
// Get the user to register
-(void)getRegLogMessage {
    //需要请求后台数据，查看用户状态
    // Need to request background data to view user status
    //用户注册信息本地数据获取
    // User registration information local data acquisition
    NSDictionary* dic = [KeyChainStore load:KEY_USER_LOGINDATA];
    NSString *password = dic[@"Password"];
    NSString *email = dic[@"Email"];
    NSDictionary* dic1 = [NSKeyedUnarchiver unarchiveObjectWithFile:LOGINDATA];
    NSString *savedEmail = dic1[@"Email"];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *navVC = [sb instantiateViewControllerWithIdentifier:@"LoginNavigationController"];
    LogViewController *loginVC = navVC.viewControllers[0];
    if ([email isKindOfClass:[NSNull class]] || email.length < 1 || [email isEqualToString:@"<null>"]) {
        // 没有邮箱
        // No Mailbox
        if ([savedEmail isKindOfClass:[NSNull class]] || savedEmail.length < 1 || [savedEmail isEqualToString:@"<null>"])
        {
            //从没有注册过
            // Never registered
            /*
             LogViewController *logVC = [[LogViewController alloc]init];
             logVC.isYouke = @"1";
             logVC.isZidong = @"1";
             [self.navigationController pushViewController:logVC animated:NO];
             */
            // New
            
            loginVC.isYouke = @"1";
            loginVC.isZidong = @"1";
            loginVC.email = @"";
            [self presentViewController:navVC animated:YES completion:nil];
        } else {
            //以前注册过，但是退出登录
            // Registered before, but quit login
            /*
             LogViewController *logVC = [[LogViewController alloc]init];
             logVC.isYouke = @"0";
             logVC.isZidong = @"1";
             logVC.email = panduanEmail;
             [self.navigationController pushViewController:logVC animated:NO];
             */
            // New
            loginVC.isYouke = @"0";
            loginVC.isZidong = @"1";
            loginVC.email = savedEmail;
            [self presentViewController:navVC animated:YES completion:nil];
        }
    } else {
        //自动登录，重新请求登录
        // Automatic login, re-request login
        NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserLogin?apikey=lygames_0953&uuid=%@&name=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],email,password,TYPESTRING];
        NSURL *url = [NSURL URLWithString:userString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (userdata) {
            NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
            if (array) {
                if ([array[0][@"state"] integerValue] == 2)
                {
                    //登录失败
                    // Login failed
                    /*
                     LogViewController *logVC = [[LogViewController alloc]init];
                     logVC.isYouke = @"0";
                     logVC.email = email;
                     logVC.isZidong = @"1";
                     [self.navigationController pushViewController:logVC animated:NO];
                     */
                    // New
                    loginVC.isYouke = @"0";
                    loginVC.isZidong = @"1";
                    loginVC.email = email;
                    [self presentViewController:navVC animated:YES completion:nil];
                    
                } else if ([array[0][@"state"] integerValue] == -1)
                {
                    //apikey输入错误
                    // Input error
                    /*
                     LogViewController *logVC = [[LogViewController alloc]init];
                     logVC.isYouke = @"0";
                     logVC.email = email;
                     logVC.isZidong = @"1";
                     [self.navigationController pushViewController:logVC animated:NO];
                     */
                    // New
                    loginVC.isYouke = @"0";
                    loginVC.isZidong = @"1";
                    loginVC.email = email;
                    [self presentViewController:navVC animated:YES completion:nil];
                } else {
                    //自动登录成功，不需要操作，直接进入
                    // Automatic login is successful, no operation is required, directly enter
                    [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:array[0]];
                    NSLog(@"Success Login!");
                    NSLog(@"Data:\n\n\n%@", [KeyChainStore load:KEY_USER_LOGINDATA]);
                }
            } else {
                //解析失败，也弹出登录页
                // The parsing failed, and the login page pops up.
                /*
                 LogViewController *logVC = [[LogViewController alloc]init];
                 logVC.isYouke = @"0";
                 logVC.email = email;
                 logVC.isZidong = @"1";
                 [self.navigationController pushViewController:logVC animated:NO];
                 */
                // New
                loginVC.isYouke = @"0";
                loginVC.isZidong = @"1";
                loginVC.email = email;
                [self presentViewController:navVC animated:YES completion:nil];
            }
        } else {
            //请求失败，也弹出登录页
            // The request failed and the login page pops up
            /*
             LogViewController *logVC = [[LogViewController alloc]init];
             logVC.isYouke = @"0";
             logVC.email = email;
             logVC.isZidong = @"1";
             [self.navigationController pushViewController:logVC animated:NO];
             */
            // New
            loginVC.isYouke = @"0";
            loginVC.isZidong = @"1";
            loginVC.email = email;
            [self presentViewController:navVC animated:YES completion:nil];
        }
    }
}

//强卡
-(void)createStrongView {
    if ([self.ADdata[@"StrongSwitch"] isKindOfClass:[NSNull class]]){
        
    } else {
        if ([self.ADdata[@"StrongSwitch"] isEqualToString:@"1"])
        {
            //自定义
            // customize
            _strongView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            _strongView.userInteractionEnabled = YES;
            _strongView.backgroundColor = [UIColor blackColor];
            _strongView.alpha = 0.5;
            
            _customImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH / 5 * 4, SCREEN_WIDTH)];
            
            _customImageView.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
            
            if ([[CommUtls panduan] isEqualToString:@"chinese"])
            {
                _customImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.ADdata[@"Strongimg"]]]];
                
            }else
            {
                _customImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.ADdata[@"ENStrongimg"]]]];
                
            }
            
            _customImageView.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
            _customImageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(linkAction)];
            [_customImageView addGestureRecognizer:tapGesturRecognizer];
            
            if ([self.ADdata[@"StrongType"] isEqualToString:@"1"])
            {
                UIButton *cancleBtn = [[UIButton alloc]initWithFrame:CGRectMake(_customImageView.frame.size.width - 40, 10, 30, 30)];
                
                [cancleBtn setImage:[UIImage imageNamed:@"downmyshelf"] forState:UIControlStateNormal];
                
                [cancleBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
                
                [_customImageView addSubview:cancleBtn];
            }
            
            self.navigationController.tabBarController.tabBar.hidden = YES;
            isStrong = 1;
            
            [self.view addSubview:_strongView];
            [self.view addSubview:_customImageView];
            
        } else {
            //导航栏
            //            [self lineButtonAction];
        }
    }
}

-(void)cancelBtnAction
{
    isStrong = 0;
    [_strongView removeFromSuperview];
    [_customImageView removeFromSuperview];
    self.navigationController.tabBarController.tabBar.hidden = NO;
    //导航栏
    //    [self lineButtonAction];
    
}

-(void)linkAction {
    // 强卡链接
    if ([[CommUtls panduan] isEqualToString:@"chinese"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"StrongLink"]]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"ENStrongLink"]]];
    }
}


- (void) createallview {
    allview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    allview.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
    [self.view addSubview:allview];
    
    topImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH - 20*KWidth_Scale)];
    if (IS_IPAD) {
        topImage.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT / 2.2);
    }
    topImage.image = [UIImage imageNamed:@"background"];
    [self.view addSubview:topImage];
    
    
    goodEvaImageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 60*KWidth_Scale, 60*KWidth_Scale)];
    
    if (IS_IPAD) {
        goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 40*KWidth_Scale, 40*KWidth_Scale);
    }
    //参与好评
    if ([self.ADdata[@"announcement"] isEqualToString:@"1"]) {
        goodEvaImageView.backgroundColor = [UIColor clearColor];
        //        goodEvaImageView.image = [UIImage imageNamed:@"commentsBG"];
        //        [goodEvaImageView sizeToFit];
        goodEvaImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goodeva)];
        [goodEvaImageView addGestureRecognizer:tapGesturRecognizer];
        [self.view addSubview:goodEvaImageView];
        
        
#pragma mark 获取广告 - Get an Ad
        if ([PanDuanEva isEqualToString:@"-1"]) {
            [goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"MainViewImageAddress"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
        } else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SHARE"] != nil && [[[NSUserDefaults standardUserDefaults] objectForKey:@"SHARE"] isEqualToString:@"2"]){
            
            
            [goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"Middle_ImgIpad"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
            
        } else {
            [goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"MainImage"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
        }
        [self.view addSubview:goodEvaImageView];
    }
    
    //创建连接按钮 - Create a Connect button
    _MainBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH/4, CGRectGetMaxY(goodEvaImageView.frame), SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
    if (IS_IPAD) {
        _MainBtn.frame = CGRectMake(self.view.center.x - SCREEN_WIDTH/7, CGRectGetMaxY(goodEvaImageView.frame), SCREEN_WIDTH/3.5, SCREEN_WIDTH/3.5);
    }
    _MainBtn.layer.masksToBounds = YES;
    _MainBtn.layer.cornerRadius = SCREEN_WIDTH/8;
    [_MainBtn setBackgroundImage:[UIImage imageNamed:@"未连接"] forState:UIControlStateNormal];
    [_MainBtn addTarget:self action:@selector(MainButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_MainBtn];
    
    // 连接中动画 - Connected animation
    self.ConnectImageView = [[UIImageView alloc]initWithFrame:_MainBtn.frame];
    self.ConnectImageView.userInteractionEnabled = NO;
    NSArray *imageArray = [NSArray arrayWithObjects:
                           [UIImage imageNamed:@"02"],
                           [UIImage imageNamed:@"03"],
                           [UIImage imageNamed:@"04"],
                           [UIImage imageNamed:@"05"],
                           [UIImage imageNamed:@"06"],
                           [UIImage imageNamed:@"07"],
                           [UIImage imageNamed:@"08"],
                           [UIImage imageNamed:@"09"],
                           [UIImage imageNamed:@"10"],
                           [UIImage imageNamed:@"11"],
                           [UIImage imageNamed:@"12"],
                           [UIImage imageNamed:@"13"],
                           [UIImage imageNamed:@"14"],
                           [UIImage imageNamed:@"15"],
                           [UIImage imageNamed:@"16"],
                           [UIImage imageNamed:@"17"],
                           [UIImage imageNamed:@"18"],
                           [UIImage imageNamed:@"19"],
                           nil];
    [self.ConnectImageView setAnimationImages:imageArray];
    [self.ConnectImageView setAnimationDuration:2];
    [self.ConnectImageView setAnimationRepeatCount:MAXFLOAT];
    
    // 连接状态 - Connection Status
    self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(20*KWidth_Scale,  CGRectGetMaxY(_MainBtn.frame)+10*KWidth_Scale, SCREEN_WIDTH - 40*KWidth_Scale, 30*KWidth_Scale)];
    self.statusLabel.font = [UIFont boldSystemFontOfSize:16.0*KWidth_Scale];
    self.statusLabel.text = NSLocalizedString(@"btnname1", nil);
    self.statusLabel.textColor = [UIColor whiteColor];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.statusLabel];
    
    /////用户信息
    UIButton *userBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 2.5)];
    
    [userBtn setBackgroundImage:[UIImage imageNamed:@"矩形7拷贝4"] forState:UIControlStateNormal];
    [userBtn addTarget:self action:@selector(goToAccountPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:userBtn];
    
    UIImageView *userImage = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100) / 3/2 -15*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale)];
    userImage.image = [UIImage imageNamed:@"用户信息"];
    [userBtn addSubview:userImage];
    
    UILabel *userLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(userImage.frame) + 20*KWidth_Scale, userBtn.frame.size.width, 15*KWidth_Scale)];
    if ([[self panduan] isEqualToString:@"chinese"]) {
        userLabel.text = @"用户信息";
        
    }else{
        userLabel.text = @"Account";
        
    }
    userLabel.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:232.0/255.0 alpha:1];
    userLabel.textAlignment = NSTextAlignmentCenter;
    userLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    [userBtn addSubview:userLabel];
    
    ////购买套餐
    UIButton *taocanBtn = [[UIButton alloc]initWithFrame:CGRectMake((SCREEN_WIDTH + 50) / 3, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 2.5)];
    [taocanBtn setBackgroundImage:[UIImage imageNamed:@"矩形7拷贝4"] forState:UIControlStateNormal];
    [taocanBtn addTarget:self action:@selector(goToProductPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:taocanBtn];
    
    UIImageView *taocanImage = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100) / 3/2 - 15*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale)];
    taocanImage.image = [UIImage imageNamed:@"二级-服务器"];
    [taocanBtn addSubview:taocanImage];
    
    UILabel *taocanLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(taocanImage.frame) + 20*KWidth_Scale, userBtn.frame.size.width, 15*KWidth_Scale)];
    taocanLabel.text = NSLocalizedString(@"key3", nil);
    taocanLabel.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:232.0/255.0 alpha:1];
    taocanLabel.textAlignment = NSTextAlignmentCenter;
    taocanLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    [taocanBtn addSubview:taocanLabel];
    
    ///剩余时间
    UIButton *timeButton = [[UIButton alloc]initWithFrame:CGRectMake((2*SCREEN_WIDTH + 10) / 3, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 2.5)];
    [timeButton setBackgroundImage:[UIImage imageNamed:@"矩形7拷贝4"] forState:UIControlStateNormal];
    [timeButton addTarget:self action:@selector(timeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:timeButton];
    
    
    UIImageView *timeImage = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH - 100) / 3/2 - 15*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale, 25*KWidth_Scale)];
    timeImage.image = [UIImage imageNamed:@"圆角矩形2拷贝"];
    [timeButton addSubview:timeImage];
    
    
    UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(timeImage.frame) + 20*KWidth_Scale, timeButton.frame.size.width, 15*KWidth_Scale)];
    if ([[self panduan] isEqualToString:@"chinese"]) {
        timeLabel.text = @"剩余时间";
        
    }else{
        timeLabel.text = @"Due time";
        
    }
    timeLabel.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:232.0/255.0 alpha:1];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    [timeButton addSubview:timeLabel];
    
    if (IS_IPAD) {
        userBtn.frame = CGRectMake(30, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 3);
        taocanBtn.frame = CGRectMake((SCREEN_WIDTH + 50) / 3, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 3);
        timeButton.frame = CGRectMake((2*SCREEN_WIDTH + 10) / 3, CGRectGetMaxY(topImage.frame) + 20*KWidth_Scale, (SCREEN_WIDTH - 100) / 3, (SCREEN_WIDTH - 100) / 3);
    }
    
    
    ///线路
    
    lineButton = [[UIButton alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 65*KWidth_Scale, SCREEN_WIDTH, 65*KWidth_Scale)];
    [lineButton setBackgroundImage:[UIImage imageNamed:@"椭圆2"] forState:UIControlStateNormal];
    [lineButton addTarget:self action:@selector(lineButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:lineButton];
    
    CountryImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3, 65*KWidth_Scale/2-15*KWidth_Scale, 30*KWidth_Scale, 30*KWidth_Scale)];
    CountryImage.image = [UIImage imageNamed:@"美国"];
    [lineButton addSubview:CountryImage];
    
    
    CountryLineName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(CountryImage.frame) + 10*KWidth_Scale, 65*KWidth_Scale/2-15*KWidth_Scale,SCREEN_WIDTH - 170*KWidth_Scale, 30*KWidth_Scale)];
    CountryLineName.text = NSLocalizedString(@"tablename1", nil);
    CountryLineName.textColor = [UIColor colorWithRed:56.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1];
    CountryLineName.font = [UIFont systemFontOfSize:15*KWidth_Scale];
    [lineButton addSubview:CountryLineName];
    
    
    
    //首页横幅广告
    [self bannerAD];
}
#pragma mark -- 线路
- (void)lineButtonAction{
    [self performSegueWithIdentifier:@"showServerSegue" sender:self];
}
#pragma 剩余时间
- (void)timeButtonAction{
    [self performSegueWithIdentifier:@"showDueTimeSegue" sender:self];
}
- (void) refreshTaoCan:(NSNotification *)noti
{
    //    NSLog(@"刷新账户界面的数据");
    [self GetLogintime1];
}

- (void) GetLogintime1 {
    if ([[self panduan] isEqualToString:@"chinese"]) {
        self.TaoCanName = self.userData[@"PackageName"];
    } else {
        self.TaoCanName = self.userData[@"EnPackageName"];
    }
    self.startTime = [ChangeTimeString GetYearMonthDayString:self.userData[@"startTime"]];
    self.endTime = [ChangeTimeString GetYearMonthDayString:self.userData[@"StopTime"]];
    self.restTime = [ChangeTimeString dateTimeDifferenceWithStartTime:[ChangeTimeString getCurrentTime] endTime:[ChangeTimeString DataChangeTimeString:self.userData[@"StopTime"]]];
}

#define mark 连接按钮点击事件
- (void) MainButtonClick:(UIButton *)sender
{
    // Current Time
    long currenttime = [ChangeTimeString getZiFuChuanFromDate:currentTrueTimeDate];
    // Effective Time
    long zhidingtime = [ChangeTimeString getZiFuChuan:self.timeYouXiao];
    
    if (![NetWorkReachability internetStatus]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"other1", nil) message:@"Can't access VPN while offline." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else if (currenttime > zhidingtime && self.vpnManager.connection.status != NEVPNStatusConnected) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"other6", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
        alertView.tag = 555;
        [alertView show];
    } else {
        NEVPNStatus status = self.vpnManager.connection.status;
        if (status == NEVPNStatusConnected) {
            [self disconnect];
        } else if (status == NEVPNStatusConnecting || status == NEVPNStatusReasserting) {
            [self disconnect];
        } else {
            [ConnectVPNManager sharedManager].linkTime = 1;
            [ConnectVPNManager sharedManager].currentVPN = VPN_IKEv2;
            [self connect];
        }
    }
}

//-  (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag == 444) {
//        if (buttonIndex == 0) {
//
//        }else
//        {
//            //获取有效期至
//            [self GetLogintime];
//            //获取vpn连接的信息
////            [self GetVPNDataMessage1];
//            //原生广告开关获取
//            [self GetAdMessageData];
//        }
//    }
//    else if (alertView.tag == 555)
//    {
//        if (buttonIndex == 0) {
//            BuysViewController * buyVC = [[BuysViewController alloc] init];
//            buyVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:buyVC animated:NO];
//        }
//    }
//}
#pragma mark -- 用户信息 -- User Info
- (void)goToAccountPage {
    [self performSegueWithIdentifier:@"showAccountSegue" sender:self];
}
#pragma matk -- 购买套餐 -- Purchase package
- (void)goToProductPage {
    [self performSegueWithIdentifier:@"showProductSegue" sender:self];
}

- (void)gogaunggao {
    [MobClick event:@"WatchAd"];
    NSString * PATH = [NSString stringWithFormat:@"%@/lygamesService.asmx/LQYSign?apikey=lygames_0953&name=%@",GONGYOUURL,[UUID getUUID]];
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:PATH parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if ([array[0][@"state"] isEqualToString:@"1"]) {
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"SHANDIANVPNFIRST"]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SHANDIANVPNFIRST"];
                NSLog(@"第一次启动");
                self.alertVC = [CKAlertViewController alertControllerWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"alert8", nil)];
                CKAlertAction *sure = [CKAlertAction actionWithTitle:NSLocalizedString(@"other2", nil) handler:^(CKAlertAction *action) {
                }];
                [self.alertVC addAction:sure];
                [self presentViewController:self.alertVC animated:NO completion:nil];
            }
            else
            {
                //                            NSLog(@"不是第一次启动");
                [self playAdMovie];
            }
        } else {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"other4", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self showErrorWithTitle:NSLocalizedString(@"other3", nil) autoCloseTime:2];
    }];
}

- (void)goodeva {
    if ([self.ADdata[@"country"] isEqualToString:@"1"]) {
        if ([PanDuanEva isEqualToString:@"-1"]) {
            [self PopDisEvaPopView];
        } else {
            //好评过之后，分享
            //分享的标题
            NSString *textToShare = @" VPN带你畅游世界";
            //分享的图片
            UIImage *imageToShare = [UIImage imageNamed:@"logo"];
            //分享的url
            NSURL *urlToShare = [NSURL URLWithString:self.ADdata[@"shareUrl"]];
            if (urlToShare) {
                //在这里呢 如果想分享图片 就把图片添加进去  文字什么的通上
                NSArray *activityItems = @[textToShare,imageToShare,urlToShare];
                UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                
                // activityView.excludedActivityTypes = @[UIActivityTypeAirDrop];
                [self presentViewController:activityView animated:YES completion:^{
                }];
                // 分享之后的回调
                activityView.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
                    if (completed) {
                        [[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"SHARE"];
                        
                        [self->goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"Middle_ImgIpad"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            
                            self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 60*KWidth_Scale, 60*KWidth_Scale);
                            
                            if (IS_IPAD) {
                                self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 40*KWidth_Scale, 40*KWidth_Scale);
                            }
                            
                        }];
                        //分享 成功
                    } else  {
                        NSLog(@"canceled");
                        //分享 取消
                    }
                };
            }
        }
    }
}

-(void)bannerAD {
    NSString *string = self.ADdata[@"MiddleNativeSwitch"];
    if ([string integerValue] == 1)
    {
        _mainAdView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(topImage.frame) + (SCREEN_WIDTH - 80) / 2.5 + 40*KWidth_Scale, SCREEN_WIDTH , SCREEN_WIDTH / 6)];
        if (IS_IPAD) {
            _mainAdView.frame = CGRectMake(0, CGRectGetMaxY(topImage.frame) + (SCREEN_WIDTH - 100) / 3 + 30*KWidth_Scale, SCREEN_WIDTH , SCREEN_WIDTH / 6);
        }
        if (IS_IPHONEX) {
            _mainAdView.frame = CGRectMake(0, CGRectGetMaxY(topImage.frame) + SCREEN_WIDTH / 2 + 30*KWidth_Scale, SCREEN_WIDTH , SCREEN_WIDTH / 6);
        }
        _mainAdView.backgroundColor = [UIColor redColor];
        if ([self.ADdata[@"MiddleNativeAd"] isEqualToString:@"native"])
        {
            //原生
            self.middleNativeView = [[MianMiddleNativeView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_WIDTH / 6)];
            
            [self.middleNativeView setup:self];
            [self.mainAdView addSubview:self.middleNativeView];
        }else if ([self.ADdata[@"MiddleNativeAd"] isEqualToString:@"custom"])
        {
            //自定义
            self.CusMianView = [[CustomMianView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_WIDTH / 6)];
            self.CusMianView.backgroundColor = [UIColor redColor];
            
            [self.mainAdView addSubview:self.CusMianView];
        }
        [self.view addSubview:self.mainAdView];
    }
}

/*#pragma mark - UITableViewDelegate
 
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if ([tableView isEqual:self.tableView])
 {
 return 90;
 }
 else if ([tableView isEqual:self.ShopCartView.OrderList.tableView])
 {
 return 50;
 }
 return 90;
 }
 
 #define SECTION_HEIGHT 30.0
 // 设置section的高度
 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
 
 if ([tableView isEqual:self.ShopCartView.OrderList.tableView])
 {
 if (section==0) {
 return SECTION_HEIGHT;
 }else return 0;
 }
 else
 return 0;
 }
 
 -(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 
 UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, SECTION_HEIGHT)];
 UILabel *leftLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 3, SECTION_HEIGHT)];
 [view addSubview:leftLine];
 if (section == 0) {
 
 
 UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, SECTION_HEIGHT)];
 headerTitle.text = [NSString stringWithFormat:@"%ld号口袋",section +1];
 headerTitle.font = [UIFont systemFontOfSize:12];
 [view addSubview:headerTitle];
 leftLine.backgroundColor = [UIColor greenColor];
 UIButton *clear = [UIButton buttonWithType:UIButtonTypeCustom];
 clear.frame= CGRectMake(self.view.bounds.size.width - 100, 0, 100, SECTION_HEIGHT);
 [clear setTitle:@"清空购物车" forState:UIControlStateNormal];
 [clear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
 clear.titleLabel.textAlignment = NSTextAlignmentCenter;
 clear.titleLabel.font = [UIFont systemFontOfSize:12];
 [clear addTarget:self action:@selector(clearShoppingCart:) forControlEvents:UIControlEventTouchUpInside];
 [view addSubview:clear];
 }
 else{
 
 if (section % 3 == 0) {
 leftLine.backgroundColor = [UIColor greenColor];
 }
 else if (section % 3 == 1)
 {
 leftLine.backgroundColor = [UIColor yellowColor];
 }
 else if (section % 3 == 2)
 {
 leftLine.backgroundColor = [UIColor redColor];
 }
 
 }
 
 //    view.backgroundColor = kUIColorFromRGB(0x9BCB3D);
 view.backgroundColor = [UIColor whiteColor];
 return view;
 }
 
 
 */

- (void) GetAdMessageData {
    NSString * str1 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetLQYjsonBYType?apikey=lygames_0953&type=%@",GONGYOUURL,TYPESTRING];
    
    NSURL *url = [NSURL URLWithString:str1];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (userdata) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        
        if (array){
            [self.adArray addObjectsFromArray:array];
            [NSKeyedArchiver archiveRootObject:array[0] toFile:AllADData];
            
        }
    }
}

- (void) refreshTime:(NSNotification *)noti {
    //    NSLog(@"刷新自动配置界面的数据");
    [self GetLogintime];
}

- (void)GetLogintime {
    _userData = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    self.ChannelState = self.userData[@"ChannelState"];
    self.timeYouXiao = [ChangeTimeString DataChangeTimeString:self.userData[@"StopTime"]];
    self.currentTime = [ChangeTimeString DataChangeTimeString:self.userData[@"currentTime"]];
    [self PanDuanUserEva];
}

- (void)PanDuanUserEva {
    //    PanDuanEva = @"-1";
    NSString * PanDuanUser = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetComments?apikey=lygames_0953&name=%@&type=2",GONGYOUURL,[UUID getUUID]];
    NSURL *url = [NSURL URLWithString:PanDuanUser];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (userdata) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        if (array) {
            PanDuanEva = array[0][@"state"];
        }
    }
}



-  (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 444) {
        if (buttonIndex == 0) {
            
        }else
        {
            //获取有效期至
            [self GetLogintime];
            //获取vpn连接的信息
            [[ConnectVPNManager sharedManager] GetVPNDataWithAreaName];
            //原生广告开关获取
            [self GetAdMessageData];
        }
    }
    else if (alertView.tag == 555) {
        if (buttonIndex == 0) {
            [self performSegueWithIdentifier:@"showProductSegue" sender:self];
        }
    }
}

//断开链接
- (void)disconnect {
    [self.vpnManager.connection stopVPNTunnel];
}

- (DisconnectGoodEvaPopView *)DisEvaPopView {
    if (_DisEvaPopView == nil) {
        _DisEvaPopView = [[DisconnectGoodEvaPopView alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH - 40, 300 * KHeight_Scale)];
        _DisEvaPopView.center = CGPointMake(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2);
        _DisEvaPopView.layer.masksToBounds = YES;
        _DisEvaPopView.layer.cornerRadius = 5;
    }
    return _DisEvaPopView;
}
- (NSArray *)titleName
{
    if (_titleName == nil) {
        _titleName = @[NSLocalizedString(@"tablename1", nil),NSLocalizedString(@"tablename2", nil),NSLocalizedString(@"tablename3", nil),NSLocalizedString(@"key3", nil),NSLocalizedString(@"tablename4", nil)];
    }
    return _titleName;
}

- (NSArray *)imageName
{
    if (_imageName == nil) {
        _imageName = @[@"wp_list1",@"wp_time",@"wp_click",@"wp_buycar",@"wp_eva"];
    }
    return _imageName;
}

- (UIImageView *)xuanzhuanImageView
{
    if (_xuanzhuanImageView == nil) {
        _xuanzhuanImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/3 + 20,SCREEN_WIDTH/3 + 20)];
        _xuanzhuanImageView.center = CGPointMake(SCREEN_WIDTH/2, 30 + (SCREEN_WIDTH/3 + 10)/2);
        _xuanzhuanImageView.backgroundColor = [UIColor clearColor];
        _xuanzhuanImageView.image = [UIImage imageNamed:@"ery"];
        [allview addSubview:_xuanzhuanImageView];
    }
    return _xuanzhuanImageView;
}

- (GoogleNativeView *)NativeView {
    if (_NativeView == nil) {
        _NativeView = [[GoogleNativeView alloc] initWithFrame:CGRectMake(0, allview.frame.size.height-100, SCREEN_WIDTH, 100)];
        [_NativeView setup:self key:nativeKey];
    }
    return _NativeView;
}

- (NSArray *)URLArray {
    if (_URLArray == nil) {
        _URLArray = @[[NSString stringWithFormat:@"%@/lygamesService.asmx/getPByTypeLQY?apikey=lygames_0953&type=%@",GONGYOUURL,TYPESTRING],[NSString stringWithFormat:@"%@/lygamesService.asmx/getPByTypeLQY?apikey=lygames_0953&type=%@",GONGYOUURL,TYPESTRING]];
    }
    return _URLArray;
}

//建立链接
- (void)connect {
    //    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"popview"]){
    //        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"popview"];
    //        CLAlertView *alertView = [[CLAlertView alloc] initWithAlertViewWithTitle:@"安装VPN配置文件" text:@"亲爱的用户您好，即将为您安装VPN配置文件，这个操作不会产生任何的安全问题，它仅仅是为了能够让vpn软件正常使用，在点击确定后，请继续点击Allow按钮，并根据苹果设备的提示完成安装。" DefauleBtn:@"确定" cancelBtn:@"取消" defaultBtnBlock:^(UIButton *defaultBtn) {
    //            [self installProfile];
    //        } cancelBtnBlock:^(UIButton *cancelBtn) {
    ////            NSLog(@"%@",cancelBtn.currentTitle);
    //        }];
    //        [alertView show];
    //    }else{
    [self installProfile];
    //    }
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(vpnConfigDidChanged:)
               name:NEVPNConfigurationChangeNotification
             object:nil];
}

#pragma mark - VPN Control
- (void)installProfile {
    
    static NSString *passwordKey = @"k_VPN_Password";
    static NSString *sharedSecretKey = @"k_VPN_sharedSecret";
    
    if ([[ConnectVPNManager sharedManager].currentRouteModel.pass integerValue] == 1)
    {
        //在其他设备登录
        CLAlertView *alertView = [[CLAlertView alloc] initWithAlertViewWithTitle:NSLocalizedString(@"reglog6", nil) text:NSLocalizedString(@"custom10", nil) DefauleBtn:NSLocalizedString(@"userMess8", nil) cancelBtn:NSLocalizedString(@"reglog17", nil) defaultBtnBlock:^(UIButton *defaultBtn) {
            
            LogViewController *logVC = [[LogViewController alloc]init];
            
            NSString *string = self.userData[@"Email"];
            
            if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"]) {
                //判断是否注册过
                NSDictionary* dic = [NSKeyedUnarchiver unarchiveObjectWithFile:LOGINDATA];
                NSString *email = dic[@"Email"];
                
                if (!email) {
                    logVC.isYouke = @"1";
                } else {
                    logVC.isYouke = @"0";
                    logVC.email = email;
                }
            } else {
                logVC.isYouke = @"0";
                logVC.email = self.userData[@"Email"];
                
                //把邮箱存到别处
                NSDictionary *locDic = [NSDictionary dictionaryWithObject:self.userData[@"Email"] forKey:@"Email"];
                
                [NSKeyedArchiver archiveRootObject:locDic toFile:LOGINDATA];
                
                //把本地的邮箱密码删除
                NSDictionary *dic = self.userData;
                
                NSMutableDictionary * muDic = [NSMutableDictionary dictionaryWithDictionary:dic];
                [muDic removeObjectForKey:@"Email"];
                [muDic removeObjectForKey:@"Password"];
                
                NSDictionary *dic1 = [NSDictionary dictionaryWithDictionary:muDic];
                
                //成功，存储本地，进入主界面
                [NSKeyedArchiver archiveRootObject:dic1 toFile:USERALLDATA];
            }
            logVC.isZidong = @"1";
            self.navigationController.tabBarController.tabBar.hidden = YES;
            [self.navigationController pushViewController:logVC animated:YES];
        } cancelBtnBlock:^(UIButton *cancelBtn) {
            
        }];
        [alertView show];
        
    } else {
        routeModel *model =   [ConnectVPNManager sharedManager].currentRouteModel;
        
        [self createKeychainValue:model.pass forIdentifier:passwordKey];
        [self createKeychainValue:@"1234" forIdentifier:sharedSecretKey];
        [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError *error) {
            if (error) {
                return;
            }
            NEVPNProtocolIKEv2 *  p = [[NEVPNProtocolIKEv2 alloc] init];
            p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
            p.serverAddress = model.IP;
            p.username = model.name;
            p.passwordReference = [self searchKeychainCopyMatching:passwordKey];
            p.sharedSecretReference = [self searchKeychainCopyMatching:sharedSecretKey];
            p.remoteIdentifier = model.remoteId;
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            self->_vpnManager.protocolConfiguration = p;
            self->_vpnManager.onDemandEnabled = YES;
            self->_vpnManager.localizedDescription = @" VPN-App内连接";
            self->_vpnManager.enabled = YES;
            
            [self->_vpnManager saveToPreferencesWithCompletionHandler:^(NSError *error) {
                if (error) {
                }
            }];
        }];
    }
}
- (void)vpnConfigDidChanged:(NSNotification *)notification
{
    [self startConnecting];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NEVPNConfigurationChangeNotification
                                                  object:nil];
}
- (void)startConnecting
{
    NSError *startError;
    
    [self.vpnManager.connection startVPNTunnelWithOptions:@{NEVPNConnectionStartOptionUsername:@"ipsEC@ioS%vPn",NEVPNConnectionStartOptionPassword:@"Cespi%NpVsoi2016"} andReturnError:&startError];
    
    if (startError) {
        NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
    }
}
#pragma mark VPN 状态改变的时候
- (void)vpnStatusDidChanged:(NSNotification *)notification
{
    NEVPNStatus status = self.vpnManager.connection.status;
    switch (status) {
        case NEVPNStatusConnected:
        {
            [self.ConnectImageView stopAnimating];
            [self.ConnectImageView removeFromSuperview];
            //连接成功
            self.statusLabel.text = NSLocalizedString(@"btnname4", nil);
            _MainBtn.userInteractionEnabled = YES;
            [_MainBtn setBackgroundImage:[UIImage imageNamed:@"已连接"] forState:UIControlStateNormal];
            
            //由ConnectVPNManager 处理
            [[ConnectVPNManager sharedManager] dealConnectedState];
            //别动
            success = @"1";
            [self connectSuccessRate];
            break;
        }
        case NEVPNStatusInvalid:
        case NEVPNStatusDisconnected:
        {
            [self.ConnectImageView stopAnimating];
            [self.ConnectImageView removeFromSuperview];
            self.statusLabel.text = NSLocalizedString(@"btnname1", nil);
            
            [_MainBtn setBackgroundImage:[UIImage imageNamed:@"未连接"] forState:UIControlStateNormal];
            _MainBtn.userInteractionEnabled = YES;
            
            if ([[ConnectVPNManager sharedManager] dealDisConnectedState]) {
                [self connect];
            }
            
            //别动
            success = @"0";
            [self connectSuccessRate];
            break;
        }
        case NEVPNStatusConnecting:
        case NEVPNStatusReasserting:
        {
            [self.view addSubview:self.ConnectImageView];
            [self.ConnectImageView startAnimating];
            
            self.statusLabel.text = NSLocalizedString(@"btnname3", nil);
            if ([ConnectVPNManager sharedManager].linkTime > 1) {
                NSString *VPntype  = @"IKev2";
                if ([ConnectVPNManager sharedManager].currentVPN == VPN_IPSEC) {
                    VPntype  = @"IPSEC";
                }
                self.statusLabel.text = [NSString stringWithFormat:@"%d次暴力连接",[ConnectVPNManager sharedManager].linkTime - 1];
            }
            
            [_MainBtn setBackgroundImage:[UIImage imageNamed:@"连接中"] forState:UIControlStateNormal];
            
            _MainBtn.userInteractionEnabled = NO;
            
            //别动
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"StatisticsConnect"];
            break;
        }
        case NEVPNStatusDisconnecting:
            self.statusLabel.text = NSLocalizedString(@"btnname2", nil);
            _MainBtn.userInteractionEnabled = NO;
            break;
        default:
            break;
    }
}
//统计连接成功率
-(void)connectSuccessRate
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"StatisticsConnect"]) {
        //如果这个值存在
        //域名 GONGYOUURL
        //IP  self.serverString
        //状态 success
        NSThread *thread=[[NSThread alloc]initWithTarget:self selector:@selector(GetLinkstate) object:nil];
        thread.name=[NSString stringWithFormat:@"ConnectThread"];//设置线程名称
        [thread start];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StatisticsConnect"];
    }
}

- (void) playAdMovie
{
    [[VungleSDK sharedSDK] setDelegate:self];
    VungleSDK *sdk = [VungleSDK sharedSDK];
    NSError *error;
    [sdk playAd:self.navigationController error:&error];
    if (error) {
        [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    }
}

- (void)vungleSDKwillShowAd {
    //通过数据接口进行签到  如果签到成功  提示签到完成   如果已经签到过了  提示您已经签到过了
    NSLog(@"视频广告将要出现");
}

- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary *)viewInfo willPresentProductSheet:(BOOL)willPresentProductSheet {
    [[VungleSDK sharedSDK] setDelegate:nil];
    viewInfo = nil;
    [self qiandao];
}

- (void)vungleSDKwillCloseProductSheet:(id)productSheet {
    NSLog(@"我也是醉了");
}

- (void)dealloc {
    [[VungleSDK sharedSDK] setDelegate:nil];
}


-(void)GetLinkstate {
    NSString * str123 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetLinkstate?apikey=lygames_0953&link=%@&ip=%@&state=%@",GONGYOUURL,GONGYOUURL,[ConnectVPNManager sharedManager].currentRouteModel.IP,success];
    NSURL *url123 = [NSURL URLWithString:str123];
    NSURLRequest *request = [NSURLRequest requestWithURL:url123 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *ADdata11 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
}

#pragma mark - KeyChain
static NSString * const serviceName = @"im.zorro.ipsec_demo.vpn_config";
- (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:serviceName forKey:(__bridge id)kSecAttrService];
    return searchDictionary;
}

- (NSData *)searchKeychainCopyMatching:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [searchDictionary setObject:@YES forKey:(__bridge id)kSecReturnPersistentRef];
    CFTypeRef result = NULL;
    SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, &result);
    return (__bridge_transfer NSData *)result;
}

- (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];
    statuss = SecItemDelete((__bridge CFDictionaryRef)dictionary);
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(__bridge id)kSecValueData];
    statuss = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
    if (statuss == errSecSuccess) {
        return YES;
    }
    return NO;
}

- (void) PopDisEvaPopView
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.view addSubview:self.BackImageView];
        [self.view addSubview:self.DisEvaPopView];
    }];
    weakself(self);
    self.DisEvaPopView.SupPopBlock = ^(){
        //        NSLog(@"好评支持");
        [weakSelf.BackImageView removeFromSuperview];
        [weakSelf.DisEvaPopView removeFromSuperview];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:weakSelf.ADdata[@"MainViewTextAddress"]]];
        [weakSelf IncreaseTime];
    };
    self.DisEvaPopView.CanPopBlock = ^(){
        [weakSelf.BackImageView removeFromSuperview];
        [weakSelf.DisEvaPopView removeFromSuperview];
    };
}
- (void) IncreaseTime
{
    //调用评论加时长的接口
    [LSProgressHUD showWithMessage:NSLocalizedString(@"other5", nil)];
    NSString * pingluntime = [NSString stringWithFormat:@"%@/lygamesService.asmx/LQYIsComment?apikey=lygames_0953&name=%@&time=%d&type=2",GONGYOUURL,[UUID getUUID],360];
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:pingluntime parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [LSProgressHUD hide];
        NSArray * array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        array[0][@"state"] = @"1";
        if ([array[0][@"state"] isEqualToString:@"0"]) {
            //评论过了
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"Evaluatin2", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
            [alertView show];
        } else {
            //评论成功
            self.timeYouXiao = [ChangeTimeString DataChangeTimeString:array[0][@"StopTime"]];
            [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
            self->PanDuanEva = @"0";
            
            //参与好评
            if ([self.ADdata[@"announcement"] isEqualToString:@"1"])
            {
                if ([self->PanDuanEva isEqualToString:@"-1"])
                {
                    [self->goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"MainViewImageAddress"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        
                        self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 60*KWidth_Scale, 60*KWidth_Scale);
                        
                        if (IS_IPAD) {
                            self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 40*KWidth_Scale, 40*KWidth_Scale);
                        }
                        
                        //                        goodEvaImageView.height = goodEvaImageView.height - 8;
                        //                        goodEvaImageView.width = image.size.width * (goodEvaImageView.height / image.size.height);
                        //                        goodEvaImageView.center = CGPointMake(goodEvaImageView.width/2, goodEvaImageView.height/2);
                        
                    }];
                }else {
                    [self->goodEvaImageView sd_setImageWithURL:[NSURL URLWithString:[self.ADdata[@"MainImage"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 60*KWidth_Scale, 60*KWidth_Scale);
                        
                        if (IS_IPAD) {
                            self->goodEvaImageView.frame = CGRectMake(SCREEN_WIDTH - 80*KWidth_Scale,NaviHeight, 40*KWidth_Scale, 40*KWidth_Scale);
                        }
                        //                        goodEvaImageView.height = goodEvaImageView.height - 8;
                        //                        goodEvaImageView.width = image.size.width * (goodEvaImageView.height / image.size.height);
                        //                        goodEvaImageView.center = CGPointMake(goodEvaImageView.width/2, goodEvaImageView.height/2);
                    }];
                }
            }
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"Evaluatin1", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [LSProgressHUD hide];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)qiandao {
    NSString * QianDaoTime = [NSString stringWithFormat:@"%@/lygamesService.asmx/LQYDailyAttendance?apikey=lygames_0953&name=%@&time=%d",GONGYOUURL,[UUID getUUID],30];
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:QianDaoTime parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (array[0][@"state"] == nil) {
            self.timeYouXiao = [ChangeTimeString DataChangeTimeString:array[0][@"StopTime"]];
            self.currentTime = [ChangeTimeString DataChangeTimeString:array[0][@"currentTime"]];
            [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
            //            [self.tableView reloadData];
            self.navigationItem.title = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"tablename2", nil),self.timeYouXiao];
            if (IS_IPHONEX) {
                self.navigationItem.title = self.timeYouXiao;
            }
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"alert6", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
            [alertView show];
        } else {
            if ([array[0][@"state"] integerValue] == -1) {
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"other4", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
                [alertView show];
            }
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self showErrorWithTitle:NSLocalizedString(@"other3", nil) autoCloseTime:2];
    }];
}

- (void)showMessage:(NSString *)message duration:(NSTimeInterval)time {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [UIColor grayColor];
    showview.frame = CGRectMake(1, 1, 1, 1);
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    
    UILabel *label = [[UILabel alloc]init];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.f],
                                 NSParagraphStyleAttributeName:paragraphStyle.copy};
    CGSize labelSize = [message boundingRectWithSize:CGSizeMake(207, 999)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes context:nil].size;
    
    label.frame = CGRectMake(10, 5, labelSize.width +20, labelSize.height);
    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = 1;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:15];
    [showview addSubview:label];
    showview.frame = CGRectMake((screenSize.width - labelSize.width - 20)/2,
                                screenSize.height - 300,
                                labelSize.width+40,
                                labelSize.height+10);
    [UIView animateWithDuration:time animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}

- (NSString *)panduan {
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}

- (NSString*)currentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}

@end
