//
//  ForeignNavTableViewCell.h
//   VPN
//
//  Created by cacac on 2017/2/17.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^ClickNetworkBlock)(NSInteger);

@interface ForeignNavTableViewCell : UITableViewCell

- (void) cellDataMessage:(NSArray *)imageArray name:(NSArray *)nameArray detail:(NSArray *)detailArray index:(NSInteger)index;

- (IBAction)clickNet:(UIButton *)sender;

@property (nonatomic,copy) ClickNetworkBlock  ClickBlock;

@end
