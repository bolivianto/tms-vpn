//
//  BuyRecordViewController.m
//   VPN
//
//  Created by cacac on 2017/3/10.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "BuyRecordViewController.h"
#import "AFNetworking.h"
#import "LSProgressHUD.h"
#import "JiLuTableViewCell.h"
#import "ChangeTimeString.h"
@interface BuyRecordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSMutableArray * dataSource;
@property (nonatomic,strong) NSMutableDictionary * Userdict;

@end

@implementation BuyRecordViewController

-(void)viewWillAppear:(BOOL)animated
{
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    [_tableView reloadData];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = NSLocalizedString(@"user5", nil);
    [self createNavi];
    [self getrequestdata];
    [self createtableview];
}

/*
 - (void)jumpImmediatelyToIndex:(NSInteger)index animated:(BOOL)animated
 {
 if (index < 0) return;
 if (index > _pageCount - 1) return;
 if (!animated)
 {
 [self jumpToIndex:index animated:NO];
 return;
 }
 
 CGSize  baseSize = self.frame.size;
 SPReuseCell *shownCell = [self getReuseCell:NO];
 
 if (ABS(index-_currentPageNumber) > 1) {
 _jumpInfo.isImmediateJump = YES;
 _jumpInfo.targetIndex = index;
 NSInteger tempIndex = index>_currentPageNumber?index-1:index+1;
 shownCell.frame = CGRectMake(tempIndex*baseSize.width, 0, baseSize.width, baseSize.height);
 [self setContentOffset:CGPointMake(baseSize.width*tempIndex, 0) animated:NO];
 }
 [self jumpToIndex:index animated:animated];
 }
 
 
 #pragma mark - Dequeue Page View
 - (UIView *)dequeuePageViewWithIndex:(NSInteger)index
 {
 return [_pageMap objectForKey:@(index)];
 }
 
 #pragma mark - Override
 - (void)setPagingEnabled:(BOOL)pagingEnabled
 {
 pagingEnabled = YES;
 [super setPagingEnabled:pagingEnabled];
 }
 
 #pragma mark - Set/Get
 - (void)setSp_delegete:(id<SPScrollPageViewDelegate>)asp_delegete
 {
 if (sp_delegete != asp_delegete) {
 sp_delegete = asp_delegete;
 
 _spDelegateRespondsTo.didEndBounce = [sp_delegete respondsToSelector:
 @selector(scrollPageDidEndBounceAtPage:index:)];
 _spDelegateRespondsTo.pageForIndex = [sp_delegete respondsToSelector:
 @selector(scrollPageView:pageForIndex:)];
 }
 }
*/

- (void) getrequestdata
{
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    
    NSString * urlPath=  [NSString stringWithFormat:@"%@/lygamesService.asmx/GetTransactionRecord?apikey=lygames_0953&name=%@",GONGYOUURL,_Userdict[@"UserName"]];
    
    NSURL *url = [NSURL URLWithString:urlPath];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (userdata){
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        
        if (array)
        {
            [self.dataSource addObjectsFromArray:array];
            [self.tableView reloadData];
        }
    }
}
- (NSMutableArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource =[NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}
- (void) createtableview
{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NaviHeight)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,  CGRectGetMaxY(view.frame) + 5*KWidth_Scale, SCREEN_WIDTH, SCREEN_HEIGHT - NaviHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"JiLuTableViewCell" bundle:nil] forCellReuseIdentifier:@"JiLuTableViewCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JiLuTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"JiLuTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.bianhaolabel.text = [NSString stringWithFormat:@"%@：%@",NSLocalizedString(@"user11", nil),self.dataSource[indexPath.row][@"OrderNum"]];
    cell.pricelabel.text = [NSString stringWithFormat:@"%@：¥%@",NSLocalizedString(@"user12", nil),self.dataSource[indexPath.row][@"TransactionAmount"]];
    cell.timelabel.text = [NSString stringWithFormat:@"%@：%@ day",NSLocalizedString(@"user13", nil),self.dataSource[indexPath.row][@"OpeningTime"]];
    cell.datelabel.text = [NSString stringWithFormat:@"%@：%@",NSLocalizedString(@"user14", nil),[ChangeTimeString DataChangeTimeString:self.dataSource[indexPath.row][@"TransactionDate"]]];
    
    
    cell.bianhaolabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.bianhaolabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    
    cell.pricelabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.pricelabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    cell.timelabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.timelabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    cell.datelabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.datelabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    
    return cell;
}

- (BOOL)shouldAutorotate {
    return NO;
}
- (void) createNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
    
}

- (void) backMainVC:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
