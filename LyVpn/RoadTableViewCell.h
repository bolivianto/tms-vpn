//
//  RoadTableViewCell.h
//  openvpn
//
//  Created by cacac on 16/10/19.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoadTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *roadImage;

@property (weak, nonatomic) IBOutlet UILabel *roadLabel;

@property (weak, nonatomic) IBOutlet UIImageView *chooseImageView;


@end
