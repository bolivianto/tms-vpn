//
//  LogViewController.m
//   VPN
//
//  Created by a on 2017/7/21.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "LogViewController.h"
#import "BuysViewController.h"
#import "ForgetViewController.h"
#import "ZiDongConfragitionViewController.h"
#import "UsersViewController.h"
#import "YPGroupBtnView.h"
#import "OldViewController.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"

#define kAlphaNum   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@"

@interface LogViewController ()<UITextFieldDelegate,GroupButtonViewDelegate,UIScrollViewDelegate>
{
    UITextField *emailText;
    UITextField *passText;
    UITextField *emailText1;
    UITextField *passText1;
    UITextField *conpassText;
    
    BOOL isLook;
    BOOL isLook1;
    
    UIButton *lookPassBtn;
    UIButton *lookPassBtn1;
    
    UIButton *regButton;
    UIButton *logButton;
    UIView *backView1;
    //各种弹窗提示
    UIAlertView *alert;
    UIAlertView *kongalert;
    UIAlertView *formatalert;
    UIAlertView *erroralert;
    UIAlertView *againalert;
    UIButton *qukLoginBtn;
    UIButton *oldUserBtn;
    NSString *userName;
    NSString *passWord;
    NSString *usermail;
    NSString *userStatus;
    UISegmentedControl *segment;
    BOOL isLog;
    UILabel *forgetPass;
    UIView *allView;
    UIScrollView *mainScroll;
    YPGroupBtnView *buttonView1;
}

@property (nonatomic,retain) UIImageView * backGroupImageView;
@property (nonatomic,strong) NSMutableDictionary * Userdict;
@property (nonatomic,strong) UIWindow * window;
@property(nonatomic,strong) YPGroupBtnView *buttonView;
@property (nonatomic, strong) NSString *script;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) UIView *weexView;
@property (nonatomic, strong) NSArray *refreshList;
@property (nonatomic, strong) NSArray *refreshList1;
@property (nonatomic, strong) NSArray *refresh;
@property (nonatomic) NSInteger count;
@end

@implementation LogViewController
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
//老用户找回
- (void)goToOldUserPage {
    [self performSegueWithIdentifier:@"showOldUserLoginSegue" sender:self];
}

#pragma mark --GroupButtonViewDelegate
- (void)GroupButtonView:(YPGroupBtnView *)view ClickButton:(UIButton *)button {
    NSInteger page = self.buttonView.selectIndex;
    CGFloat offsetX = page * SCREEN_WIDTH;
    CGPoint offset = CGPointMake(offsetX, 0);
    [mainScroll setContentOffset:offset animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    self.tabBarController.tabBar.hidden = YES;
    [self creatNavi];
    isLog = 1;
    self.view.backgroundColor = [UIColor whiteColor];
    allView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    allView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:allView];
    
    //注册
    // Register
    [self logView];
    
    //增加监听，当键盘出现或改变时收出消息
    // Increase the listener and receive the message when the keyboard appears or changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //增加监听，当键退出时收出消息
    // Increase the listener, receive the message when the key exits
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    ////******************/
    [self startSplashScreen];
    ////******************/
}
//当键盘出现或改变时调用
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    allView.frame = CGRectMake(0, -height/2, SCREEN_WIDTH, SCREEN_HEIGHT);
}

//当键退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification{
    allView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
}

-(void)creatNavi {
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
}
- (NSMutableDictionary *)Userdict
{
    if (_Userdict == nil) {
        _Userdict = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return _Userdict;
}
-(void)logView
{
    UIImageView *topImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    topImage.image = [UIImage imageNamed:@"background"];
    [allView addSubview:topImage];
    
    
    ///logo
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH / 3.5, SCREEN_WIDTH / 3.5)];
    logoImageView.image = [UIImage imageNamed:@"topLogo"];
    logoImageView.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 6);
    [allView addSubview:logoImageView];
    
    isLog = 1;
    
    buttonView1 = [[YPGroupBtnView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6, SCREEN_HEIGHT / 3 , SCREEN_WIDTH / 3 * 2, 30*KWidth_Scale) andDataArray:@[NSLocalizedString(@"reglog4", nil),NSLocalizedString(@"reglog3", nil)]];
    //    if (IS_IPHONE_5)
    //    {
    //        buttonView1.frame = CGRectMake(SCREEN_WIDTH / 6, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 3 * 2, 50);
    //    }
    buttonView1.delegate = self;
    buttonView1.showAnimation = YES;
    buttonView1.clickShowAnimation = NO;
    buttonView1.tintColor = [UIColor whiteColor];
    self.buttonView = buttonView1;
    [allView addSubview:_buttonView];
    
    mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _buttonView.frame.size.height + _buttonView.frame.origin.y + 5*KWidth_Scale, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_buttonView.frame) - 10*KWidth_Scale)];
    if (IS_IPAD) {
        mainScroll.frame = CGRectMake(0, _buttonView.frame.size.height + _buttonView.frame.origin.y + 5*KWidth_Scale, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_buttonView.frame) - 10*KWidth_Scale);
    }
    mainScroll.contentSize = CGSizeMake(SCREEN_WIDTH * 2, mainScroll.frame.size.height);
    mainScroll.bounces = NO;
    mainScroll.delegate = self;
    mainScroll.pagingEnabled = YES;
    mainScroll.showsHorizontalScrollIndicator=NO;
    mainScroll.backgroundColor = [UIColor whiteColor];
    [allView addSubview:mainScroll];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self createLog];
    [self creatReg];
    
    //网络异常提示
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                       message:NSLocalizedString(@"reglog18", nil)
                                      delegate:self
                             cancelButtonTitle:nil
                             otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //格式不正确提示
    kongalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                           message:NSLocalizedString(@"reglog7", nil)
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //账号或密码不正确
    erroralert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"reglog19", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    //邮箱格式不正确
    formatalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                             message:NSLocalizedString(@"reglog20", nil)
                                            delegate:self
                                   cancelButtonTitle:nil
                                   otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    //不能重复注册
    againalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"reglog6", nil)
                                            message:NSLocalizedString(@"reglog21", nil)
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"reglog17", nil), nil];
    
    
    
}
-(void)createLog {
    UIView *logView = [[UIView alloc]initWithFrame:mainScroll.bounds];
    UILabel *emailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,segment.frame.origin.y + segment.frame.size.height + 30*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    emailTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    emailTextLabel.text = NSLocalizedString(@"reglog16", nil);
    emailTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [logView addSubview:emailTextLabel];
    
    UIImageView *emailTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    emailTextImage.image = [UIImage imageNamed:@"矩形4"];
    [logView addSubview:emailTextImage];
    
    
    emailText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    
    //设置边框样式，只有设置了才会显示边框样式
    emailText.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    emailText.backgroundColor = [UIColor clearColor];
    emailText.returnKeyType = UIReturnKeyDone;
    //文字居中
    emailText.textAlignment = 1;
    //当输入框没有内容时，水印提示 提示内容为邮箱
    emailText.placeholder = NSLocalizedString(@"reglog16", nil);
    [emailText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    emailText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    //设置输入框内容的字体样式和大小
    emailText.font = [UIFont fontWithName:@"Arial" size:15.0f*KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    emailText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    emailText.clearsOnBeginEditing = NO;
    //设置键盘的样式
    emailText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    emailText.delegate = self;
    
    [logView addSubview:emailText];
    
    UILabel *passTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailText.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    passTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    passTextLabel.text = NSLocalizedString(@"reglog2", nil);
    passTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [logView addSubview:passTextLabel];
    
    UIImageView *passTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passTextImage.image = [UIImage imageNamed:@"矩形4"];
    [logView addSubview:passTextImage];
    //密码
    passText = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passText.textAlignment = 1;
    
    //设置边框样式，只有设置了才会显示边框样式
    passText.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    passText.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    passText.placeholder = NSLocalizedString(@"reglog2", nil);
    [passText setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    passText.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    //设置输入框内容的字体样式和大小
    passText.font = [UIFont fontWithName:@"Arial" size:15.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    passText.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    passText.clearsOnBeginEditing = NO;
    //设置键盘的样式
    passText.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    passText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    passText.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    passText.secureTextEntry = YES;
    
    //return键变成什么键
    passText.returnKeyType = UIReturnKeyDone;
    
    [logView addSubview:passText];
    
    //忘记密码
    forgetPass = [[UILabel alloc]initWithFrame:CGRectMake(passText.frame.origin.x + passText.frame.size.width - 180, CGRectGetMaxY(passText.frame) + 10*KWidth_Scale, 180, 30)];
    forgetPass.userInteractionEnabled = YES;
    forgetPass.backgroundColor = [UIColor clearColor];
    forgetPass.font = [UIFont systemFontOfSize:10*KWidth_Scale];
    forgetPass.text = NSLocalizedString(@"reglog11", nil);
    forgetPass.textColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1];
    
    UITapGestureRecognizer *TapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(forgetPassWord)];
    [forgetPass addGestureRecognizer:TapGesturRecognizer];
    forgetPass.textAlignment = 2;
    [logView addSubview:forgetPass];
    
    //登录按钮
    logButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6, CGRectGetMaxY(forgetPass.frame) + 20*KWidth_Scale, SCREEN_WIDTH / 3*2, 40*KWidth_Scale)];
    logButton.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    [logButton setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    [logButton setTitle:NSLocalizedString(@"reglog4", nil) forState:UIControlStateNormal];
    [logButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [logView addSubview:logButton];
    
    
    
    //游客登录按钮
    if ([self.isYouke isEqualToString:@"1"]) {
        //证明从没有注册过，显示游客登录
        //有问题，如果已经购买套餐呢
        qukLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2, CGRectGetMaxY(logButton.frame) + 15*KWidth_Scale, passText.frame.size.width / 2, 30*KWidth_Scale)];
        qukLoginBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*KWidth_Scale];
        [qukLoginBtn setTitle:NSLocalizedString(@"reglog5", nil) forState:UIControlStateNormal];
        [qukLoginBtn setTitleColor:[UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1] forState:UIControlStateNormal];
        [qukLoginBtn addTarget:self action:@selector(qukLoginBtn) forControlEvents:UIControlEventTouchUpInside];
        [logView addSubview:qukLoginBtn];
        oldUserBtn = [[UIButton alloc]initWithFrame:CGRectMake(passText.frame.origin.x, CGRectGetMaxY(logButton.frame) + 15*KWidth_Scale, passText.frame.size.width / 2, 30*KWidth_Scale)];
        oldUserBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*KWidth_Scale];
        [oldUserBtn setTitle:NSLocalizedString(@"reglog15", nil) forState:UIControlStateNormal];
        [oldUserBtn setTitleColor:[UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1] forState:UIControlStateNormal];
        [oldUserBtn addTarget:self action:@selector(goToOldUserPage) forControlEvents:UIControlEventTouchUpInside];
        [logView addSubview:oldUserBtn];
    } else {
        //注册过，不显示游客登录，提示邮箱
        emailText.text = self.email;
        oldUserBtn = [[UIButton alloc]initWithFrame:CGRectMake(passText.frame.origin.x + passText.frame.size.width / 4,CGRectGetMaxY(logButton.frame) + 15*KWidth_Scale, passText.frame.size.width / 2, 30*KWidth_Scale)];
        oldUserBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*KWidth_Scale];
        [oldUserBtn setTitle:NSLocalizedString(@"reglog15", nil) forState:UIControlStateNormal];
        [oldUserBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [oldUserBtn addTarget:self action:@selector(goToOldUserPage) forControlEvents:UIControlEventTouchUpInside];
        [logView addSubview:oldUserBtn];
    }
    
    //显示密码
    lookPassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn.center = CGPointMake(CGRectGetMaxX(passText.frame) + 15*KWidth_Scale, passText.center.y);
    [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [logView addSubview:lookPassBtn];
    
    isLook = 0;
    isLook1 = 0;
    
    [mainScroll addSubview:logView];
}
-(void)creatReg
{
    UIView *regView = [[UIView alloc]initWithFrame:CGRectMake(mainScroll.frame.size.width, 0, mainScroll.frame.size.width, mainScroll.frame.size.height)];
    
    
    UILabel *emailTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,segment.frame.origin.y + segment.frame.size.height + 30*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    emailTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    emailTextLabel.text = NSLocalizedString(@"reglog1", nil);
    emailTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [regView addSubview:emailTextLabel];
    
    
    
    UIImageView *emailTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    emailTextImage.image = [UIImage imageNamed:@"矩形4"];
    [regView addSubview:emailTextImage];
    
    
    emailText1 = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    
    //设置边框样式，只有设置了才会显示边框样式
    emailText1.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    emailText1.backgroundColor = [UIColor clearColor];
    
    emailText1.returnKeyType = UIReturnKeyDone;
    
    //文字居中
    emailText1.textAlignment = 1;
    //当输入框没有内容时，水印提示 提示内容为邮箱
    emailText1.placeholder = NSLocalizedString(@"reglog1", nil);
    [emailText1 setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    emailText1.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    
    //设置输入框内容的字体样式和大小
    emailText1.font = [UIFont fontWithName:@"Arial" size:15.0f*KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    emailText1.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    emailText1.clearsOnBeginEditing = NO;
    //设置键盘的样式
    emailText1.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    emailText1.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    emailText1.delegate = self;
    //最右侧加图片是以下代码   左侧类似
    [regView addSubview:emailText1];
    
    UILabel *passTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(emailText1.frame) + 20*KWidth_Scale, SCREEN_WIDTH /3*2, 15*KWidth_Scale)];
    passTextLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    passTextLabel.text = NSLocalizedString(@"reglog2", nil);
    passTextLabel.font = [UIFont systemFontOfSize:12*KWidth_Scale];
    [regView addSubview:passTextLabel];
    
    UIImageView *passTextImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    passTextImage.image = [UIImage imageNamed:@"矩形4"];
    [regView addSubview:passTextImage];
    
    
    //密码
    //初始化textfield并设置位置及大小
    passText1 = [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6,CGRectGetMaxY(passTextLabel.frame) + 10*KWidth_Scale, SCREEN_WIDTH /3*2, 35*KWidth_Scale)];
    
    
    //      文字从中间向两边扩展
    passText1.textAlignment = 1;
    
    //设置边框样式，只有设置了才会显示边框样式
    passText1.borderStyle = UITextBorderStyleNone;
    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
    passText1.backgroundColor = [UIColor clearColor];
    //当输入框没有内容时，水印提示 提示内容为邮箱
    passText1.placeholder = NSLocalizedString(@"reglog2", nil);
    [passText1 setValue:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    passText1.textColor = [UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1];
    
    //设置输入框内容的字体样式和大小
    passText1.font = [UIFont fontWithName:@"Arial" size:15.0f *KWidth_Scale];
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    passText1.clearButtonMode = UITextFieldViewModeWhileEditing;
    //再次编辑就清空
    passText1.clearsOnBeginEditing = NO;
    //设置键盘的样式
    passText1.keyboardType = UIKeyboardTypeEmailAddress;
    //首字母是否大写
    passText1.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //设置代理 用于实现协议
    passText1.delegate = self;
    //每输入一个字符就变成点 用语密码输入
    passText1.secureTextEntry = YES;
    //最右侧加图片是以下代码   左侧类似
    
    //return键变成什么键
    passText1.returnKeyType = UIReturnKeyDone;
    
    [regView addSubview:passText1];
    
    //显示密码
    lookPassBtn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30*KWidth_Scale, 30*KWidth_Scale)];
    
    lookPassBtn1.center = CGPointMake(CGRectGetMaxX(passText1.frame) + 15*KWidth_Scale, passText.center.y);
    [lookPassBtn1 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
    
    [lookPassBtn1 addTarget:self action:@selector(lookPass1) forControlEvents:UIControlEventTouchUpInside];
    
    [regView addSubview:lookPassBtn1];
    
    isLook = 0;
    isLook1 = 0;
    
    //注册按钮
    regButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 6, CGRectGetMaxY(forgetPass.frame) + 20*KWidth_Scale, SCREEN_WIDTH / 3*2, 40*KWidth_Scale)];
    
    regButton.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    
    [regButton setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];
    
    [regButton setTitle:NSLocalizedString(@"reglog3", nil) forState:UIControlStateNormal];
    
    [regButton addTarget:self action:@selector(resetPassword) forControlEvents:UIControlEventTouchUpInside];
    
    [regView addSubview:regButton];
    
    
    [mainScroll addSubview:regView];
    
    
}

#pragma mark - UIScrollViewDelegate
/**
 *  当scrollView正在滚动就会调用
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView==mainScroll) {
        // 根据scrollView的滚动位置决定pageControl显示第几页
        CGFloat scrollW = scrollView.frame.size.width;
        int page = (scrollView.contentOffset.x+0.5*scrollW)/ scrollW;
        self.buttonView.selectIndex = page;
        
        NSLog( @"***===*****  %ld ",(long)self.buttonView.selectIndex);
    }
}


-(void)forgetPassWord
{
    [self performSegueWithIdentifier:@"showForgotPasswordSegue" sender:self];
}

//判断语言
- (NSString*)getPreferredLanguage
{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
    return preferredLang;
}

- (void)startSplashScreen
{
    UIView* splashView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    splashView.backgroundColor = [UIColor clearColor];
    
    UIImageView *iconImageView = [UIImageView new];
    UIImage *icon = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"weex-icon" ofType:@"png"]];
    if ([icon respondsToSelector:@selector(imageWithRenderingMode:)]) {
        iconImageView.image = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iconImageView.tintColor = [UIColor whiteColor];
    } else {
        iconImageView.image = icon;
    }
    iconImageView.frame = CGRectMake(0, 0, 320, 320);
    iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    iconImageView.center = splashView.center;
    
    
    float animationDuration = 1.4;
    CGFloat shrinkDuration = animationDuration * 0.3;
    CGFloat growDuration = animationDuration * 0.7;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [UIView animateWithDuration:shrinkDuration delay:1.0 usingSpringWithDamping:0.7f initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            CGAffineTransform scaleTransform = CGAffineTransformMakeScale(0.75, 0.75);
            iconImageView.transform = scaleTransform;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:growDuration animations:^{
                CGAffineTransform scaleTransform = CGAffineTransformMakeScale(20, 20);
                iconImageView.transform = scaleTransform;
                splashView.alpha = 0;
            } completion:^(BOOL finished) {
                [splashView removeFromSuperview];
            }];
        }];
    } else {
        [UIView animateWithDuration:shrinkDuration delay:1.0 options:0 animations:^{
            CGAffineTransform scaleTransform = CGAffineTransformMakeScale(0.75, 0.75);
            iconImageView.transform = scaleTransform;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:growDuration animations:^{
                CGAffineTransform scaleTransform = CGAffineTransformMakeScale(20, 20);
                iconImageView.transform = scaleTransform;
                splashView.alpha = 0;
            } completion:^(BOOL finished) {
            }];
        }];
    }
}

- (NSString *)MD5:(NSString *)mdStr {
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}


//注册登录切换
-(void)regButtonAC
{
    //注册，首先判断是否为空
    if (![emailText1.text isEqualToString:@""] && ![passText1.text isEqualToString:@""])
    {
        //判断是否为邮箱
        if ([self checkEmail:emailText1.text]){
            
            NSString *name = _Userdict[@"UserName"];
            
            NSString *md5Pass = [self MD5:passText1.text];
            
            NSString *userString;
            if (name){
                userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserReg?apikey=lygames_0953&uuid=%@&name=%@&mail=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],name,emailText1.text,md5Pass,TYPESTRING];
            } else {
                //如果没有用户名
                userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserReg?apikey=lygames_0953&uuid=%@&name=&mail=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],emailText1.text,md5Pass,TYPESTRING];
            }
            
            NSURL *url = [NSURL URLWithString:userString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
            NSURLResponse *response = nil;
            NSError *error = nil;
            NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (userdata) {
                NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
                if (array.count > 0){
                    userStatus = array[0][@"state"];
                    
                    if ([userStatus isEqualToString:@"0"]){
                        //注册失败，提醒用户手机或邮箱不能重复注册
                        [againalert show];
                        
                    } else if ([userStatus isEqualToString:@"-1"]) {
                        //apikey输入错误
                        [alert show];
                    } else {
                        //注册成功，存储本地，进入主界面
                        [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                        [KeyChainStore save:KEY_USER_LOGINDATA data:array[0]];
                        self.tabBarController.tabBar.hidden = NO;
                        //pop到指定界面
                        if ([_isZidong isEqualToString:@"1"])
                        {
                            for (UIViewController *controller in self.navigationController.viewControllers) {
                                if ([controller isKindOfClass:[ZiDongConfragitionViewController class]]) {
                                    ZiDongConfragitionViewController *zidongVC =(ZiDongConfragitionViewController *)controller;
                                    [self.navigationController popToViewController:zidongVC animated:YES];
                                }
                            }
                        }else if ([_isZidong isEqualToString:@"2"])
                        {
                            for (UIViewController *controller in self.navigationController.viewControllers) {
                                if ([controller isKindOfClass:[BuysViewController class]]) {
                                    BuysViewController *zidongVC =(BuysViewController *)controller;
                                    [self.navigationController popToViewController:zidongVC animated:YES];
                                }
                            }
                        }else{
                            for (UIViewController *controller in self.navigationController.viewControllers) {
                                if ([controller isKindOfClass:[UsersViewController class]]) {
                                    UsersViewController *usersVC =(UsersViewController *)controller;
                                    [self.navigationController popToViewController:usersVC animated:YES];
                                }
                            }
                        }
                    }
                }else
                {
                    //解析失败，请求重新解析
                    [alert show];
                }
            } else {
                //请求失败，网络加载失败，重新加载
                [alert show];
            }
        } else {
            //格式不正确,提示用户
            [formatalert show];
        }
    } else {
        //输入为空，提示用户
        [kongalert show];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [emailText resignFirstResponder];
    [passText resignFirstResponder];
    [emailText1 resignFirstResponder];
    [passText1 resignFirstResponder];
    return YES;
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [emailText resignFirstResponder];
    [passText resignFirstResponder];
    [emailText1 resignFirstResponder];
    [passText1 resignFirstResponder];
}
//登录
// Login
-(void)logButtonAC
{
    //登录功能
    // Login Function
    if (![emailText.text isEqualToString:@""] && ![passText.text isEqualToString:@""]){
        [SVProgressHUD showWithStatus:@"Loading..."];
        NSString *md5Pass = [self MD5:passText.text];
        NSString *userString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserLogin?apikey=lygames_0953&uuid=%@&name=%@&pass=%@&source=%@",GONGYOUURL,[UUID getUUID],emailText.text,md5Pass,TYPESTRING];
        NSURL *url = [NSURL URLWithString:userString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error == nil) {
            NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
            if (array) {
                [SVProgressHUD dismiss];
                userStatus = array[0][@"state"];
                if ([userStatus isEqualToString:@"2"]) {
                    //Failure, reminder
                    [erroralert show];
                    
                } else if ([userStatus isEqualToString:@"-1"])
                {
                    //apikey输入错误
                    // Input Error
                    [erroralert show];
                } else {
                    // 成功，存储本地，进入主界面
                    // Successful, store local, enter the main interface
                    [Global printLogWithObject:array[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                    [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:array[0]];
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *mainNav = [sb instantiateInitialViewController];
                    if (self.navigationController.isBeingPresented) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    } else if (self.navigationController.presentedViewController == nil) {
                        [self.navigationController presentViewController:mainNav animated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    }
                    
                    /*
                     self.tabBarController.tabBar.hidden = NO;
                     //pop到指定界面
                     if ([_isZidong isEqualToString:@"1"])
                     {
                     for (UIViewController *controller in self.navigationController.viewControllers) {
                     if ([controller isKindOfClass:[ZiDongConfragitionViewController class]]) {
                     ZiDongConfragitionViewController *zidongVC =(ZiDongConfragitionViewController *)controller;
                     [self.navigationController popToViewController:zidongVC animated:YES];
                     }
                     }
                     }else if ([_isZidong isEqualToString:@"2"])
                     {
                     for (UIViewController *controller in self.navigationController.viewControllers) {
                     if ([controller isKindOfClass:[BuysViewController class]]) {
                     BuysViewController *zidongVC =(BuysViewController *)controller;
                     [self.navigationController popToViewController:zidongVC animated:YES];
                     }
                     }
                     } else
                     {
                     for (UIViewController *controller in self.navigationController.viewControllers) {
                     if ([controller isKindOfClass:[UsersViewController class]]) {
                     UsersViewController *usersVC =(UsersViewController *)controller;
                     [self.navigationController popToViewController:usersVC animated:YES];
                     }
                     }
                     }
                     */
                }
            } else {
                //解析失败，请求重新解析
                // Cant parse array JSON
                [SVProgressHUD dismiss];
                [alert show];
            }
        } else {
            //请求失败，网络加载失败，重新加载
            [SVProgressHUD dismiss];
            [alert show];
        }
    }else
    {
        //输入为空，提示用户
        // The input is empty, prompting the user
        [kongalert show];
    }
}
// 游客登录
// Guest login
- (void)qukLoginBtn {
    [SVProgressHUD show];
    NSString *name = _Userdict[@"UserName"];
    NSString *string;
    if (name) {
        string = [NSString stringWithFormat:@"%@/usertest/ios.asmx/PayvisitorLoginReg?apikey=lygames_0953&uuid=%@&name=%@&source=%@",GONGYOUURL,[UUID getUUID],name,TYPESTRING];
    } else {
        string = [NSString stringWithFormat:@"%@/usertest/ios.asmx/PayvisitorLoginReg?apikey=lygames_0953&uuid=%@&name=&source=%@",GONGYOUURL,[UUID getUUID],TYPESTRING];
    }
    
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error == nil) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        if (array)
        {
            [Global printLogWithObject:array function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            userName = array[0][@"UserName"];
            usermail = array[0][@"UserMail"];
            passWord = array[0][@"UserPass"];
            userStatus = array[0][@"state"];
            
            if ([userStatus isEqualToString:@"0"])
            {
                //注册失败
                // registration failed
                [alert show];
                
            }else if ([userStatus isEqualToString:@"-1"])
            {
                //apikey输入错误
                // Apikey input error
                [alert show];
            } else if ([userStatus isEqualToString:@"3"])
            {
                //登录失败
                // Login failed
                [alert show];
            } else {
                //注册成功，存储本地，进入主界面
                // Successful registration, store local, enter the main interface
                [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
                [KeyChainStore save:KEY_USER_LOGINDATA data:array[0]];
                self.tabBarController.tabBar.hidden = NO;
                [self.navigationController dismissViewControllerAnimated:true completion:nil];
            }
        } else {
            //解析失败，请求重新解析
            // Parsing failed, requesting reparsing
            [alert show];
        }
    } else {
        //请求失败，网络加载失败，重新加载
        // Request failed, network loading failed, reload
        [alert show];
    }
    [SVProgressHUD dismiss];
}

//显示密码
-(void)lookPass {
    if (isLook) {
        [lookPassBtn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        passText.secureTextEntry = YES;
        isLook = 0;
    } else {
        [lookPassBtn setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        passText.secureTextEntry = NO;
        isLook = 1;
    }
}
//显示密码
-(void)lookPass1 {
    if (isLook1) {
        [lookPassBtn1 setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        passText.secureTextEntry = YES;
        isLook1 = 0;
    } else {
        [lookPassBtn1 setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        passText1.secureTextEntry = NO;
        isLook1 = 1;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"showOldUserLoginSegue"]) {
        OldViewController *vc = segue.destinationViewController;
        if (self.isZidong) {
            vc.isZidong = self.isZidong;
        } else {
            vc.isZidong = @"1";
        }
    }
    if ([segue.identifier isEqual: @"showForgotPasswordSegue"]) {
        ForgetViewController *vc = segue.destinationViewController;
        vc.isZidong = self.isZidong;
    }
}

#pragma mark 判断邮箱手机号合法性
- (BOOL)checkPhone:(NSString *)phoneNumber {
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:phoneNumber];
    if (!isMatch) {
        return NO;
    }
    return YES;
}

- (BOOL)checkEmail:(NSString *)email {
    //^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$
    NSString *regex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [emailTest evaluateWithObject:email];
}


@end
