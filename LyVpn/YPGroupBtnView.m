//
//  YPGroupBtnView.m
//  Inroad
//
//  Created by cyp on 16/1/22.
//  Copyright © 2016年 cyp. All rights reserved.
//

#import "YPGroupBtnView.h"

#define YPDuration 0.2

@interface YPGroupBtnView ()
{
    UIButton *_btn;
    
    UIView *lineView;
//    UIImageView *lineView;
    UIImageView *sanJIao;
    
}

@end

@implementation YPGroupBtnView
- (NSMutableArray *)btnArray{
    
    if (_btnArray==nil) {
        _btnArray=[NSMutableArray array];
    }
    return _btnArray;
    
}

- (void)setShowAnimation:(BOOL)showAnimation{
    
    _showAnimation=showAnimation;
    _clickShowAnimation=showAnimation;
    
    lineView.hidden=!showAnimation;
    
}
- (instancetype)initWithFrame:(CGRect)frame andDataArray:(NSArray*) dataArray{
    
    if(self=[super initWithFrame:frame]){
        
        for (int i=0; i<dataArray.count; i++) {
            
            self.userInteractionEnabled=YES;
            
            CGFloat btnWeight=frame.size.width/dataArray.count;
            
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(i*btnWeight, 0, btnWeight, frame.size.height)];
            [button setBackgroundColor:[UIColor clearColor]];
            button.titleLabel.textAlignment=NSTextAlignmentCenter;
            button.titleLabel.numberOfLines=2;
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            button.tag=i+555;
            [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchDown];
            [self addSubview:button];
            
            [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
            
            [self.btnArray addObject:button];
            
            
            if (i==0) {
                [button setSelected:YES];
                _btn=button;
                button.titleLabel.font = [UIFont boldSystemFontOfSize:14*KWidth_Scale];

            }else if (i == 1){
                button.titleLabel.font = [UIFont boldSystemFontOfSize:11*KWidth_Scale];
                
            }
            
            
        }
//        lineView=[[UIImageView alloc]initWithFrame:CGRectMake(30,frame.size.height-5, frame.size.width/dataArray.count-60, 2)];
//        [lineView setBackgroundColor:[UIColor whiteColor]];
//        lineView.userInteractionEnabled=NO;
//        lineView.hidden=YES;
//        [self addSubview:lineView];
//
     
//        sanJIao = [[UIImageView alloc]initWithFrame:CGRectMake(0,frame.size.height-5, 10*KWidth_Scale, 10*KWidth_Scale)];
//        sanJIao.image = [UIImage imageNamed:@"三角形"];
//        [button addSubview:sanJIao];
        
    }

    
    return  self;
    
}
- (void)setButtonTitle:(NSArray *)dataArray{
    
    for (int i=0; i<dataArray.count; i++) {
        
        [[self.btnArray objectAtIndex:i] setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
    }
}

- (void)setLineColor:(UIColor *)lineColor{
    
    _lineColor=lineColor;
    lineView.backgroundColor=lineColor;
    
}

- (void)setTintColor:(UIColor *)tintColor{
    
    [super setTintColor:tintColor];
    lineView.backgroundColor=tintColor;
    [self setButtonTitleClor:tintColor forState:UIControlStateNormal];
    
}
- (instancetype)initWithFrame:(CGRect)frame andDataArray:(NSArray*) dataArray andGapWith:(CGFloat)gap{
    
    if(self=[super initWithFrame:frame])
        
        for (int i=0; i<dataArray.count; i++) {
            
            self.userInteractionEnabled=YES;
            
            CGFloat btnWeight=frame.size.width/dataArray.count-gap;
            
            
            UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(i*(btnWeight+gap), 0, btnWeight, frame.size.height)];
            [button setBackgroundColor:[UIColor clearColor]];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
      
            button.tag= i+555;
            [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchDown];
            [self addSubview:button];
            
            [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
            
            [self.btnArray addObject:button];
            if (i==0) {
                [button setSelected:YES];
                _btn=button;

            }
        }
    
    return  self;
}


- (void)clickButton:(UIButton *)button {
    UIButton *button_one = (UIButton *)[self viewWithTag:(555)];
    UIButton *button_two = (UIButton *)[self viewWithTag:(556)];

    
    switch (button.tag) {
        case 555:
        {
            button_one.titleLabel.font = [UIFont boldSystemFontOfSize:14*KWidth_Scale];
            button_two.titleLabel.font = [UIFont boldSystemFontOfSize:11*KWidth_Scale];


        }
            break;
        case 556:
        {
            button_one.titleLabel.font = [UIFont boldSystemFontOfSize:11*KWidth_Scale];
            button_two.titleLabel.font = [UIFont boldSystemFontOfSize:14*KWidth_Scale];
        }
            break;
        default:
            break;
    }
    
    
    
    if (self.iscanRepeCLick) {
        if (![_btn isEqual:button])
        {
            _selectIndex=button.tag-555;
            _btn.selected=NO;
            button.selected=YES;
            _btn=button;
            
            if (self.showAnimation&&self.clickShowAnimation) {
                [UIView animateWithDuration:YPDuration animations:^{
                    
                    CGRect lineFrame=lineView.frame;
                    lineFrame.origin.x=button.frame.origin.x+30;
                    lineView.frame=lineFrame;
                }];
            }
            
        }
        if ([self.delegate respondsToSelector:@selector(GroupButtonView:ClickButton:)] ){
            
            [self.delegate GroupButtonView:self ClickButton:button];
        }
        
    }else{
        
        if (![_btn isEqual:button])
        {
            _selectIndex=button.tag-555;
            _btn.selected=NO;
            button.selected=YES;
            _btn=button;
            if (self.showAnimation&&self.clickShowAnimation) {
                [UIView animateWithDuration:YPDuration animations:^{
                    
                    CGRect lineFrame=lineView.frame;
                    lineFrame.origin.x=button.frame.origin.x+30;
                    lineView.frame=lineFrame;
                }];
            }
            
            if ([self.delegate respondsToSelector:@selector(GroupButtonView:ClickButton:)] ){
                
                [self.delegate GroupButtonView:self ClickButton:button];
            }
        }
        
    }
    
}
- (void)setButtonBackgroundImage:(UIImage *)image forState:(UIControlState)state{
    
    for (UIButton *button in self.btnArray) {
        [button setBackgroundImage:image forState:state];
    }
}
-(void)setButtonImage:(UIImage *)image forState:(UIControlState)state{
    
    for (UIButton *button in self.btnArray) {
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
        [button setImage:image forState:state];
    }
    
}

-(void)setButtonTitleClor:(UIColor *)color forState:(UIControlState)state{
    
    for (UIButton *button in self.btnArray) {
        [button setTitleColor:color forState:state];
    }
}

-(void)setButtonTitleFont:(UIFont *)font{
    for (UIButton *button in self.btnArray) {
        [button.titleLabel setFont:font];
    }
    
}

-(void)setButtonImage:(UIImage *)image forBtnIndex:(NSUInteger)index andStat:(UIControlState)state{
    
    UIButton *button=[self.btnArray objectAtIndex:index];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [button setImage:image forState:state];
    
}


- (void)setSelectIndex:(NSInteger)selectIndex{
    UIButton *button_one = (UIButton *)[self viewWithTag:(555)];
    UIButton *button_two = (UIButton *)[self viewWithTag:(556)];
    NSLog( @"********   %ld",(long)selectIndex);
    if (selectIndex == 0) {

        button_one.titleLabel.font = [UIFont boldSystemFontOfSize:14*KWidth_Scale];
        button_two.titleLabel.font = [UIFont boldSystemFontOfSize:11*KWidth_Scale];
    }else{
        button_one.titleLabel.font = [UIFont boldSystemFontOfSize:11*KWidth_Scale];
        button_two.titleLabel.font = [UIFont boldSystemFontOfSize:14*KWidth_Scale];
    }
    
    if(selectIndex>self.btnArray.count-1)
        return;
    
    _selectIndex=selectIndex;
    _btn.selected=NO;
    UIButton *button=[self.btnArray objectAtIndex:selectIndex];
    button.selected=YES;
    
    
    if (self.showAnimation) {
        [UIView animateWithDuration:YPDuration animations:^{
            
            CGRect lineFrame=lineView.frame;
            lineFrame.origin.x=button.frame.origin.x+30;
            lineView.frame=lineFrame;
        }];
    }
    _btn = button;

}

@end
