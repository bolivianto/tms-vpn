//
//  BuysTableViewCell.m
//   VPN
//
//  Created by cacac on 2017/2/15.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "BuysTableViewCell.h"


@interface BuysTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *TaoCanName;

@property (weak, nonatomic) IBOutlet UILabel *DanPrice;

@property (weak, nonatomic) IBOutlet UILabel *ZongPrice;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

@implementation BuysTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void) GetCellDataMessage:(NSArray *)array index:(NSInteger)index
{
    
//    switch (index) {
//        case 0:
//            if ([[self panduan] isEqualToString:@"chinese"]) {
//                self.TaoCanName.text = array[0][@"srvname"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%@/月",array[0][@"unitprice"]];
//            }else
//            {
//                self.TaoCanName.text = array[0][@"EnName"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%@/month",array[0][@"unitprice"]];
//            }
//            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[0][@"unitprice"]];
//            break;
//        case 1:
//            if ([[self panduan] isEqualToString:@"chinese"]) {
//                self.TaoCanName.text = array[1][@"srvname"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[1][@"unitprice"] integerValue]/3.00];
//            }else
//            {
//                self.TaoCanName.text = array[1][@"EnName"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[1][@"unitprice"] integerValue]/3.00];
//            }
//            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[1][@"unitprice"]];
//            break;
//        case 2:
//            if ([[self panduan] isEqualToString:@"chinese"]) {
//                self.TaoCanName.text = array[2][@"srvname"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[2][@"unitprice"] integerValue]/6.00];
//            }else
//            {
//                self.TaoCanName.text = array[2][@"EnName"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[2][@"unitprice"] integerValue]/6.00];
//            }
//            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[2][@"unitprice"]];
//            break;
//        case 3:
//            if ([[self panduan] isEqualToString:@"chinese"]) {
//                self.TaoCanName.text = array[3][@"srvname"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[3][@"unitprice"] integerValue]/12.00];
//            }else
//            {
//                self.TaoCanName.text = array[3][@"EnName"];
//                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[3][@"unitprice"] integerValue]/12.00];
//            }
//            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[3][@"unitprice"]];
//            break;
//        default:
//            break;
//    }

    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    switch (index) {
        case 0:

            self.backgroundImage.image = [UIImage imageNamed:@"矩形9"];
            if ([[self panduan] isEqualToString:@"chinese"]) {
                self.TaoCanName.text = array[0][@"srvname"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%@/月",array[0][@"unitprice"]];
            }else
            {
                self.TaoCanName.text = array[0][@"EnName"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%@/month",array[0][@"unitprice"]];
            }
            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[0][@"unitprice"]];
            break;
        case 1:
            self.backgroundImage.image = [UIImage imageNamed:@"矩形9拷贝"];

            if ([[self panduan] isEqualToString:@"chinese"]) {
                self.TaoCanName.text = array[1][@"srvname"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[1][@"unitprice"] integerValue]/3.00];
            }else
            {
                self.TaoCanName.text = array[1][@"EnName"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[1][@"unitprice"] integerValue]/3.00];
            }
            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[1][@"unitprice"]];
            break;
        case 2:
            self.backgroundImage.image = [UIImage imageNamed:@"矩形9拷贝2"];

            if ([[self panduan] isEqualToString:@"chinese"]) {
                self.TaoCanName.text = array[2][@"srvname"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[2][@"unitprice"] integerValue]/6.00];
            }else
            {
                self.TaoCanName.text = array[2][@"EnName"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[2][@"unitprice"] integerValue]/6.00];
            }
            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[2][@"unitprice"]];
            break;
        case 3:
            self.backgroundImage.image = [UIImage imageNamed:@"矩形9拷贝3"];

            if ([[self panduan] isEqualToString:@"chinese"]) {
                self.TaoCanName.text = array[3][@"srvname"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/月",(float)[array[3][@"unitprice"] integerValue]/12.00];
            }else
            {
                self.TaoCanName.text = array[3][@"EnName"];
                self.DanPrice.text = [NSString stringWithFormat:@"￥%0.2f/month",(float)[array[3][@"unitprice"] integerValue]/12.00];
            }
            self.ZongPrice.text = [NSString stringWithFormat:@"￥%@",array[3][@"unitprice"]];
            break;
        default:
            break;
    }
}

- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
//        NSLog(@"current Language == Chinese");
        return @"chinese";
    }
    else{
        lang = @"en";
//        NSLog(@"current Language == English");
        return @"English";
    }
}

-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}
@end
