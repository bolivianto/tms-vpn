//
//  AboutMeViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "AboutMeViewController.h"

@interface AboutMeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray * arrayOne;
@property (nonatomic,strong) NSArray * arrayTwo;

@property (nonatomic,strong) UITableView *tableView;
@end

@implementation AboutMeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = bluecOLOR;
    [self createheaderView];
    [self createNavi];
    [self createtableView];
}
- (NSArray *)arrayOne
{
    if (_arrayOne == nil) {
        
        _arrayOne = @[@"版本",@"qq交流群",@"官方社区"];
    }
    return _arrayOne;
}

- (NSArray *)arrayTwo
{
    if (_arrayTwo == nil) {
        
        _arrayTwo = @[@"1.0",@"459284443",@"暂未公布"];
    }
    return _arrayTwo;
}
- (void) createtableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(15, 243, self.view.frame.size.width - 30, self.view.frame.size.height - 243) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//每个分组中的数据总数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

//告诉表格控件，每一行cell单元格细节
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifier";
    //实例化tableViewCell时，使用initWithStyle方法来进行实例化
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.textLabel.text = self.arrayOne[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.text = self.arrayTwo[indexPath.row];
    return cell;
}

//返回分组的标题文字
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @" VPN";
}
//cell的点击方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            
            break;
        case 1:
            //跳转qq群
        {
            NSString * urlStr = @"https://jq.qq.com/?_wv=1027&k=46PiTlI";
            NSURL *url = [NSURL URLWithString:urlStr];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
        case 2:
            //跳转官方网站
        {
            NSString * urlStr = @"https://jq.qq.com/?_wv=1027&k=46PiTlI";
            NSURL *url = [NSURL URLWithString:urlStr];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
        default:
            break;
    }
}
- (void) createNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:80.0/255.0 green:165.0/255.0 blue:94.0/255.0 alpha:1]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    
}

- (void) backMainVC:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) createheaderView
{
    self.title = @"关于我们";
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 32, 114, 64, 64)];
    imageV.image = [UIImage imageNamed:@"icon.jpg"];
    [self.view addSubview:imageV];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 40, 193, 80, 30)];
    textLabel.text = @"VPN";
    textLabel.font = [UIFont systemFontOfSize:17];
    textLabel.textColor = [UIColor darkGrayColor];
    textLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:textLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (BOOL)shouldAutorotate {
    return NO;
}
@end
