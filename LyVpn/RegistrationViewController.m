//
//  RegistrationViewController.m
//  VPN
//
//  Created by 王树超 on 24/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

#import "RegistrationViewController.h"
#import "VPN-Swift.h"
#import "AFNetworking.h"
#import "ACFloatingTextField.h"
#import "KeyChainStore.h"

@interface RegistrationViewController () {
    NSMutableDictionary *userDict;
    BOOL isShowPassword;
}

@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtPassword;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordButton;

@end

@implementation RegistrationViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Init Show Password
    isShowPassword = NO;
    // Load User Data from Local.
    userDict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    if (userDict == nil) {
        userDict = [KeyChainStore load:KEY_USER_LOGINDATA];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)register:(id)sender {
    if ([self checkValidation]) {
        // Load Saved Username
        NSString *username;
        NSDictionary *parameters;
        if (![userDict[@"UserName"] isKindOfClass:[NSNull class]]) {
            username = userDict[@"UserName"];
        }
        NSString *md5Pass = [Global getMD5StringWithString:self.txtPassword.text];
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserReg", GONGYOUURL];
        if (username) {
            parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": username, @"mail": self.txtEmail.text, @"pass": md5Pass, @"source": TYPESTRING};
        } else {
            parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": @"", @"mail": self.txtEmail.text, @"pass": md5Pass, @"source": TYPESTRING};
        }
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"2"] || [status isEqualToString:@"-1"] || [status isEqualToString:@"0"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"reglog19", nil)];
                } else {
                    // Successful, store local, enter the main interface
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"reglog39", nil)];
                    // Save To Keychain and UserDefault
                    [NSKeyedArchiver archiveRootObject:objectArray[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:objectArray[0]];
                    // Show Main Storyboard
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *mainNav = [sb instantiateInitialViewController];
                    if (self.navigationController.isBeingPresented) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    } else if (self.navigationController.presentedViewController == nil) {
                        [self.navigationController presentViewController:mainNav animated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    }
                }
            }
        }];
        [dataTask resume];
        
        /*
         NSURL *url = [NSURL URLWithString:urlString];
         NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
         NSURLResponse *response = nil;
         NSError *error = nil;
         NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
         
         if (userdata) {
         NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
         if (array.count > 0){
         userStatus = array[0][@"state"];
         
         if ([userStatus isEqualToString:@"0"]){
         //注册失败，提醒用户手机或邮箱不能重复注册
         [againalert show];
         
         } else if ([userStatus isEqualToString:@"-1"]) {
         //apikey输入错误
         [alert show];
         } else {
         //注册成功，存储本地，进入主界面
         [NSKeyedArchiver archiveRootObject:array[0] toFile:USERALLDATA];
         [KeyChainStore save:KEY_USER_LOGINDATA data:array[0]];
         self.tabBarController.tabBar.hidden = NO;
         //pop到指定界面
         if ([_isZidong isEqualToString:@"1"])
         {
         for (UIViewController *controller in self.navigationController.viewControllers) {
         if ([controller isKindOfClass:[ZiDongConfragitionViewController class]]) {
         ZiDongConfragitionViewController *zidongVC =(ZiDongConfragitionViewController *)controller;
         [self.navigationController popToViewController:zidongVC animated:YES];
         }
         }
         }else if ([_isZidong isEqualToString:@"2"])
         {
         for (UIViewController *controller in self.navigationController.viewControllers) {
         if ([controller isKindOfClass:[BuysViewController class]]) {
         BuysViewController *zidongVC =(BuysViewController *)controller;
         [self.navigationController popToViewController:zidongVC animated:YES];
         }
         }
         }else{
         for (UIViewController *controller in self.navigationController.viewControllers) {
         if ([controller isKindOfClass:[UsersViewController class]]) {
         UsersViewController *usersVC =(UsersViewController *)controller;
         [self.navigationController popToViewController:usersVC animated:YES];
         }
         }
         }
         }
         }else
         {
         //解析失败，请求重新解析
         [alert show];
         }
         } else {
         //请求失败，网络加载失败，重新加载
         [alert show];
         }
         */
    }
}

// Hide/Show password
- (IBAction)showPassword {
    if (isShowPassword) {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_hide_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = YES;
        isShowPassword = NO;
    } else {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_show_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = NO;
        isShowPassword = YES;
    }
}

- (BOOL)checkValidation { // Check Validation For Reset Password
    if ([self.txtEmail.text isEqualToString:@""]) { // Empty Email
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if (![Global isValidEmailWithTestStr:self.txtEmail.text]) { // Wrong Email Format
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog20", nil)];
        return NO;
    }
    if ([self.txtPassword.text isEqualToString:@""]) { // Empty Password
        [self.txtPassword showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    return YES;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
