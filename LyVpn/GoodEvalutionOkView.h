//
//  GoodEvalutionOkView.h
//  极光VPN
//
//  Created by cacac on 2017/4/25.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^jujueBlock)();
typedef void(^pingjiaBlock)();
typedef void(^haspingjiaBlock)();
@interface GoodEvalutionOkView : UIView


@property (nonatomic,copy) jujueBlock jujueblock;
@property (nonatomic,copy) pingjiaBlock pingjiablock;
@property (nonatomic,copy) haspingjiaBlock haspingjiablcok;

@end
