//
//  GoodEvalutionOkView.m
//  极光VPN
//
//  Created by cacac on 2017/4/25.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "GoodEvalutionOkView.h"
#import "GoodEvalutionTableViewCell.h"
#import "AFNetworking.h"
@interface GoodEvalutionOkView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel * smallLabel;
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSMutableArray * DataSource;

@property (nonatomic,strong) UIButton * GoodButton;
@property (nonatomic,strong) UIButton * GiveUpButton;
@property (nonatomic,strong) UIButton * HasButton;
@end

@implementation GoodEvalutionOkView

- (NSMutableArray *)DataSource
{
    if (_DataSource == nil) {
        _DataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _DataSource;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    [self setup];
    return self;
}

- (void) setup
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15 * KHeight_Scale, self.frame.size.width - 40, 30 * KHeight_Scale)];
    self.titleLabel.text = NSLocalizedString(@"custom10", nil);
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.textAlignment = 1;
    self.titleLabel.font = [UIFont systemFontOfSize:17*KWidth_Scale];
    [self addSubview:self.titleLabel];
    
    
    self.smallLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, CGRectGetMaxY(self.titleLabel.frame) + 10 * KHeight_Scale, self.frame.size.width - 50, 20 * KHeight_Scale)];
    self.smallLabel.text = NSLocalizedString(@"custom11", nil);
    self.smallLabel.textColor = [UIColor darkGrayColor];
    self.smallLabel.textAlignment = 1;
    self.smallLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    [self addSubview:self.smallLabel];
    
    
    [self GetGoodMessage];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.smallLabel.frame) + 10 * KHeight_Scale, self.frame.size.width - 40, 150 * KHeight_Scale) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.tableView];
    
    //注册
    [self.tableView registerNib:[UINib nibWithNibName:@"GoodEvalutionTableViewCell" bundle:nil] forCellReuseIdentifier:@"GoodEvalutionTableViewCell"];
    
    self.GiveUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.GiveUpButton.frame = CGRectMake(self.frame.size.width/2 - self.frame.size.width/8, CGRectGetMaxY(self.tableView.frame) + 10 * KHeight_Scale, self.frame.size.width/4, 40 * KHeight_Scale);
    [self.GiveUpButton setTitle:NSLocalizedString(@"custom12", nil) forState:UIControlStateNormal];
    [self.GiveUpButton setBackgroundColor:[UIColor darkGrayColor]];
    [self.GiveUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.GiveUpButton.titleLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    self.GiveUpButton.titleLabel.textAlignment = 1;
    [self.GiveUpButton addTarget:self action:@selector(CancelClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.GiveUpButton];
    
    //创建按钮
    self.GoodButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.GoodButton.frame = CGRectMake(CGRectGetMinX(self.GiveUpButton.frame) - self.frame.size.width/4 - 10, CGRectGetMaxY(self.tableView.frame) + 10 * KHeight_Scale, self.frame.size.width/4, 40 * KHeight_Scale);
    [self.GoodButton setTitle:NSLocalizedString(@"custom13", nil) forState:UIControlStateNormal];
    [self.GoodButton setBackgroundColor:bluecOLOR];
    [self.GoodButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.GoodButton.titleLabel.textAlignment = 1;
    self.GoodButton.titleLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    [self.GoodButton addTarget:self action:@selector(CONNECTClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.GoodButton];
    
    self.HasButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.HasButton.frame = CGRectMake(CGRectGetMaxX(self.GiveUpButton.frame) + 10, CGRectGetMaxY(self.tableView.frame) + 10 * KHeight_Scale, self.frame.size.width/4, 40 * KHeight_Scale);
    [self.HasButton setTitle:NSLocalizedString(@"custom14", nil) forState:UIControlStateNormal];
    [self.HasButton setBackgroundColor:bluecOLOR];
    [self.HasButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.HasButton.titleLabel.textAlignment = 1;
    self.HasButton.titleLabel.font = [UIFont systemFontOfSize:14*KWidth_Scale];
    [self.HasButton addTarget:self action:@selector(hasClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.HasButton];
    
}


- (void)GetGoodMessage
{
    NSString * GoodMessageUrl = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetComment?apikey=lygames_0953&languages=0",GONGYOUURL];
    if ([[self panduan] isEqualToString:@"chinese"]) {
        
        GoodMessageUrl = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetComment?apikey=lygames_0953&languages=1",GONGYOUURL];
    }
    
    NSURL *url = [NSURL URLWithString:GoodMessageUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (userdata)
    {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil]; 
        
        if (array)
        {
            [self.DataSource addObjectsFromArray:array];
            [self.tableView reloadData];
        }
        
    }

    
}

#pragma mark  --------------- UITableViewDelegate  &&  UITableViewDataSource   -----
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.DataSource.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 25 * KHeight_Scale;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoodEvalutionTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GoodEvalutionTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setEveryName:self.DataSource index:indexPath.row];
    return cell;
}

- (void) CONNECTClick:(UIButton *)sender
{
//    NSLog(@"好评支持");
    if (_pingjiablock) {
        _pingjiablock();
    }
}

- (void) CancelClick:(UIButton *)sender
{
//    NSLog(@"残忍拒绝");
    if (_jujueblock) {
        _jujueblock();
    }
}

- (void) hasClick:(UIButton *)sender
{
//    NSLog(@"我以评价");
    if (_haspingjiablcok) {
        _haspingjiablcok();
    }
}

- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        return @"chinese";
    }
    else{
        lang = @"en";
        return @"English";
    }
}

-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    return currentLang;
}
@end
