//
//  ConnectPopView.h
//  极光VPN
//
//  Created by cacac on 2017/4/20.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^yesChooseRoadVcBlock)();

typedef void(^cancelRemoveViewBlock)();

@interface ConnectPopView : UIView
- (void) setup:(UIViewController *)controller;
- (void) releaseNativeView;

@property (nonatomic,copy) yesChooseRoadVcBlock YESBlock;
@property (nonatomic,copy) cancelRemoveViewBlock CANCELBlock;
@end
