//
//  EnglishViewController.m
//   VPN
//
//  Created by cacac on 2017/3/28.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "EnglishViewController.h"
#import "DetailViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "LSProgressHUD.h"
#import "SVProgressHUD.h"
static NSString * cellidenty = @"cell";
@interface EnglishViewController ()<UITableViewDelegate,UITableViewDataSource,UMSocialUIDelegate,MFMailComposeViewControllerDelegate>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * DataSource;
@property (nonatomic,strong) MFMailComposeViewController *mailPicker;
@property (nonatomic,strong) NSMutableDictionary * ADdata;

@end

@implementation EnglishViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _ADdata = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];
    
    self.title = @"help";
    [self createNavi];
    [self.view addSubview:self.tableView];
}
- (void) createNavi
{
    //    创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

- (void) backMainVC:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSArray*)DataSource
{
    if (_DataSource == nil) {
        
        _DataSource  = @[@"Why Need to Install Profile",@"Rate app",@"Contact us",@"Share"];
    }
    return _DataSource;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.DataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenty];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [UIImage imageNamed:@"wp_question"];
    cell.textLabel.text = self.DataSource[indexPath.row];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    if (indexPath.row == 0)
    {
        cell.textLabel.text = self.ADdata[@"urlStringEN"];
        
    }else if (indexPath.row == 1)
    {
        cell.textLabel.text = self.ADdata[@"LineSelectionEN"];
        
    }else if (indexPath.row == 2)
    {
        cell.textLabel.text = self.ADdata[@"urlStringEN"];
        
    }else if (indexPath.row == 3)
    {
        cell.textLabel.text = self.ADdata[@"shareEN"];
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * DetailString;
    if (indexPath.row == 0) {
        DetailString = @"Don’t worry! Installing Profile will not cause any security problems.VPN works by proxying your network traffic which must be allowed by system.Tap 'Allow' to start!";
        DetailViewController * detailvc = [[DetailViewController alloc] init];
        detailvc.labeltext = DetailString;
        if ([self.ADdata[@"LandingFailureSwitch"] integerValue] == 1)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"LandingFailureLink"]]];
        }else
        {
            [self.navigationController pushViewController:detailvc animated:YES];
            
        }
    }
    if (indexPath.row == 1) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8",APPID]]];
    }
    if (indexPath.row == 2) {
        
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
        if (!mailClass) {
            NSLog(@"当前系统版本不支持应用内发送邮件功能，您可以使用mailto方法代替");
            return;
        }
        if (![mailClass canSendMail]) {
            NSLog(@"用户没有设置邮件账户");
            return;
        }
        [self displayMailPicker];
    }
    if (indexPath.row == 3) {
        [UMSocialSnsService presentSnsIconSheetView:self
                                             appKey:@"57e38203e0f55a139f003bd4"
                                          shareText:@"Exc VPN take you to see the world, click to download and go to the world with me wandering!"
                                         shareImage:[UIImage imageNamed:@"icon"]
                                    shareToSnsNames:@[UMShareToTwitter,UMShareToFacebook]
                                           delegate:self];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//调出邮件发送窗口
- (void)displayMailPicker
{
    _mailPicker = [[MFMailComposeViewController alloc] init];
    _mailPicker.mailComposeDelegate = self;
    
    //设置主题
    [_mailPicker setSubject: @"Feedback"];
    //添加收件人
    NSArray *toRecipients = [NSArray arrayWithObject: self.ADdata[@"urlStringMail"]];
    [_mailPicker setToRecipients: toRecipients];
    //添加抄送
    NSArray *ccRecipients = [NSArray arrayWithObjects:@"",nil];
    [_mailPicker setCcRecipients:ccRecipients];
    //添加密送
    NSArray *bccRecipients = [NSArray arrayWithObjects:@"",nil];
    [_mailPicker setBccRecipients:bccRecipients];
    
    NSString *emailBody = @"<font color='red'>eMail</font> 正文";
    [_mailPicker setMessageBody:emailBody isHTML:YES];
    [self presentModalViewController: _mailPicker animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //关闭邮件发送窗口
    [controller dismissModalViewControllerAnimated:YES];
    NSString *msg;
    switch (result) {
        case MFMailComposeResultCancelled:
            msg = @"cancel the edit message";
            break;
        case MFMailComposeResultSaved:
            msg = @"successful saved the message";
            break;
        case MFMailComposeResultSent:
            msg = @"Place the message in the queue";
            break;
        case MFMailComposeResultFailed:
            msg = @"try to save or send the message failed";
            break;
        default:
            msg = @"";
            break;
    }
    [SVProgressHUD showSuccessWithStatus:msg];
}
@end
