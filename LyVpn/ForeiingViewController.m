//
//  ForeiingViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ForeiingViewController.h"
#import "ForeignNavTableViewCell.h"


static NSString * cellindetiy = @"cell";
@interface ForeiingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView * TableView;
@property (nonatomic,strong) NSArray * Name1Array;
@property (nonatomic,strong) NSArray * Name2Array;
@property (nonatomic,strong) NSArray * image1Array;
@property (nonatomic,strong) NSArray * image2Array;
@property (nonatomic,strong) NSArray * detail1Array;
@property (nonatomic,strong) NSArray * detail2Array;
@end

@implementation ForeiingViewController
{
    NSString * netsTRING;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createTableview];
}

- (void) createTableview
{
    self.TableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 44) style:UITableViewStylePlain];
    self.TableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.TableView.delegate =self;
    self.TableView.dataSource = self;
    [self.view addSubview:self.TableView];
    [self.TableView registerNib:[UINib nibWithNibName:@"ForeignNavTableViewCell" bundle:nil] forCellReuseIdentifier:@"ForeignNavTableViewCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark  --------------------  UITableViewDelegate  &  UITableViewDataSource ------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.Name1Array.count;
    }
    return self.Name2Array.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"大家正在看";
    }
    return @"精品推荐";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ForeignNavTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ForeignNavTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        [cell cellDataMessage:self.image1Array name:self.Name1Array detail:self.detail1Array index:indexPath.row];
    }
    else
    {
        [cell cellDataMessage:self.image2Array name:self.Name2Array detail:self.detail2Array index:indexPath.row];
    }
    cell.ClickBlock = ^(NSInteger index){
        
        if (indexPath.section == 0) {
            
            [self clickNet:index];
            
        }else
        {
            [self clicktuijian:index];
        }
        
    };
    return cell;
}
- (NSArray *)Name1Array
{
    if (_Name1Array == nil) {
        
        _Name1Array = @[@"Google",@"YouTube",@"烂番茄网"];
    }
    return _Name1Array;
}

- (NSArray *)Name2Array
{
    if (_Name2Array == nil) {
        
        _Name2Array = @[@"Facebook",@"推特",@"instagram",@"Gmail",@"dailymotio",@"WallStreet",@"twitch",@"niconico"];
    }
    return _Name2Array;
}

- (NSArray *)detail1Array
{
    if (_detail1Array == nil) {
        _detail1Array = @[@"谷歌公司（Google Inc.）成立于1998年9月4日，由拉里·佩奇和谢尔盖·布林共同创建，被公认为全球最大的搜索引擎。",@"YouTube是世界上最大的视频网站，早期公司总部位于加利福尼亚州的圣布鲁诺。在比萨店和日本餐馆，让用户下载、观看及分享影片或短片。公司于2005年2月15日注册，由华裔美籍华人陈士骏等人创立。",@"烂番茄（ROTTEN TOMATOES）是美国一个网站，以提供电影相关评论、资讯和新闻为主，其母公司为IGN Entertainment。网站由电影迷Senh Duong于1998年5月创建，已经成为电影消费者和影迷的首选目的地。基于来自芝加哥太阳时报、新纽约客、今日美国报等媒体的好评，并且以其很多独特的电影评论方式，烂番茄为其用户提供了全方位的服务，特色产品及交流社区。"];
    }
    return _detail1Array;
}

- (NSArray *)detail2Array
{
    if (_detail2Array == nil) {
        _detail2Array = @[@"Facebook是美国的一个社交网络服务网站,于2004年2月4日上线,于2012年3月6日发布Windows版的桌面聊天软件Facebook Messenger 。主要创始人为美国人马克·扎克伯格。Facebook是世界排名领先的照片分享站点，截至2013年11月每天上传约3.5亿张照片。截至2012年5月，Facebook拥有约9亿用户。",@"Twitter（非官方汉语通称推特）是一家美国社交网络及微博客服务的网站，是全球互联网上访问量最大的十个网站之一。",@"Instagram是一款最初运行在iOS平台上的移动应用，以一种快速、美妙和有趣的方式将你随时抓拍下的图片分享彼此，安卓版Instagram于2012年4月3日起登陆Android应用商店Google Play.",@"Gmail是Google的免费网络邮件服务。",@"Dailymotion是一家视频分享网站，用户可以在该网站上传、分享和观看视频，其总部位于法国巴黎的十八区。它的域名在YouTube之后一个月注册。",@"华尔街日报（The Wall Street Journal）创刊于1889年，以超过200万份的发行量成为美国付费发行量最大的财经报纸",@"Twitch是一个面向视频游戏的实时流媒体视频平台，由Justin Kan和Emmett Shear联合创立，它是Justin.tv旗下专注于游戏相关内容的独立运营站点。",@"NICONICO动画（日文：ニコニコ动画）是NIWANGO公司所提供的线上影片分享网站，常被简称为niconico或nico等。"];
    }
    return _detail2Array;
}

- (NSArray *)image1Array
{
    if (_image1Array == nil) {
        
        _image1Array = @[@"google",@"toutube",@"烂番茄网"];
    }
    return _image1Array;
}

- (NSArray *)image2Array
{
    if (_image2Array == nil) {
        _image2Array = @[@"facebook",@"推特",@"instagram",@"gmil",@"dailymotio",@"wsj",@"twitch",@"niconico"];
    }
    return _image2Array;
}


- (void) clickNet:(NSInteger)index
{
    if (index == 0) {
        netsTRING = @"http://www.google.cn";
    }else if (index == 1)
    {
        netsTRING = @"https://www.youtube.com";
    }else
    {
        netsTRING = @"https://www.rottentomatoes.com";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:netsTRING]];
}

- (void) clicktuijian:(NSInteger)index
{
    if (index == 0) {
        netsTRING = @"http://www.facebook.com";
    }
    else if (index == 1)
    {
        netsTRING = @"https://www.twitter.com";
    }
    else if (index == 2)
    {
        netsTRING = @"http://www.instagram.com";
    }else if (index == 3)
    {
        netsTRING = @"https://mail.google.com";
    }else if (index == 4)
    {
        netsTRING = @"http://dailymotion.trendolizer.com";
    }else if (index == 5)
    {
        netsTRING = @"http://www.wall-street.com";
        
    }else if (index == 6)
    {
        netsTRING = @"https://www.twitch.tv";
    }else if (index == 7)
    {
        netsTRING = @"http://www.nicovideo.jp";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:netsTRING]];
}
@end
