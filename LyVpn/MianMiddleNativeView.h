//
//  MianMiddleNativeView.h
//  极光VPN
//
//  Created by cacac on 2017/4/19.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MianMiddleNativeView : UIView
- (void) setup:(UIViewController *)controller;
- (void) releaseNativeView;
@end
