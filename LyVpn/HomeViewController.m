//
//  HomeViewController.m
//  VPN
//
//  Created by 王树超 on 18/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

#import "HomeViewController.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"
#import <Dezignables/Dezignables-Swift.h>
#import "AFNetworking.h"

@interface HomeViewController () {
    NSDictionary *userDict;
    NSDictionary *keychainUserDict;
}
@property IBOutlet DezignableButton *quickLoginButton;
@end

@implementation HomeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    userDict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    keychainUserDict = [KeyChainStore load:KEY_USER_LOGINDATA];
    if (userDict == nil && keychainUserDict == nil) {
        [self.quickLoginButton setEnabled:YES];
    } else {
        [self.quickLoginButton setEnabled:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)quickLogin:(id)sender {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
    userDict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    NSString *userName = userDict[@"UserName"];
    NSString *urlString = [NSString stringWithFormat:@"%@/usertest/ios.asmx/PayvisitorLoginReg",GONGYOUURL];
    NSDictionary *parameters;
    if (userName) {
        parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": userName, @"source": TYPESTRING};
    } else {
        parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": @"", @"source": TYPESTRING};
    }
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
        } else {
            NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            // Check Status
            NSString *status = objectArray[0][@"state"];
            if ([status isEqualToString:@"2"] || [status isEqualToString:@"-1"] || [status isEqualToString:@"0"] || [status isEqualToString:@"3"]) {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"reglog19", nil)];
            } else {
                // Successful, store local, enter the main interface
                // Save To Keychain and UserDefault
                [NSKeyedArchiver archiveRootObject:objectArray[0] toFile:USERALLDATA];
                [KeyChainStore save:KEY_USER_LOGINDATA data:objectArray[0]];
                // Show Main Storyboard
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *mainNav = [sb instantiateInitialViewController];
                if (self.navigationController.isBeingPresented) {
                    [self.navigationController dismissViewControllerAnimated:YES completion:^{
                        [SVProgressHUD dismiss];
                    }];
                } else if (self.navigationController.presentedViewController == nil) {
                    [self.navigationController presentViewController:mainNav animated:YES completion:^{
                        [SVProgressHUD dismiss];
                    }];
                }
            }
        }
    }];
    [dataTask resume];
}

@end
