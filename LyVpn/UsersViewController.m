//
//  UsersViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "UsersViewController.h"
#import "UserDetailViewController.h"
#import "DataTaoCanViewController.h"
#import "ChangeTimeString.h"
#import "BuyRecordViewController.h"
#import "LogViewController.h"
#import "BindingViewController.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"
#import "HomeViewController.h"

static NSString * cellidenty = @"cell";
@interface UsersViewController ()<UITableViewDelegate,UITableViewDataSource>
    {
        UIButton *getOutBtn;
        UIImageView *topImage;
        UIImageView *logoImage;
    }
    @property (nonatomic,strong) UITableView * tableView;
    @property (nonatomic,strong) NSArray * dataSource;
    @property (nonatomic,strong) NSMutableDictionary * Userdict;
    @property (nonatomic,strong) NSMutableArray *dataArray;
    @property (nonatomic,strong) NSMutableArray *ordersArray;
    @property (nonatomic,strong) CALayer *dotLayer;
    @property (nonatomic,assign) CGFloat endPointX;
    @property (nonatomic,assign) CGFloat endPointY;
    @property (nonatomic,strong) UIBezierPath *path;
    @property (nonatomic,assign) NSUInteger totalOrders;
    
    //用户名
    @property (nonatomic,strong) NSString * userUUID;
    
    //起始日期  结束日期
    @property (nonatomic,strong) NSString * startTime;
    @property (nonatomic,strong) NSString * endTime;
    @property (nonatomic,strong) NSString * restTime;
    @property (nonatomic,strong) NSString * TaoCanName;
    @end

@implementation UsersViewController
    
    
- (NSString *)panduan {
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound)
    {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}
    
- (NSString*)currentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    
    return currentLang;
}
    
- (void)viewWillAppear:(BOOL)animated {
    // Get User Offline Data
    _Userdict = [KeyChainStore load:KEY_USER_LOGINDATA];
    if (_Userdict == nil) {
        _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    }
    if (_Userdict) {
        [Global printLogWithObject:self.Userdict function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    }
    [self GetLogintime];
    
    ////退出登录
    NSString *string = self.Userdict[@"Email"];
    if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"]) {
        [getOutBtn setTitle:NSLocalizedString(@"reglog4", nil) forState:UIControlStateNormal];
    } else {
        [getOutBtn setTitle:NSLocalizedString(@"user16", nil) forState:UIControlStateNormal];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    /*
     *透明效果
     */
    /*
     [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
     self.navigationController.navigationBar.shadowImage = [UIImage new];
     [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
     */
    
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    
    self.view.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTaoCan:) name:@"refresh2" object:nil];
    
    topImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    topImage.image = [UIImage imageNamed:@"background"];
    [self.view addSubview:topImage];
    
    UIImageView *whiteImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH/2.2 , NaviHeight + 30*KWidth_Scale, SCREEN_WIDTH/1.1, SCREEN_HEIGHT - 140*KWidth_Scale)];
    whiteImage.image = [UIImage imageNamed:@"底板"];
    [self.view addSubview:whiteImage];
    
    
    if (IS_IPAD) {
        topImage.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2);
        whiteImage.frame = CGRectMake(self.view.center.x - SCREEN_WIDTH/2.2 , NaviHeight + 30*KWidth_Scale, SCREEN_WIDTH/1.1, SCREEN_HEIGHT - 100*KWidth_Scale);
    }
    
    logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH/8 ,NaviHeight + 80*KWidth_Scale, SCREEN_WIDTH/4, SCREEN_WIDTH/4)];
    logoImage.image = [UIImage imageNamed:@"icon"];
    [self.view addSubview:logoImage];
    [self createTableView];
    [self GetLogintime];
    [self creatNavi];
    
    NSDictionary *dic1 = @{@"id": @9323283,
                           @"name": @"红色",
                           @"min_price": @12.0,
                           @"praise_num": @20,
                           @"picture":@"1.png",
                           @"month_saled":@12};
    
    NSDictionary *dic2 = @{@"id": @9323284,
                           @"name": @"黄色",
                           @"min_price": @28.0,
                           @"praise_num": @6,
                           @"picture":@"2.png",
                           @"month_saled":@34};
    
    NSDictionary *dic3 = @{@"id": @9323285,
                           @"name": @"橙色",
                           @"min_price": @28.0,
                           @"praise_num": @8,
                           @"picture":@"3.png",
                           @"month_saled":@16};
    
    NSDictionary *dic4 = @{@"id": @26844943,
                           @"name": @"绿色",
                           @"min_price": @32.0,
                           @"praise_num": @1,
                           @"picture":@"4.png",
                           @"month_saled":@56};
    
    NSDictionary *dic5 = @{@"id": @9323279,
                           @"name": @"青色",
                           @"min_price": @29.0,
                           @"praise_num": @11,
                           @"picture":@"5.png",
                           @"month_saled":@11};
    
    NSDictionary *dic6 = @{@"id": @9323289,
                           @"name": @"蓝色",
                           @"min_price": @22.0,
                           @"praise_num": @2,
                           @"picture":@"6.png",
                           @"month_saled":@5};
    
    NSDictionary *dic7 = @{@"id": @9323243,
                           @"name": @"紫色",
                           @"min_price": @72.0,
                           @"praise_num": @0,
                           @"picture":@"7.png",
                           @"month_saled":@19};
    
    NSDictionary *dic8 = @{@"id": @9323220,
                           @"name": @"核红",
                           @"min_price": @64.0,
                           @"praise_num": @28,
                           @"picture":@"8.png",
                           @"month_saled":@7};
    
    NSDictionary *dic9 = @{@"id": @9323280,
                           @"name": @"桃红",
                           @"min_price": @30.0,
                           @"praise_num": @48,
                           @"picture":@"9.png",
                           @"month_saled":@0};
    
    NSDictionary *dic10 = @{@"id": @9323267,
                            @"name": @"屌丝绿",
                            @"min_price": @16.0,
                            @"praise_num": @9,
                            @"picture":@"10.png",
                            @"month_saled":@136};
    
    //这样书写的定义数据，用于后面的动态添加订单个数的key：orderCountTF。 实际项目中没有这么复杂
    // The definition data written in this way is used to add the order number of the key: orderCountTF. The actual project is not so complicated.
    _dataArray = [@[[dic1 mutableCopy],[dic2 mutableCopy],[dic3 mutableCopy],[dic4 mutableCopy],[dic5 mutableCopy],[dic6 mutableCopy],[dic7 mutableCopy],[dic8 mutableCopy],[dic9 mutableCopy],[dic10 mutableCopy]] mutableCopy];
}
-(void)creatNavi {
    UIButton *adBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [adBtn setImage:[UIImage imageNamed:@"返回1"] forState:UIControlStateNormal];
    //    if (IS_IPHONEX) {
    //        [adBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    //    }
    [adBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * iteml4=[[UIBarButtonItem alloc]initWithCustomView:adBtn];
    self.navigationItem.leftBarButtonItem = iteml4;
}
    
- (void)dismissViewController {
    [self.navigationController popViewControllerAnimated:YES];
}
    
- (void) refreshTaoCan:(NSNotification *)noti {
    //    NSLog(@"刷新账户界面的数据");
    [self GetLogintime];
}
    
- (void) GetLogintime {
    if ([[self panduan] isEqualToString:@"chinese"]) {
        self.TaoCanName = self.Userdict[@"PackageName"];
    } else {
        self.TaoCanName = self.Userdict[@"EnPackageName"];
    }
    self.startTime = [ChangeTimeString GetYearMonthDayString:self.Userdict[@"startTime"]];
    self.endTime = [ChangeTimeString GetYearMonthDayString:self.Userdict[@"StopTime"]];
    self.restTime = [ChangeTimeString dateTimeDifferenceWithStartTime:[ChangeTimeString getCurrentTime] endTime:[ChangeTimeString DataChangeTimeString:self.Userdict[@"StopTime"]]];
    [self.tableView reloadData];
}
    
- (NSArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = @[NSLocalizedString(@"user2", nil),NSLocalizedString(@"user15", nil),NSLocalizedString(@"user3", nil),NSLocalizedString(@"user4", nil),NSLocalizedString(@"user5", nil)];
    }
    return _dataSource;
}
    
    
- (void) createTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH/3, CGRectGetMaxY(logoImage.frame) + 30*KWidth_Scale, SCREEN_WIDTH/1.5, SCREEN_HEIGHT - SCREEN_WIDTH) style:UITableViewStylePlain];
    if (IS_IPAD) {
        _tableView.frame = CGRectMake(self.view.center.x - SCREEN_WIDTH/3, CGRectGetMaxY(logoImage.frame) + 30*KWidth_Scale, SCREEN_WIDTH/1.5, SCREEN_HEIGHT - SCREEN_HEIGHT/1.5);
    }
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UITableView alloc]init];
    [self.tableView setSeparatorColor: [UIColor colorWithRed:204.0/255.0 green:228.0/255.0 blue:249.0/255.0 alpha:1]];
    [self.view addSubview:self.tableView];
    
    ////退出登录
    getOutBtn = [[UIButton alloc]init];
    getOutBtn.frame = CGRectMake(SCREEN_WIDTH / 6, CGRectGetMaxY(_tableView.frame) + 10*KWidth_Scale, SCREEN_WIDTH / 3*2, 35*KWidth_Scale);
    getOutBtn.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    [getOutBtn setBackgroundImage:[UIImage imageNamed:@"登录2"] forState:UIControlStateNormal];
    NSString *string = self.Userdict[@"Email"];
    
    if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"]) {
        [getOutBtn setTitle:NSLocalizedString(@"reglog4", nil) forState:UIControlStateNormal];
    } else {
        [getOutBtn setTitle:NSLocalizedString(@"user16", nil) forState:UIControlStateNormal];
    }
    [getOutBtn addTarget:self action:@selector(getOutBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getOutBtn];
}
    
-(void)getOutBtnAction {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *navVC = [sb instantiateInitialViewController];
    HomeViewController *homeVC = navVC.viewControllers[0];
    NSString *string = self.Userdict[@"Email"];
    
    if ([string isKindOfClass:[NSNull class]] || string.length < 1 || [string isEqualToString:@"<null>"]) {
        // 判断是否注册过
        // Determine whether you have registered
        NSDictionary* dic = [NSKeyedUnarchiver unarchiveObjectWithFile:LOGINDATA];
        NSString *email = dic[@"Email"];
        if(!email) {
            homeVC.isShowQuickLoginButton = YES;
        } else {
            homeVC.isShowQuickLoginButton = NO;
        }
    } else {
        homeVC.isShowQuickLoginButton = NO;
        
        // 把邮箱存到别处
        // Save the mailbox elsewhere
        NSDictionary *locDic = [NSDictionary dictionaryWithObject:self.Userdict[@"Email"] forKey:@"Email"];
        
        [NSKeyedArchiver archiveRootObject:locDic toFile:LOGINDATA];
        
        // 把本地的邮箱密码删除
        // Delete the local email password
        NSDictionary *dic = self.Userdict;
        
        NSMutableDictionary * muDic = [NSMutableDictionary dictionaryWithDictionary:dic];
        [muDic removeObjectForKey:@"Email"];
        [muDic removeObjectForKey:@"Password"];
        
        NSDictionary *dic1 = [NSDictionary dictionaryWithDictionary:muDic];
        
        // 成功，存储本地，进入主界面
        // Successful, store local, enter the main interface
        [NSKeyedArchiver archiveRootObject:dic1 toFile:USERALLDATA];
    }
    if (self.navigationController.isBeingPresented) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else if (self.navigationController.presentedViewController == nil) {
        [self.navigationController presentViewController:navVC animated:YES completion:nil];
    }
}
    
    
#pragma mark  ------------------- UITableViewDelegate &  UITableViewDataSource  ------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
    
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD) {
        return 80;
    } else {
        return 50;
    }
    
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellidenty];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1];
    cell.textLabel.font = [UIFont systemFontOfSize:13*KWidth_Scale];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:11*KWidth_Scale];
    
    
    if (indexPath.row == 0)
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.detailTextLabel.text = self.Userdict[@"UserName"];
    }else if (indexPath.row == 1)
    {
        NSString *string = [NSString stringWithFormat:@"%@",self.Userdict[@"Email"]];
        
        if (string)
        {
            if ([string isEqualToString:@"<null>"] || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
            {
                //                //绑定
                //                UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 120, 0, 100, 50)];
                //                if (IS_IPAD) {
                //                    button.frame = CGRectMake(SCREEN_WIDTH - 120, 0, 100, 80);
                //                }
                //                button.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
                //
                //                [button setTitle:NSLocalizedString(@"userMess3", nil) forState:UIControlStateNormal];
                //                button.backgroundColor = [UIColor orangeColor];
                //                [button setTitleColor:[UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1] forState:UIControlStateNormal];
                //
                //                [button addTarget:self action:@selector(bingdingBtn) forControlEvents:UIControlEventTouchUpInside];
                //
                //                [cell.contentView addSubview:button];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                cell.detailTextLabel.text = NSLocalizedString(@"userMess3", nil);
                
                
            } else {
                //显示
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",self.Userdict[@"Email"]];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
    } else if (indexPath.row == 2) {
        NSString *string = [NSString stringWithFormat:@"%@",self.Userdict[@"ChannelState"]];
        if ([string isEqualToString:@"0"]) {
            //游客
            cell.detailTextLabel.text = NSLocalizedString(@"usersmall2", nil);
        } else if ([string isEqualToString:@"1"]) {
            //免费
            cell.detailTextLabel.text = NSLocalizedString(@"usersmall3", nil);
        } else if ([string isEqualToString:@"3"]) {
            //vip
            cell.detailTextLabel.textColor = [UIColor colorWithRed:255.0/255.0 green:62.0/255.0 blue:58.0/255.0 alpha:1];
            cell.detailTextLabel.text = NSLocalizedString(@"usersmall4", nil);
        }
    } else if (indexPath.row == 3) {
        cell.detailTextLabel.text = self.TaoCanName;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if (indexPath.row == 4) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.dataSource[indexPath.row];
    return cell;
}
    
    
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UserDetailViewController * detailVC = [[UserDetailViewController alloc] init];
        detailVC.hidesBottomBarWhenPushed = YES;
        detailVC.UUID = self.Userdict[@"UserName"];
        [self.navigationController pushViewController:detailVC animated:NO];
    } else if (indexPath.row == 1)
    {
        //        NSString *string = [NSString stringWithFormat:@"%@",self.Userdict[@"Email"]];
        //
        //        if (string) {
        //            if ([string isEqualToString:@"<null>"] || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        //            {
        
        BindingViewController *bindVC = [[BindingViewController alloc]init];
        bindVC.userName = self.Userdict[@"UserName"];
        [self.navigationController pushViewController:bindVC animated:YES];
        
        //            }
        //
        //        }
    }  else if (indexPath.row == 3) {
        /*
         DataTaoCanViewController * dataVC = [[DataTaoCanViewController alloc] init];
         dataVC.hidesBottomBarWhenPushed = YES;
         dataVC.startTime = self.startTime;
         dataVC.endTime = self.endTime;
         dataVC.restTime = self.restTime;
         dataVC.TaoCanname = self.TaoCanName;
         [self.navigationController pushViewController:dataVC animated:NO];
         */
        [self performSegueWithIdentifier:@"showDueTimeSegue" sender:self];
        
    } else if (indexPath.row == 4) {
        BuyRecordViewController * buyvc = [[BuyRecordViewController alloc] init];
        buyvc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:buyvc animated:NO];
    }
}
    
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual: @"showDueTimeSegue"]) {
        DataTaoCanViewController *dataVC = [segue destinationViewController];
        dataVC.startTime = self.startTime;
        dataVC.endTime = self.endTime;
        dataVC.restTime = self.restTime;
        dataVC.TaoCanname = self.TaoCanName;
    }
}
    //-(void)bingdingBtn
    //{
    //    BindingViewController *bindVC = [[BindingViewController alloc]init];
    //
    //    bindVC.userName = self.Userdict[@"UserName"];
    //
    //
    //    [self.navigationController pushViewController:bindVC animated:YES];
    //
    //}
    
    @end
