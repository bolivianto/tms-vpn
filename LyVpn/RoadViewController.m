//
//  RoadViewController.m
//   VPN
//
//  Created by cacac on 2017/2/10.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "RoadViewController.h"
#import "AFNetworking.h"
#import "RoadTableViewCell.h"
#import "BoolModel.h"
#import "LSProgressHUD.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "CLAlertView.h"
#import "LogViewController.h"
#import "BuysViewController.h"
#import "VPN-Swift.h"
#import <Dezignables/Dezignables-Swift.h>

#define ServerLocationsIdentifier @"serverLocations"

@interface RoadViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
    @property (weak, nonatomic) IBOutlet DezignableView *serverContainerView;
@property (nonatomic,strong) IBOutlet UITableView * tableView;
@property (nonatomic,strong) RoadTableViewCell * roadCell;
@property (nonatomic,strong) NSMutableArray * dataSource;
@property (nonatomic,strong) NSMutableArray * roadBoolChoose;
@property (nonatomic, strong) NSMutableArray *childConstraints;
@property (nonatomic, strong) NSMutableArray *constraints;
@property (nonatomic, assign) CGFloat layoutMultiplier;
@property (nonatomic, assign) CGFloat layoutConstant;
@property (nonatomic, assign) BOOL hasLayoutRelation;
@property (nonatomic, strong) id mas_key;
@property (nonatomic, assign) BOOL useAnimator;
@property (nonatomic,strong) NSMutableArray * adArray;
@property (nonatomic,strong) NSMutableDictionary * Userdict;
@property (nonatomic,strong) NSString * country;
@property (nonatomic,strong) NSMutableArray * CountryServerMessage;
@end

@implementation RoadViewController
- (NSString *) panduan
{
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound) {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}

-(NSString*)currentLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    NSLog(@"%@",currentLang);
    return currentLang;
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"tablename1", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self GetAdMessageData];
    [self createNavi];
    [self createTableView];
    [self createRefresh];
    
    // Init Reachability
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case (NotReachable):
                [Global printLogWithObject:[NSString stringWithFormat:@"Reachability: %@",  AFStringFromNetworkReachabilityStatus(status)] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                [self loadOfflineServers];
                break;
            default:
                [self refreshServerList];
                break;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}
- (void) GetAdMessageData {
    NSString * str1 = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetLQYjsonBYType?apikey=lygames_0953&type=%@",GONGYOUURL,TYPESTRING];
    NSURL *url = [NSURL URLWithString:str1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *userdata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (userdata) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:userdata options:NSJSONReadingMutableContainers error:nil];
        if (array) {
            [self.adArray addObjectsFromArray:array];
        }
    }
}

- (void) createRefresh
{
    UIButton * buttn = [UIButton buttonWithType:UIButtonTypeCustom];
    buttn.frame = CGRectMake(0, 0, 30, 30);
    [buttn setImage:[UIImage imageNamed:@"刷新(1)"] forState:UIControlStateNormal];
    //    if (IS_IPHONEX) {
    //        [buttn setImage:[UIImage imageNamed:@"wp_refreshX"] forState:UIControlStateNormal];
    //    }
    [buttn addTarget:self action:@selector(refreshServerList) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithCustomView:buttn];
    self.navigationItem.rightBarButtonItem = search;
    
}
- (void) createNavi
{
    // 创建导航栏左按钮   用于编辑
    // Create a navigation bar left button for editing
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"返回1"] forState:UIControlStateNormal];
    //    if (IS_IPHONEX) {
    //        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    //    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    /*
     * Transparent Navbar
     */
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    
}

- (void)backMainVC:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)getServerList {
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    weakself(self);
    if ([[self panduan]isEqualToString:@"chinese"]) {
        _country = @"1";
    } else {
        _country = @"0";
    }
    
    /* Request Server List */
    [LSProgressHUD showWithMessage:NSLocalizedString(@"other5", nil)];
    NSString * url = [NSString stringWithFormat:@"%@/lygamesService.asmx/LQYAddlistByTypes?apikey=lygames_0953&type=1&type1=%@&Country=%@&name=%@&uuid=%@",GONGYOUURL,TYPESTRING,_country,self.Userdict[@"UserName"],[UUID getUUID]];
    AFHTTPRequestOperationManager * manager1 = [AFHTTPRequestOperationManager manager];
    manager1.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager1 GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [LSProgressHUD hide];
        NSArray<NSDictionary*> *array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // Save Offline
        [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"serverLocations"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [weakSelf.dataSource addObjectsFromArray:array];
        [weakSelf initDataSource];
        [weakSelf.tableView reloadData];
        if (array) {
            NSString *state = array[0][@"state"];
            if ([state isEqualToString:@"0"]) {
                //不管是否登录，改掉密码，不可连接
                // Change the password regardless of whether you log in or not.
                [[NSNotificationCenter defaultCenter]postNotificationName:@"Repeat" object:nil];
                
                //在其他设备登录
                // Log in on other devices
                CLAlertView *alertView = [[CLAlertView alloc] initWithAlertViewWithTitle:NSLocalizedString(@"reglog6", nil) text:NSLocalizedString(@"custom10", nil) DefauleBtn:NSLocalizedString(@"userMess8", nil) cancelBtn:NSLocalizedString(@"reglog32", nil) defaultBtnBlock:^(UIButton *defaultBtn) {
                    LogViewController *logVC = [[LogViewController alloc]init];
                    [self.navigationController pushViewController:logVC animated:YES];
                } cancelBtnBlock:^(UIButton *cancelBtn) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertView show];
            }
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
        [self loadOfflineServers];
        [LSProgressHUD hide];
    }];
}

- (void) loadOfflineServers {
    NSArray<NSDictionary*> *offlineLocations = [[NSUserDefaults standardUserDefaults] objectForKey:ServerLocationsIdentifier];
    [self.dataSource addObjectsFromArray:offlineLocations];
    [self initDataSource];
    [self.tableView reloadData];
}

- (void) initDataSource {
    for (int j = 0; j < self.dataSource.count; j++) {
        BoolModel * model = [[BoolModel alloc] init];
        NSString * indexStr = [[NSUserDefaults standardUserDefaults] objectForKey:ROAD_STORE_KEY];
        
        NSInteger index = [indexStr intValue];
        if (!indexStr) {
            if (j == 0) {
                model.selectStatus = YES;
            }
            else
            {
                model.selectStatus = NO;
            }
        }
        
        else
        {
            if (j == index) {
                model.selectStatus = YES;
            }
            else {
                model.selectStatus = NO;
            }
        }
        [self.roadBoolChoose addObject:model];
    }
}
- (NSMutableArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (NSMutableArray *)roadBoolChoose {
    if (_roadBoolChoose == nil) {
        _roadBoolChoose = [NSMutableArray arrayWithCapacity:0];
    }
    return _roadBoolChoose;
}

- (NSMutableArray *)adArray {
    if (_adArray == nil) {
        _adArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _adArray;
}
- (NSMutableArray *)CountryServerMessage {
    if (_CountryServerMessage == nil) {
        _CountryServerMessage = [NSMutableArray arrayWithCapacity:0];
    }
    return _CountryServerMessage;
}

- (void)refreshServerList {
    [self.dataSource removeAllObjects];
    [self getServerList];
}

- (void) createTableView {
    [self.tableView registerNib:[UINib nibWithNibName:@"RoadTableViewCell" bundle:nil] forCellReuseIdentifier:@"RoadTableViewCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
}

#pragma mark  ---------  UITableViewDelegate  and   UITableViewDataSource  -------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    _roadCell = [tableView dequeueReusableCellWithIdentifier:@"RoadTableViewCell" forIndexPath:indexPath];
    _roadCell.backgroundColor = [UIColor clearColor];
    
    if ([[self panduan] isEqualToString:@"chinese"]) {
        _roadCell.roadLabel.text = self.dataSource[indexPath.row][@"CnArea"];
    } else {
        _roadCell.roadLabel.text = self.dataSource[indexPath.row][@"EnArea"];
    }
    _roadCell.selectionStyle = UITableViewCellSelectionStyleNone;
    BoolModel * model = _roadBoolChoose[indexPath.row];
    BOOL selectStatus = model.selectStatus;
    if (selectStatus == YES) {
        //此处需要进行修改
        _roadCell.chooseImageView.image = [UIImage imageNamed:@"选中"];
    } else {
        //此处需要进行修改
        _roadCell.chooseImageView.image = [UIImage imageNamed:@"未选中"];
    }
    /*
    if (IS_IPAD) {
        if (@available(iOS 11.0, *)) {
            _roadCell.roadLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleLargeTitle];
        } else {
            // Fallback on earlier versions
            _roadCell.roadLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle1];
        }
        //_roadCell.roadLabel.font = [UIFont fontWithName:@"Lobster 1.4" size:24*KWidth_Scale];
    } else {
        //_roadCell.roadLabel.font = [UIFont fontWithName:@"Lobster 1.4" size:14];
        _roadCell.roadLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    }
    */
    _roadCell.roadLabel.textColor = [UIColor darkGrayColor];
    [_roadCell.roadImage sd_setImageWithURL:[NSURL URLWithString:[self.dataSource[indexPath.row][@"ImgUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    return _roadCell;
}

- (void)setSizeOffdset:(CGSize)sizeOffset {
    NSLayoutAttribute layoutAttribute;
    switch (layoutAttribute) {
        case NSLayoutAttributeWidth:
            self.layoutConstant = sizeOffset.width;
            break;
        case NSLayoutAttributeHeight:
            self.layoutConstant = sizeOffset.height;
            break;
        default:
            break;
    }
}

- (void)setCenterOdffset:(CGPoint)centerOffset {
    NSLayoutAttribute layoutAttribute ;
    switch (layoutAttribute) {
        case NSLayoutAttributeCenterX:
            self.layoutConstant = centerOffset.x;
            break;
        case NSLayoutAttributeCenterY:
            self.layoutConstant = centerOffset.y;
            break;
        default:
            break;
    }
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60*KWidth_Scale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *state = self.Userdict[@"ChannelState"];
    
    //1为普通用户   3为会员用户
    if (indexPath.row >=3) {
        if ([state integerValue] == 1 || [state integerValue] == 0) {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"other1", nil) message:NSLocalizedString(@"road1", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"other2", nil) otherButtonTitles:nil];
            alertView.delegate = self;
            alertView.tag = 321;
            [alertView show];
            
        }else if ([state integerValue] == 3)
        {
            [self canExchangeRoad:indexPath.row];
        }
    }
    else
    {
        [self canExchangeRoad:indexPath.row];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 321 && buttonIndex == 0) {
        BuysViewController *buysVC = [[BuysViewController alloc]init];
        [self.navigationController pushViewController:buysVC animated:YES];
    }
    
}
- (void) canExchangeRoad:(NSInteger)index
{
    //将国家名存到本地，方便首页通过国家名获取后台线路和密码
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString *name = self.dataSource[index][@"Area"];
    [defaults setObject:name forKey:@"areaname"];
    
    NSString * roadString = [NSString stringWithFormat:@"%@/lygamesService.asmx/GetAddLQYIP1?apikey=lygames_0953&Area=%@&state=3&type=%@",GONGYOUURL,self.dataSource[index][@"Area"],TYPESTRING];
    NSString *keyword = [roadString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    NSLog(@"点击方法");
    //改变数据源
    for (int j = 0; j < _roadBoolChoose.count; j++) {
        BoolModel *model = _roadBoolChoose[j];
        if (j==index) {
            model.selectStatus = YES;
        } else {
            model.selectStatus = NO;
        }
    }
    NSString * indexStr = [NSString stringWithFormat:@"%ld",index];
    [[NSUserDefaults standardUserDefaults] setObject:indexStr forKey:ROAD_STORE_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:self.dataSource[index][@"ImgUrl"] forKey:@"countryImage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([[self panduan] isEqualToString:@"chinese"]) {
        [[NSUserDefaults standardUserDefaults] setObject:self.dataSource[index][@"Area"] forKey:@"CountryName"];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:self.dataSource[index][@"AreaEn"] forKey:@"CountryName"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    AFHTTPRequestOperationManager * manager2 = [AFHTTPRequestOperationManager manager];
    manager2.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager2 GET:keyword parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        [hud hideAnimated:YES];
        
        //请求的数据再解密
        // Requested data decryption
        NSString *str = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        if ([str rangeOfString:@"数据源"].location !=NSNotFound) {
            
        }else
        {
            NSData *data = [[desFile decryptWithText:str] dataUsingEncoding:NSUTF8StringEncoding];
            NSArray * array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            [[NSUserDefaults standardUserDefaults] setObject:array[0] forKey:@"changeLine"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLine" object:nil];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"wocalei" forKey:@"wocalei"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StatisticsConnect"];
            NSString * Path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *filename = [Path stringByAppendingPathComponent:@"roadtest"];
            [NSKeyedArchiver archiveRootObject:array toFile:filename];
            
            [self.CountryServerMessage addObjectsFromArray:array];
            [self.tableView reloadData];
            if (self->_ChooseCountryNameBlock) {
                
                if ([[self panduan] isEqualToString:@"chinese"]) {
                    self->_ChooseCountryNameBlock(self.dataSource[index][@"Area"],self.CountryServerMessage);
                }
                else
                {
                    self->_ChooseCountryNameBlock(self.dataSource[index][@"AreaEn"],self.CountryServerMessage);
                }
            }
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        //        NSLog(@"加载失败");
    }];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
