//
//  Global.swift
//  VPN
//
//  Created by 王树超 on 02/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

import UIKit

let isDevelopment = true

class Global: NSObject {
    static func printLog(object: Any, function: String) {
        if isDevelopment {
            print(String(format: "TMS Log: \nValue: %@\nFunction: %@", arguments: [object as! CVarArg, function]))
        }
    }
    
    static func getMD5String(string: String) -> String {
        return string.md5
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension String {
    var md5: String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)

        CC_MD5(str!, strLen, result)

        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }

        result.deallocate(capacity: digestLen)
        return String(format: hash as String)
    }
}
