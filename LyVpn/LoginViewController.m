//
//  LoginViewController.m
//  VPN
//
//  Created by 王树超 on 23/07/18.
//  Copyright © 2018 王鹏. All rights reserved.
//

#import "LoginViewController.h"
#import "KeyChainStore.h"
#import "VPN-Swift.h"
#import "AFNetworking.h"
#import "ForgetViewController.h"
#import "ACFloatingTextField.h"

@interface LoginViewController () {
    NSMutableDictionary *userDict;
    BOOL isShowPassword;
}

@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtPassword;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordButton;

@end

@implementation LoginViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Load User Data from Local.
    userDict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    if (userDict == nil) {
        userDict = [KeyChainStore load:KEY_USER_LOGINDATA];
    }
    
    // Load Saved Email
    if (![userDict[@"Email"] isKindOfClass:[NSNull class]]) {
        self.txtEmail.text = userDict[@"Email"];
    }
    
    // Init Show Password
    isShowPassword = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Hide/Show password
- (IBAction)showPassword {
    if (isShowPassword) {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_hide_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = YES;
        isShowPassword = NO;
    } else {
        [_showPasswordButton setImage:[UIImage imageNamed:@"ic_show_password"] forState:UIControlStateNormal];
        _txtPassword.secureTextEntry = NO;
        isShowPassword = YES;
    }
}

- (BOOL)checkValidation { // Check Validation For Reset Password
    if ([self.txtEmail.text isEqualToString:@""]) { // Empty Email
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    if (![Global isValidEmailWithTestStr:self.txtEmail.text]) { // Wrong Email Format
        [self.txtEmail showErrorWithText:NSLocalizedString(@"reglog20", nil)];
        return NO;
    }
    if ([self.txtPassword.text isEqualToString:@""]) { // Empty Password
        [self.txtPassword showErrorWithText:NSLocalizedString(@"reglog7", nil)];
        return NO;
    }
    return YES;
}

// Login Function
- (IBAction)login {
    if ([self checkValidation]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"key12", nil)];
        NSString *md5Pass = [Global getMD5StringWithString:_txtPassword.text];
        NSDictionary *parameters = @{@"apikey": @"lygames_0953", @"uuid": [UUID getUUID], @"name": _txtEmail.text, @"pass": md5Pass, @"source": TYPESTRING};
        NSString *urlString = [NSString stringWithFormat:@"%@/lygamesService.asmx/TollUserLogin",GONGYOUURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                [Global printLogWithObject:error.localizedDescription function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
            } else {
                NSArray *objectArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [Global printLogWithObject:objectArray[0] function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
                // Check Status
                NSString *status = objectArray[0][@"state"];
                if ([status isEqualToString:@"2"] || [status isEqualToString:@"-1"]) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"reglog19", nil)];
                } else {
                    // Successful, store local, enter the main interface
                    // Save To Keychain and UserDefault
                    [NSKeyedArchiver archiveRootObject:objectArray[0] toFile:USERALLDATA];
                    [KeyChainStore save:KEY_USER_LOGINDATA data:objectArray[0]];
                    // Show Main Storyboard
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *mainNav = [sb instantiateInitialViewController];
                    if (self.navigationController.isBeingPresented) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    } else if (self.navigationController.presentedViewController == nil) {
                        [self.navigationController presentViewController:mainNav animated:YES completion:^{
                            [SVProgressHUD dismiss];
                        }];
                    }
                }
            }
        }];
        [dataTask resume];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"showForgotPasswordSegue"]) {
        ForgetViewController *vc = segue.destinationViewController;
        vc.email = self.txtEmail.text;
    }
}

@end
