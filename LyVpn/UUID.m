//
//  UUID.m
//  DouBan
//
//  Created by 乐游先锋 on 16/5/24.
//  Copyright © 2016年 cnaa. All rights reserved.
//

#import "UUID.h"
#import "VPN-Swift.h"
#import "KeyChainStore.h"


CFUUIDRef uuidRef;

@implementation UUID

+ (NSString *)getUUID {
    NSString * strUUID = (NSString *)[KeyChainStore load:KEY_USERNAME_PASSWORD];
    if ([strUUID isEqualToString:@""] || !strUUID) {
        uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        [KeyChainStore save:KEY_USERNAME_PASSWORD data:strUUID];
    }
    [Global printLogWithObject:strUUID function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    return strUUID;
}

- (void)dealloc
{
    uuidRef = nil;
}
@end
