//
//  HelpViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "HelpViewController.h"
#import "DetailViewController.h"
#import "AboutMeViewController.h"
static NSString * cellidenty = @"cell";

@interface HelpViewController ()<UITableViewDelegate,UITableViewDataSource,UMSocialUIDelegate>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * DataSource;
@property (nonatomic,strong) NSArray * ImageArray;
@property (nonatomic,strong) NSArray * titleArray;
@property (nonatomic,strong) NSArray * detailtxt;
@property (nonatomic,strong) NSMutableDictionary * ADdata;

@end

@implementation HelpViewController


- (NSArray *)detailtxt
{
    if (_detailtxt == nil) {
        
        _detailtxt = @[@"1.请确认当前软件是否在维护状态。\n2.可关闭软件，重新打开。\n3.可尝试关闭软件后，切换手机网络WIFI或者4G再打开连接。",@"如何选择服务器线路? \n1.如果访问的网站，对于地区有要求，那么可以选择同地区的服务线路；\n2.如果以视频为主，建议使用美国、日本和新加坡线路；\n3.如果以网页、邮件为主，绝大部分线路均可满足；\n4.如果需要访问CCTV、优酷、土豆等国内视频网站，请选择国内线路；\n5.可根据VPN速度测试，ms越小连接的成功率越高；\n6.服务器列表空白，可关闭VPN重新打开，也可能是网络不稳定，请尝试切换网络在打开。",@"如果连接不上，可能的情况有：\n1.请检查当前手机网络是否畅通，VPN速度测试是否可以连通，如显示NA或0ms，当前网络无法连接到服务器，可尝试切换WIFI和4G信号在连接；\n2.不同网络运营商，所适合的服务器线路稳定性和速度有差异，可以尝试选择不同区域的服务线路；\n3.偶然性的连接不上，可以再次尝试连接。",@"1、本产品只用于工作和学习，请勿使用本产品浏览任何非法内容。\n2、本产品在使用的过程中，如果出现连接不上的问题，可尝试更换线路后重新连接。\n3、本产品在使用的过程中遇到任何问题，请加入官方QQ群咨询。"];
    }
    return _detailtxt;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    _ADdata = [NSKeyedUnarchiver unarchiveObjectWithFile:AllADData];

    self.title = @"帮助";
    [self createNavi];
    [self.view addSubview:self.tableView];
}


- (void) createNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

- (void) backMainVC:(UIButton *)sender
{
//    self.navigationController.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  ------------------- UITableViewDelegate &  UITableViewDataSource  ------

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellidenty];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.section == 0) {
        
        cell.imageView.image = [UIImage imageNamed:@"wp_question2"];

        if (indexPath.row == 0)
        {
            cell.textLabel.text = self.ADdata[@"LandingFailureCN"];
            
        }else if (indexPath.row == 1)
        {
            cell.textLabel.text = self.ADdata[@"LineSelectionCN"];
            
        }else if (indexPath.row == 2)
        {
            cell.textLabel.text = self.ADdata[@"CannotConnectCN"];
        }
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"wp_question"];

        cell.textLabel.text = self.titleArray[indexPath.row];
        if (indexPath.row == 0)
        {
            cell.textLabel.text = self.ADdata[@"shareCN"];
            
        }else if (indexPath.row == 1)
        {
            cell.textLabel.text = self.ADdata[@"urlStringCN"];
            
        }else if (indexPath.row == 2)
        {
            cell.textLabel.text = self.ADdata[@"statement"];
        }

    }
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        
        return self.DataSource.count;
    }
    return self.ImageArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView * view = [UIView new];
        view.backgroundColor = [UIColor clearColor];
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        label.text = @"常见问题";
        label.textColor = [UIColor darkGrayColor];
        label.font = [UIFont systemFontOfSize:18];
        [view addSubview:label];
        return view;
    }
    else
    {
        UIView * view = [UIView new];
        view.backgroundColor = [UIColor clearColor];
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        label.text = @"其他";
        label.textColor = [UIColor darkGrayColor];
        label.font = [UIFont systemFontOfSize:18];
        [view addSubview:label];
        return view;
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * DetailString;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                DetailString  = self.detailtxt[0];
                if ([self.ADdata[@"LandingFailureSwitch"] integerValue] == 1)
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"LandingFailureLink"]]];
                }else
                {
                    DetailViewController * detailvc = [[DetailViewController alloc] init];
                    detailvc.labeltext = DetailString;
                    [self.navigationController pushViewController:detailvc animated:YES];
                }
                break;
                
            case 1:
                DetailString  = self.detailtxt[1];
                if ([self.ADdata[@"LineSelectionSwitch"] integerValue] == 1)
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"LineSelectionLink"]]];
                }else
                {
                    DetailViewController * detailvc = [[DetailViewController alloc] init];
                    detailvc.labeltext = DetailString;
                    [self.navigationController pushViewController:detailvc animated:YES];
                }
                break;
            case 2:
                DetailString  = self.detailtxt[2];
                if ([self.ADdata[@"CannotConnectSwitch"] integerValue] == 1)
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"CannotConnectLink"]]];
                }else
                {
                    DetailViewController * detailvc = [[DetailViewController alloc] init];
                    detailvc.labeltext = DetailString;
                    [self.navigationController pushViewController:detailvc animated:YES];
                }
                break;
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
//                NSLog(@"分享");
                [UMSocialData defaultData].extConfig.wechatSessionData.url = self.ADdata[@"shareUrl"];
                [UMSocialData defaultData].extConfig.wechatTimelineData.url = self.ADdata[@"shareUrl"];
                [UMSocialData defaultData].extConfig.wechatFavoriteData.url = self.ADdata[@"shareUrl"];
                [UMSocialSnsService presentSnsIconSheetView:self
                                                     appKey:@"57e38203e0f55a139f003bd4"
                                                  shareText:@"يأخذك لرؤية العالم، انقر لتحميل والذهاب إلى العالم معي تجول!"
                                                 shareImage:[UIImage imageNamed:@"icon"]
                                            shareToSnsNames:@[UMShareToWechatTimeline,UMShareToWechatSession,UMShareToWechatFavorite,UMShareToRenren]
                                                   delegate:self];
                break;
            case 1:
            {
                NSString * urlStr = self.ADdata[@"urlString"];
                NSURL *url = [NSURL URLWithString:urlStr];
                [[UIApplication sharedApplication] openURL:url];
                
                break;
            }
            case 2:
            {
                DetailString  = self.detailtxt[3];
               
                if ([self.ADdata[@"statementSwitch"] integerValue] == 1)
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ADdata[@"statementLink"]]];
                }else
                {
                    DetailViewController * detailvc = [[DetailViewController alloc] init];
                    detailvc.labeltext = DetailString;
                    [self.navigationController pushViewController:detailvc animated:YES];
                }
                break;
            }
        }
    }
}
- (NSArray *)DataSource
{
    if (_DataSource == nil) {
        _DataSource = @[@"为什么“登录失败”或者账户“禁用”",@"如何选择服务器线路",@"为什么“连不上”"];
    }
    return _DataSource;
}

- (NSArray *)ImageArray
{
    if (_ImageArray == nil) {
        
        _ImageArray = @[@"wp_share",@"wp_email",@"product_name"];
    }
    return _ImageArray;
}

- (NSArray *) titleArray
{
    if (_titleArray == nil) {
        
        _titleArray = @[@"分享",@"官方QQ群",@"产品声明"];
    }
    return _titleArray;
}

- (BOOL)shouldAutorotate {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
