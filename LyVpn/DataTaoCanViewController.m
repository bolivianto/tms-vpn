//
//  DataTaoCanViewController.m
//   VPN
//
//  Created by cacac on 2017/2/13.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "DataTaoCanViewController.h"
#import "ChangeTimeString.h"
#import "DueTimeTableViewCell.h"
#import "KeyChainStore.h"
#import "VPN-Swift.h"


static NSString * cellidenty = @"cell";
@interface DataTaoCanViewController ()<UITableViewDelegate,UITableViewDataSource>
    
    @property (nonatomic,strong) IBOutlet UITableView * tableView;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewContainerHeightConstraint;
    @property (nonatomic,strong) NSMutableDictionary * Userdict;
    @property (nonatomic,strong) NSArray * dataArray;
    
    @end

@implementation DataTaoCanViewController
    
- (NSString *) panduan {
    NSString *lang;
    if([[self currentLanguage] compare:@"zh-Hans" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] compare:@"zh-Hant" options:NSCaseInsensitiveSearch]==NSOrderedSame || [[self currentLanguage] rangeOfString:@"zh-Hans"].location == !NSNotFound || [[self currentLanguage] rangeOfString:@"zh-Hant"].location == !NSNotFound) {
        lang = @"zh";
        return @"chinese";
    } else {
        lang = @"en";
        return @"English";
    }
}
    
- (NSString*)currentLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLang = [languages objectAtIndex:0];
    // NSLog(@"%@",currentLang);
    return currentLang;
}
    
+ (NSString *)getCurrentTime {
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}
    
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"tablename2", nil);
    
    if (IS_IPAD) {
        self.tableViewContainerHeightConstraint.constant = 4*70;
    } else {
        self.tableViewContainerHeightConstraint.constant = 4*50;
    }
    
    [self createNavi];
    
    _Userdict = [NSKeyedUnarchiver unarchiveObjectWithFile:USERALLDATA];
    if (_Userdict == nil) {
        _Userdict = [KeyChainStore load:KEY_USER_LOGINDATA];
    }
    NSString *availableTime = [NSString stringWithFormat:@"Start : %@, Stop : %@", [ChangeTimeString GetYearMonthDayString: self.Userdict[@"startTime"]], [ChangeTimeString GetYearMonthDayString: self.Userdict[@"StopTime"]]];
    NSString *startTime = [ChangeTimeString DataChangeTimeString:self.Userdict[@"startTime"]];
    NSString *stopTime = [ChangeTimeString DataChangeTimeString:self.Userdict[@"StopTime"]];
    NSLog(@"Start: %@", startTime);
    NSLog(@"Stop: %@", stopTime);
    
    [Global printLogWithObject:availableTime function:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView reloadData];
}
    
- (void) createNavi {
    // 创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"返回1"] forState:UIControlStateNormal];
    //    if (IS_IPHONEX) {
    //        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    //    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
}
    
- (void) backMainVC:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
    
- (NSArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = @[NSLocalizedString(@"user4", nil),NSLocalizedString(@"user8", nil),NSLocalizedString(@"user9", nil),NSLocalizedString(@"user10", nil)];
    }
    return _dataArray;
}
    
#pragma mark  ------------ UITableViewDelegate  &  UITableViewDataSource ------
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD) {
        return 70;
    } else {
        return 50;
    }
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DueTimeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellidenty];
    cell.lblKey.text = self.dataArray[indexPath.row];
    
    if (indexPath.row == 0) {
        if ([[self panduan] isEqualToString:@"chinese"]) {
            cell.lblValue.text = self.Userdict[@"PackageName"];
        } else {
            cell.lblValue.text = self.Userdict[@"EnPackageName"];
        }
        cell.iconImageView.image = [UIImage imageNamed:@"ic_product"];
    } else if (indexPath.row == 1) {
        NSString *startTime = self.Userdict[@"startTime"];
        cell.lblValue.text = [ChangeTimeString GetYearMonthDayString:startTime];
        cell.iconImageView.image = [UIImage imageNamed:@"ic_calendar"];
    } else if (indexPath.row == 2) {
        NSString *stopTime = self.Userdict[@"StopTime"];
        cell.lblValue.text = [ChangeTimeString GetYearMonthDayString:stopTime];
        cell.iconImageView.image = [UIImage imageNamed:@"ic_calendar"];
    } else {
        NSString *surTime = [ChangeTimeString dateTimeDifferenceWithStartTime:[ChangeTimeString getCurrentTime] endTime:[ChangeTimeString DataChangeTimeString:self.Userdict[@"StopTime"]]];
        cell.lblValue.text = surTime;
        cell.iconImageView.image = [UIImage imageNamed:@"ic_time"];
    }
    return cell;
}
    
    @end
