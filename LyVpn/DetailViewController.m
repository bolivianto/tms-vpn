//
//  DetailViewController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "DetailViewController.h"
#import "CalculateHeight.h"
#import "UILabel+LXAdd.h"
@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"VPN";
    self.view.backgroundColor = [UIColor whiteColor];
    [self createNavi];
    [self CreateBack];
}
- (void) createNavi {
    // 创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backMainVC:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
}

- (void) backMainVC:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) CreateBack {
    UIView * backView = [[UIView alloc] initWithFrame:CGRectMake(0, 14, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    UILabel * label = [[UILabel alloc] init];
    label.text = _labeltext;
    label.numberOfLines = 0;
    label.font=[UIFont systemFontOfSize:18];
    label.lineSpace=5;//行间距
    label.characterSpace=3;//字间距
    CGSize labSize =  [label getLableSizeWithMaxWidth:SCREEN_WIDTH - 20];
    label.frame=CGRectMake(15, 10, labSize.width, labSize.height);
    label.textColor = [UIColor darkGrayColor];
    [backView addSubview:label];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
