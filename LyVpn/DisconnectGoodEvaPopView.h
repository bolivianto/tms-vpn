//
//  DisconnectGoodEvaPopView.h
//  极光VPN
//
//  Created by cacac on 2017/5/5.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DisGoodEvaSupPopBlock)();

typedef void(^DisGoodEvaCanPopBlock)();

@interface DisconnectGoodEvaPopView : UIView

@property (nonatomic,copy) DisGoodEvaSupPopBlock SupPopBlock;
@property (nonatomic,copy) DisGoodEvaCanPopBlock CanPopBlock;
@end
