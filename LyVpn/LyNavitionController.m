//
//  LyNavitionController.m
//   VPN
//
//  Created by cacac on 2017/2/9.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "LyNavitionController.h"
@interface LyNavitionController ()

@end

@implementation LyNavitionController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationBar.barStyle = UIBarStyleDefault;
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:248/255.0 green:206/255.0 blue:65/255.0 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Arial Rounded MT Bold" size:20]}];
//    self.navigationBar.barTintColor = bluecOLOR;
    self.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
