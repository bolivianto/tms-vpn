//
//  BuysTableViewCell.h
//   VPN
//
//  Created by cacac on 2017/2/15.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuysTableViewCell : UITableViewCell

- (void) GetCellDataMessage:(NSArray *)array index:(NSInteger)index;

@end
