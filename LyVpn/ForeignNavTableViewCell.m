//
//  ForeignNavTableViewCell.m
//   VPN
//
//  Created by cacac on 2017/2/17.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ForeignNavTableViewCell.h"

@interface ForeignNavTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *forImage;

@property (weak, nonatomic) IBOutlet UILabel *forName;
@property (weak, nonatomic) IBOutlet UILabel *forDetail;
@property (weak, nonatomic) IBOutlet UIButton *NetBtn;

@end

@implementation ForeignNavTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void) cellDataMessage:(NSArray *)imageArray name:(NSArray *)nameArray detail:(NSArray *)detailArray index:(NSInteger)index
{
    self.NetBtn.tag = index;
    NSString * path = [[NSBundle mainBundle] pathForResource:imageArray[index] ofType:@"png"];
    self.forImage.image = [UIImage imageWithContentsOfFile:path];
    self.forName.text = nameArray[index];
    self.forDetail.text = detailArray[index];
    self.forDetail.textColor = [UIColor lightGrayColor];
}
- (IBAction)clickNet:(UIButton *)sender {
    NSLog(@"点击打开");
    if (_ClickBlock) {
        _ClickBlock(sender.tag);
    }
}
@end
