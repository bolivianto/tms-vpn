//
//  PushHelpViewController.m
//  Lantern VPN
//
//  Created by apple on 15/10/17.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "PushHelpViewController.h"

@interface PushHelpViewController ()

@property (nonatomic,strong) UIColor *textColor;
@property (nonatomic,strong) UILabel *textLabel;
@property (nonatomic,strong) UILabel *money;
@property (nonatomic,strong) UIButton *accountBtn;
@property (nonatomic,strong) UIButton *shoppingCartBtn;
@property (nonatomic,assign) NSUInteger minFreeMoney;
@property (nonatomic,assign) NSInteger nTotal; //用于存放总金额
@property (nonatomic,strong) NSString *shoppingCatrstring; //用于存放购物车里商品名和数量
@property (nonatomic,strong) NSMutableArray *objects;

@end

@implementation PushHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUPPushHelpView];
}
-(void)setUPPushHelpView{
    UIView *mainHelpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    mainHelpView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mainHelpView];
    
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH / 3, SCREEN_WIDTH/3)];
    logoImage.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT / 3);
    logoImage.image = [UIImage imageNamed:@"logo"];
    [mainHelpView addSubview:logoImage];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(logoImage.frame), SCREEN_WIDTH, 50*KWidth_Scale)];
    label1.text = NSLocalizedString(@"key7", nil);
    label1.textAlignment = 1;
    label1.font = [UIFont boldSystemFontOfSize:23*KWidth_Scale];
    [mainHelpView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(50*KWidth_Scale, CGRectGetMaxY(label1.frame), SCREEN_WIDTH - 100*KWidth_Scale, 100*KWidth_Scale)];
    label2.text = NSLocalizedString(@"key11", nil);
    label2.textColor = [UIColor darkGrayColor];
    label2.numberOfLines = 0;
    label2.font = [UIFont boldSystemFontOfSize:19*KWidth_Scale];
    [mainHelpView addSubview:label2];
    
    UIButton *mainHelpBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/3, 40*KWidth_Scale)];
    mainHelpBtn.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/4*3);
    mainHelpBtn.backgroundColor = [UIColor colorWithRed:39.0/255.0 green:189.0/255.0 blue:66.0/255.0 alpha:1];
    mainHelpBtn.layer.masksToBounds = YES;
    mainHelpBtn.layer.cornerRadius = 5.0;
    [mainHelpBtn setTitle:NSLocalizedString(@"key6", nil) forState:UIControlStateNormal];
    mainHelpBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20*KWidth_Scale];
    [mainHelpBtn addTarget:self action:@selector(mainHelpBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [mainHelpView addSubview:mainHelpBtn];
    
    
    [self layoutUI];
    
}

-(void)layoutUI
{
    _minFreeMoney = 0.1;
    //横线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    line.layer.borderColor = [UIColor lightGrayColor].CGColor;
    line.layer.borderWidth = 0.25;
    
    
    //购物金额提示框
    _money = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0,30)];
    [_money setTextColor:[UIColor grayColor]];
    [_money setText:@"购物车空空如也~"];
    [_money setFont:[UIFont systemFontOfSize:13.0]];
    
    //结账按钮
    _accountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _accountBtn.layer.cornerRadius = 5;
    _accountBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    _accountBtn.frame = CGRectMake(0, 0, 0,0);
    _accountBtn.backgroundColor = [UIColor lightGrayColor];
    [_accountBtn setTitle:[NSString stringWithFormat:@"请选色"] forState:UIControlStateNormal];
    [_accountBtn addTarget:self action:@selector(pay:) forControlEvents:UIControlEventTouchUpInside];
    _accountBtn.enabled = NO;
    
    //购物车
    _shoppingCartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_shoppingCartBtn setUserInteractionEnabled:NO];
    [_shoppingCartBtn setBackgroundImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    _shoppingCartBtn.frame = CGRectMake(0, 0, 0,0);
    [_shoppingCartBtn addTarget:self action:@selector(clickCartBtn:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark - private method

- (void)setCartImage:(NSString *)imageName
{
    [_shoppingCartBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}


- (void)mainHelpBtnAction
{
    NSString *desStr = [[NSString alloc]
                        initWithData:[[NSData alloc]
                                      initWithBase64EncodedString:[desFile decryptWithText:@"32BVneR5+L+NjJpm93PQfVtlqlfh99ML+U1ej9SwhAPR4NQYNY3CxqVnn+9uFHDP7giI+B+2+MI="] options:0] encoding:NSUTF8StringEncoding];
    //创建URL
    NSURL *url = [NSURL URLWithString:desStr];
    [[UIApplication sharedApplication] openURL:url];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
