//
//  OKViewController.m
//  -VPN
//
//  Created by a on 2017/8/4.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "OKViewController.h"
#import "ZiDongConfragitionViewController.h"
#import "UsersViewController.h"
#import "BuysViewController.h"
@interface OKViewController ()
{
    UIButton *bindBtn;
}

@end

@implementation OKViewController


-(void)creatNavi
{
    //创建导航栏左按钮   用于编辑
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0,30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"wp_back"] forState:UIControlStateNormal];
    if (IS_IPHONEX) {
        [leftBtn setImage:[UIImage imageNamed:@"wp_backX"] forState:UIControlStateNormal];
    }
    [leftBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;    
    self.navigationItem.title = NSLocalizedString(@"oldUser3", nil);
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:bluecOLOR,NSForegroundColorAttributeName,nil]];
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)creatMain
{
    
    
    ///logo
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - SCREEN_WIDTH / 7 , 80*KWidth_Scale, SCREEN_WIDTH / 3.5, SCREEN_WIDTH / 3.5)];
    
    logoImageView.image = [UIImage imageNamed:@"account"];
    
    
    [self.view addSubview:logoImageView];

    
    UILabel *remindLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(logoImageView.frame) + 20*KWidth_Scale, SCREEN_WIDTH, 50)];
    
    remindLabel.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3);
    
    remindLabel.backgroundColor = [UIColor clearColor];
    
    remindLabel.textColor = bluecOLOR;
    
    remindLabel.textAlignment = 1;
    
    remindLabel.text = NSLocalizedString(@"oldUser17", nil);
    
    remindLabel.font = [UIFont boldSystemFontOfSize:30.0*KWidth_Scale];
    
    remindLabel.numberOfLines = 0;
    
    [self.view addSubview:remindLabel];
    
    //绑定按钮
    bindBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/6, CGRectGetMaxY(remindLabel.frame) + 50*KWidth_Scale, SCREEN_WIDTH/3*2, 40*KWidth_Scale)];
    
    bindBtn.titleLabel.font = [UIFont systemFontOfSize:16.0*KWidth_Scale];
    
    [bindBtn setBackgroundImage:[UIImage imageNamed:@"登陆"] forState:UIControlStateNormal];

    [bindBtn setTitle:NSLocalizedString(@"oldUser8", nil) forState:UIControlStateNormal];
    
    [bindBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [bindBtn addTarget:self action:@selector(activate) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:bindBtn];

    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self creatNavi];
    [self creatMain];
    
}
#warning ......
- (void)opefffffnURL:(NSString*)URL
{
    NSString *transformURL = URL;
    NSArray* elts = [URL componentsSeparatedByString:@"?"];
    if (elts.count >= 2) {
        NSArray *urls = [elts.lastObject componentsSeparatedByString:@"="];
        for (NSString *param in urls) {
            if ([param isEqualToString:@"_wx_tpl"]) {
                transformURL = [[urls lastObject]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            }
        }
    }
    NSURL *url = [NSURL URLWithString:transformURL];
 
    [self jsReplace:url];

    NSMutableDictionary *queryDict = [NSMutableDictionary new];
    if (1 == 0) {
        NSURLComponents *components = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
        NSArray *queryItems = [components queryItems];
        
        for (NSURLQueryItem *item in queryItems)
            [queryDict setObject:item.value forKey:item.name];
    }else {
        queryDict = [self queryWithURL:url];
    }
    NSString *wsport = queryDict[@"wsport"] ?: @"8082";
    NSURL *socketURL = [NSURL URLWithString:[NSString stringWithFormat:@"ws://%@:%@", url.host, wsport]];
   
    
}

- (NSMutableDictionary*)queryWithURL:(NSURL *)url {
    NSMutableDictionary * queryDic = nil;
    if (![url query]) {
        return queryDic;
    }
    queryDic = [NSMutableDictionary new];
    NSArray* components = [[url query] componentsSeparatedByString:@"&"];
    for (NSUInteger i = 0; i < [components count]; i ++) {
        NSString * queryParam = [components objectAtIndex:i];
        NSArray* component = [queryParam componentsSeparatedByString:@"="];
        [queryDic setValue:component[1] forKey:component[0]];
    }
    
    return  queryDic;
}

#pragma mark - Replace JS

- (void)jsReplace:(NSURL *)url
{
    if ([[url host] isEqualToString:@"weex-remote-debugger"]){
        NSString* path = [url path];
        if ([path isEqualToString:@"/dynamic/replace/bundle"]){
            for (NSString * param in [[url query] componentsSeparatedByString:@"&"]) {
                NSArray* elts = [param componentsSeparatedByString:@"="];
                if ([elts count] < 2) {
                    continue;
                }
                if ([[elts firstObject] isEqualToString:@"bundle"]){
                }
            }
        }
        
        if ([path isEqualToString:@"/dynamic/replace/framework"]){
            for (NSString * param in [[url query] componentsSeparatedByString:@"&"]) {
                NSArray* elts = [param componentsSeparatedByString:@"="];
                if ([elts count] < 2) {
                    continue;
                }
                if ([[elts firstObject] isEqualToString:@"framework"]){
                }
            }
        }
    }
}
#warning ........

-(void)bindBtnAction
{
    
    self.tabBarController.tabBar.hidden = NO;
    //pop到指定界面
    if ([_isZidong isEqualToString:@"1"])
    {
        for (UIViewController *controller in self.navigationController.viewControllers) {  
            if ([controller isKindOfClass:[ZiDongConfragitionViewController class]]) {  
                ZiDongConfragitionViewController *zidongVC =(ZiDongConfragitionViewController *)controller;  
                [self.navigationController popToViewController:zidongVC animated:YES];  
            }  
        }  
    }else if ([_isZidong isEqualToString:@"2"])
    {
        for (UIViewController *controller in self.navigationController.viewControllers) {  
            if ([controller isKindOfClass:[BuysViewController class]]) {  
                BuysViewController *zidongVC =(BuysViewController *)controller;  
                [self.navigationController popToViewController:zidongVC animated:YES];  
            }  
        }  
    }else
    {
        for (UIViewController *controller in self.navigationController.viewControllers) {  
            if ([controller isKindOfClass:[UsersViewController class]]) {  
                UsersViewController *usersVC =(UsersViewController *)controller;  
                [self.navigationController popToViewController:usersVC animated:YES];  
            }  
        }  
    }

}

@end
