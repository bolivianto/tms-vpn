//
//  GoodEvalutionTableViewCell.m
//  极光VPN
//
//  Created by cacac on 2017/4/21.
//  Copyright © 2017年 cnaa. All rights reserved.
//

#import "GoodEvalutionTableViewCell.h"

@interface GoodEvalutionTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *goodString;

@property (weak, nonatomic) IBOutlet UIButton *goodButton;

@property (nonatomic,strong) NSMutableArray * textArray;
@end

@implementation GoodEvalutionTableViewCell


- (NSMutableArray *)textArray
{
    if (_textArray == nil) {
        _textArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _textArray;
}

- (IBAction)goodClick:(UIButton *)sender {
//    NSLog(@"复制");
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    NSString *string = self.textArray[sender.tag][@"Vcontent"];
    [pab setString:string];
    if (pab == nil) {
//        NSLog(@"复制失败");
        [self showMessage:NSLocalizedString(@"custom9", nil) duration:2];
    }else
    {
//        NSLog(@"复制成功");
        [self showMessage:NSLocalizedString(@"custom8", nil) duration:2];
    }
}


- (void) setEveryName:(NSArray *)array index:(NSInteger)index
{
    [self.textArray addObjectsFromArray:array];
    self.goodString.text = array[index][@"Vcontent"];
    self.goodButton.tag = index;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.goodString.font = [UIFont systemFontOfSize:13 * KWidth_Scale];
    self.goodButton.titleLabel.font = [UIFont systemFontOfSize:13 * KWidth_Scale];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)showMessage:(NSString *)message duration:(NSTimeInterval)time
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [UIColor grayColor];
    showview.frame = CGRectMake(1, 1, 1, 1);
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    
    UILabel *label = [[UILabel alloc]init];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.f],
                                 NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [message boundingRectWithSize:CGSizeMake(207, 999)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes context:nil].size;
    
    label.frame = CGRectMake(10, 5, labelSize.width +20, labelSize.height);
    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = 1;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:15];
    [showview addSubview:label];
    
    showview.frame = CGRectMake((screenSize.width - labelSize.width - 20)/2,
                                screenSize.height - 300,
                                labelSize.width+40,
                                labelSize.height+10);
    [UIView animateWithDuration:time animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}
@end
