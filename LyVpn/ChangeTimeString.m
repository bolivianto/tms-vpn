//
//  ChangeTimeString.m
//   VPN
//
//  Created by cacac on 2017/2/16.
//  Copyright © 2017年 cacac. All rights reserved.
//

#import "ChangeTimeString.h"

@implementation ChangeTimeString
+  (NSString *) DataChangeTimeString:(NSString *)time
{
    if (!time || [time length] == 0 ) {  // 字符串为空判断
        return @"";
    }
    NSMutableString * muStr = [NSMutableString stringWithString:time];
    NSString * timeStampString = [NSString string];
    //  遍历取出括号内的时间戳
    for (int c = 0; c < time.length; c ++) {
        NSRange startRang = [muStr rangeOfString:@"("];
        NSRange endRang = [muStr rangeOfString:@")"];
        if (startRang.location != NSNotFound) {
            // 左边括号位置
            NSInteger loc = startRang.location;
            // 右边括号距离左边括号的长度
            NSInteger len = endRang.location - startRang.location;
            // 截取括号时间戳内容
            timeStampString = [muStr substringWithRange:NSMakeRange(loc + 1,len - 1)];
        }
    }
    // 把时间戳转化成时间
    NSTimeInterval interval=[timeStampString doubleValue] / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *objDateformat = [[NSDateFormatter alloc] init];
    [objDateformat setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString * timeStr = [NSString stringWithFormat:@"%@",[objDateformat stringFromDate: date]];
    return timeStr;
}

+ (NSString *) GetYearMonthDayString:(NSString *)time
{
    if (!time || [time length] == 0 ) {  // 字符串为空判断
        return @"";
    }
    NSMutableString * muStr = [NSMutableString stringWithString:time];
    NSString * timeStampString = [NSString string];
    //  遍历取出括号内的时间戳
    for (int c = 0; c < time.length; c ++) {
        NSRange startRang = [muStr rangeOfString:@"("];
        NSRange endRang = [muStr rangeOfString:@")"];
        if (startRang.location != NSNotFound) {
            // 左边括号位置
            NSInteger loc = startRang.location;
            // 右边括号距离左边括号的长度
            NSInteger len = endRang.location - startRang.location;
            // 截取括号时间戳内容
            timeStampString = [muStr substringWithRange:NSMakeRange(loc + 1,len - 1)];
        }
    }
    // 把时间戳转化成时间
    NSTimeInterval interval=[timeStampString doubleValue] / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *objDateformat = [[NSDateFormatter alloc] init];
    [objDateformat setDateFormat:@"yyyy-MM-dd"];
    NSString * timeStr = [NSString stringWithFormat:@"%@", [objDateformat stringFromDate: date]];
    return timeStr;
}

+ (NSString *)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime {
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = end - start;

    int minute = (int)value /60%60; //分
    int house = (int)value %(24*3600)/3600;   //时
    int day = (int)value / (24 * 3600);     //天
    NSString *str;
    if (day != 0) {
        if (day < 0) {
            str = @"0分";
        }else
        {
           str = [NSString stringWithFormat:@"%0.2d天%0.2d时%0.2d分",day,house,minute];
        }
    }else if (day==0 && house != 0) {
        if (house < 0) {
            str = @"0分";
        }
        else
        {
            str = [NSString stringWithFormat:@"%0.2d时%0.2d分",house,minute];
        }
    }else if (day== 0 && house== 0 && minute!=0) {
        if (minute < 0) {
            str = @"0分";
        }
        else
        {
            str = [NSString stringWithFormat:@"00天00时%0.2d分",minute];
        }
    }else{
        str = @"0分";
    }
    return str;
}

+ (NSString *)getCurrentTime
{
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (long)getZiFuChuan:(NSString*)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date1 = [dateFormatter dateFromString:time];
    return [date1 timeIntervalSince1970]*1000;
}

+ (long)getZiFuChuanFromDate:(NSDate*)time
{
    return [time timeIntervalSince1970]*1000;
}
@end
